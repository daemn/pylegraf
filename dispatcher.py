#!/usr/bin/env python3
__doc__ = 'Import and execute plugins.'

import os, sys
import time
from random import randint
import signal
import threading
import importlib.util
import pylegraf_lib as plib
import logging, logging.handlers
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)
EXEC_MODE = 'threads'


def use_plugin_dirs(dirs):
    r'''Setup dirs for search plugins.
    '''
    Module.use_source_dirs(dirs)


def set_exec_mode(mode):
    r'''Setup execution mode.
    '''
    global EXEC_MODE
    EXEC_MODE = mode
    LOG.info('Now exec mode is "{}".'.format(EXEC_MODE))


def add_plugin(conf):
    r'''Import module and initiate plugin.
    in: plugin config
    '''
    LOG.info('Add plugin "{}".'.format(conf['name']))
    try:
        plugin = Plugin(conf['name'])
        plugin.interface = PluginInterface(conf)
        plugin.module = Module(conf['module'])
    except Exception as err:
        LOG.debug(str(err))
        plugin.destroy()
        return False
    return True


def test_plugin(conf):
    r'''Import module, add plugin and test.
    in: plugin config
    '''
    LOG.info('Add plugin "{}".'.format(conf['name']))
    plugin = Plugin(conf['name'])
    plugin.interface = PluginInterface(conf)
    plugin.module = Module(conf['module'])
    return plugin.test()


def run():
    r'''Execute plugins.
    '''
    LOG.info('Setup plugins.')
    for plugin in Plugin.all(): plugin.setup()

    if len(list(Plugin.all())) == 0:
        LOG.critical('No plugins for execute.')
        return False

    LOG.info('Execute plugins.')
    if EXEC_MODE == 'once': _execute_plugins_once()
    elif EXEC_MODE == 'single': _execute_plugins_in_single_mode()
    else: _execute_plugins_in_threads_mode()

    LOG.info('Destroy plugins.')
    for plugin in Plugin.all(): plugin.destroy()


class _SignalCatcher():
    r'''Processing of signals.
    got_sigint      \
    got_sigterm     | True if an appropriate signal is received.
    got_sigusr1     /
    must_work       False, if receive INT or TERM.
    reset_signals() Reset signal flags.

    ----- examples -----
    A = _SignalCatcher()
    while A.must_work:   # Wait for signal
        print('foo')
    if A.got_sigterm:
        sys.exit(0)      # Exit if recieved SIGTERM,
    print('bar')         # else print 'bar'
    '''
    instances = []

    def __init__(self):
        self.reset_signals()
        self.must_work = True
        signal.signal(signal.SIGINT,  self._register_signal)
        signal.signal(signal.SIGTERM, self._register_signal)
        signal.signal(signal.SIGUSR1, self._register_sigusr1)
        _SignalCatcher.instances.append(self)

    def _register_signal(self, signum, frame):
        if signum == signal.SIGINT:
            if self.got_sigint:
                LOG.warning('Got double SIGINT')
                sys.exit(255)
            LOG.info('Got SIGINT')
            self.got_sigint = True
        elif signum == signal.SIGTERM:
            if self.got_sigterm:
                LOG.warning('Got double SIGTERM')
                sys.exit(255)
            LOG.info('Got SIGTERM')
            self.got_sigterm = True
        self.must_work = False

    def _register_sigusr1(self, signum, frame):
        LOG.info('Got SIGUSR1')
        self.got_sigusr1 = True

    def reset_signals(self):
        r'''Reset signals flags.
        '''
        self.got_sigint  = False
        self.got_sigterm = False
        self.got_sigusr1 = False


class Module():
    source_dirs = []
    instances = []

    @classmethod
    def use_source_dirs(cls, dirs):
        r'''Setup dirs for search modules.
        '''
        cls.source_dirs = plib.typecast(dirs, list)
        LOG.debug('New list of plugin dirs is: {}.'
                  .format(', '.join(cls.source_dirs)))

    def __init__(self, name):
        r'''In: module name.
        '''
        self.name = name
        LOG.info('Import module "{}".'.format(name))
        self.module_name = ''.join(
            [a if (a.isalnum() or a in '-_') else '_' for a in name])
        self.module_name = 'plg_' + self.module_name.strip('_')
        self.module_name = plib.pick_up_an_unused_name(self.module_name,
                                                       sys.modules)
        if not self._search_file(): raise ImportError('Module file not found.')
        if not self._import(): raise ImportError('Import failed.')
        self.__class__.instances.append(self)

    def _search_file(self):
        self.source_file = None
        for source_dir in self.__class__.source_dirs:
            source_file = os.path.join(source_dir, self.name + '.py')
            if os.path.isfile(source_file):
                self.source_file = source_file
                return True
        return False

    def _import_via_exec(self):
        self.module_object = None
        LOG.debug('Try to import module via exec(source) from file "{}".'
                  .format(self.source_file))
        with open(self.source_file, 'r', encoding='UTF-8') as fd:
            source_code = fd.read()
        module_object = type(sys)(self.module_name)
        sys.modules[self.module_name] = module_object
        exec(source_code, module_object.__dict__)
        self.module_object = module_object
        return True

    def _import_via_importlib(self):
        self.module_object = None
        LOG.debug('Try to import module via importlib from file "{}".'
                  .format(self.source_file))
        spec = importlib.util.spec_from_file_location(self.module_name,
                                                      self.source_file)
        module_object = importlib.util.module_from_spec(spec)
        sys.modules[self.module_name] = module_object
        spec.loader.exec_module(module_object)
        self.module_object = module_object
        return True

    # Importlib not present before python 3.5
    if plib.python_version() < 3.5: _import = _import_via_exec
    else: _import = _import_via_importlib

    def is_imported(self):
        r'''Check the module has been imported.
        '''
        return self.module_object is not None

    def have_test(self):
        r'''Check of a test function in the module.
        '''
        return (self.module_object is not None
                and hasattr(self.module_object, 'test'))

    def have_setup(self):
        r'''Check of a setup function in the module.
        '''
        return (self.module_object is not None
                and hasattr(self.module_object, 'setup'))

    def have_execute(self):
        r'''Check of an execute function in the module.
        '''
        return (self.module_object is not None
                and hasattr(self.module_object, 'execute'))

    def have_destroy(self):
        r'''Check of a destroy function in the module.
        '''
        return (self.module_object is not None
                and hasattr(self.module_object, 'destroy'))

    def test(self, interface):
        r'''Execute a test function.
        '''
        return self.module_object.test(interface)

    def setup(self, interface):
        r'''Execute a setup function.
        '''
        return self.module_object.setup(interface)

    def execute(self, interface):
        r'''Execute an execute function.
        '''
        return self.module_object.execute(interface)

    def destroy(self, interface):
        r'''Execute a destroy function.
        '''
        return self.module_object.destroy(interface)


def _execute_plugins_once():
    r'''Execute plugins once and exit.
    '''
    LOG.info('Begin of "once" mode.')

    # Collect data.
    for plugin in Plugin.collectors():
        plugin.execute()

    # Send data.
    for plugin in Plugin.senders():
        plugin.execute()


def _execute_plugins_in_single_mode():
    r'''Periodically execute plugins one by one.
    '''
    LOG.info('Begin of "single" mode.')
    signal = _SignalCatcher()

    # Main job cycle.
    while signal.must_work:
        for plugin in Plugin.all():
            if int(time.time()) >= plugin.interface.next_time:
                plugin.execute()
        time.sleep(.3)


def _execute_plugins_in_threads_mode():
    r'''Execute every plugin in personal thread.
    '''
    LOG.info('Begin of "threads" mode.')
    for plugin in Plugin.all():
        error = None
        try:
            thread = _PluginThread(plugin)
            thread.daemon = True
            thread.start()
        except Exception as err:
            error = str(err)
        if error:
            LOG.critical('The threads start was failed. {}'.format(error))
            _PluginThread.stop_threads()
            return False

    # Main job cycle.
    signal = _SignalCatcher()
    while signal.must_work: time.sleep(.1)

    _PluginThread.stop_threads()


class _PluginThread(threading.Thread):
    r'''Thread for plugin.
    in: plugin object

    ----- methods -----
    cls.stop_threads()
    '''
    instances = []
    stop_timeout = 10

    @classmethod
    def stop_threads(cls):
        r'''Stop all plugin threads.
        '''
        for thread in cls.instances:
            if thread is not None: thread.must_work = False
        deadtime = int(time.time()) + cls.stop_timeout
        while threading.active_count() > 1:
            if deadtime < int(time.time()):
                LOG.warning('Threads stop too long.')
                return False
            time.sleep(.1)
        return True

    def __init__(self, plugin):
        super().__init__()
        self.plugin = plugin
        self.name   = plugin.name
        LOG.debug('Thread {} initiation.'.format(self.name))
        self.must_work = True
        self.plugin.interface.inbox_locker = threading.Lock()
        self.__class__.instances.append(self)

    def run(self):
        LOG.debug('Start the thread "{}".'.format(self.name))
        while self.must_work:
            time.sleep(.3)
            if int(time.time()) >= self.plugin.interface.next_time:
                self.plugin.execute()
            if self.plugin not in Plugin.instances:
                self.must_work = False

        instances = self.__class__.instances
        try: instances[instances.index(self)] = None
        except ValueError: pass
        return None


class PluginInterface():
    r'''Interface between plugin and dispatcher.

    ----- attributes -----
    name              - Name of the plugin.
    settings          - An individual plugin settings.
    plugin_metric     - Plugin part of metric name.
    exec_period       - Execution period for the execute() function.
    previous_time     - Previous execution time of the execute() function.
    current_time      - Current execution time of the execute() function.
    sleep_time        - Time bitween previous and current running of execute().
    next_time         - Next time of runnig execute().
    fail_limit        - After this number of fails destroy the plugin.
    failures_in_row   - Number of failed executions.
    successes_in_row  - Number of success executions.
    module_name       - Name of imported module in config (not in system).

    ----- methods -----
    log_debug()               \
    log_info()                |
    log_warning()             | Out message.
    log_error()               |
    log_critical()            /
    sleep()                   - Do not execute plugin for a while.
    apply_default_settings()  - Merge default and user settings.
    put_to_storage()          - ###TODO
    get_from_storage()        - ###TODO
    i_need_metrics()          - Send metrics to the plugin.
    i_dont_need_metrics()     - Don`t send metrics to the plugin.
    recieve_metric()          - Get recieved metrics one by one.
    send_metric()             - Send metric to other plugins that need it.
    '''

    def __init__(self, conf):
        r'''In: dictionary with plugin configuration.
        name           - Plugin name.
        exec_period    - Period for execute plugin.
        fail_limit     - Limit of fail executes.
        plugin_metric  - Plugin part of metric.
        settings       - Personal plugin settings.
        module_name    - Name of imported module in config (not in system).
        '''
        self.name          = conf['name']
        self._log_prefix   = '(' + self.name + ') '
        self.settings      = conf.get('settings', {})
        self.plugin_metric = conf.get('plugin_metric',
                                      plib.convert_to_metric(self.name))
        self.exec_period   = conf.get('exec_period', 10)
        self.previous_time = 0
        self.current_time  = 0
        self.sleep_time    = 0
        self.next_time     = int(time.time()) + randint(1, self.exec_period)
        self.fail_limit    = conf.get('fail_limit', 10)
        self.failures_in_row  = 0
        self.successes_in_row = 0
        self.inbox_locker  = None
        self.inbox         = []
        self.outbox        = []
        self.need_metrics  = False
        self.module_name   = conf.get('module', '')

    def log_debug(self, message): LOG.debug(self._log_prefix + str(message))

    def log_info(self, message): LOG.info(self._log_prefix + str(message))

    def log_warning(self, message): LOG.warning(self._log_prefix + str(message))

    def log_error(self, message): LOG.error(self._log_prefix + str(message))

    def log_critical(self, message): LOG.critical(self._log_prefix + str(message))

    def sleep(self, seconds):
        r'''Do not execute plugin for specified time.
        '''
        self.log_debug('Will sleep for {} seconds.'.format(seconds))
        self.next_time = int(time.time()) + int(seconds)
        return True

    def apply_default_settings(self, default_settings, allow_strange=True):
        r'''Merge plugin default settings and settings from configuration.
        in: default settings
            allow strange option flag
        '''
        if not isinstance(default_settings, dict):
            self.log_error('Only dictionary allowed as settings.')
            return False
        settings = plib.Options()
        for key, value in default_settings.items():
            settings.add_option(key, default=value)
        for key, value in self.settings.items():
            try: settings[key] = value
            except KeyError as err:
                if allow_strange:
                    settings.add_option(key, default=value)
                else:
                    self.log_error('Unknown setting "{}".'.format(key))
                    self.log_debug(err)
                    return False
            except TypeError as err:
                self.log_error('Apply setting "{}" was failed.'.format(key))
                self.log_debug(err)
                return False
        self.settings = settings.get_all()
        return True

    def i_need_metrics(self):
        r'''Register plugin as metrics reciever.
        '''
        self.log_info('Register as metrics reciever.')
        self.need_metrics = True
        return True

    def i_dont_need_metrics(self):
        r'''Unregister plugin as metrics reciever.
        '''
        self.log_info('Unregister as metrics reciever.')
        self.need_metrics = False
        return True

    def recieve_metric(self):
        r'''Get a metrics from inbox.
        format: (time, name, type, value)
        ----- example -----
        for note in PI.recieve_metric():
            print(note['mname'], note['mval'])
        '''
        if self.inbox_locker:
            with self.inbox_locker:
                inbox_copy = self.inbox
                self.inbox = []
        else:
            inbox_copy = self.inbox
            self.inbox = []
        if inbox_copy:
            self.log_info('Recieve {} metrics.'.format(len(inbox_copy)))
        for note in inbox_copy:
            yield dict(zip(('mtime', 'mname', 'mtype', 'mval'), note))
        return None

    def send_metric(self, mtime=None, mname='', mtype='gauge', mval=None):
        r'''Put a formatted metrics to the outbox.
        ----- input -----
        mtime (int) Metric time in Unix format. Will set automaticaly if None.
        mname (str) Metric name. Default is ''.
        mtype (str) Metric type. Default is 'gauge'.
        mval  (int, float) Value.
        '''
        mtime = round(mtime or time.time())
        mname = plib.convert_to_metric(mname, preserve_dot=True)
        if mtype not in ('count', 'gauge'):
            self.log_error('Not allowed mtype "{}"'.format(mtype))
            return False
        if not isinstance(mval, int) and not isinstance(mval, float):
            self.log_error('Not integer or float mval.')
            return False
        mname = plib.join_metric((self.plugin_metric, mname))
        self.outbox.append((mtime, mname, mtype, mval))
        self.log_debug('Send: {} {} {} {}.'.format(mtime, mname, mtype, mval))
        return True


class Plugin():
    r'''Plugin objects.
    ----- methods -----
    cls.all()
    cls.collectors()
    cls.senders()
    test()
    setup()
    execute()
    destroy()
    '''
    instances = []

    @classmethod
    def all(cls):
        r'''Plugins iterator.
        '''
        for plugin in cls.instances:
            if plugin is not None: yield plugin

    @classmethod
    def collectors(cls):
        r'''Collectors iterator.
        '''
        for plugin in cls.all():
            if not plugin.interface.need_metrics: yield plugin

    @classmethod
    def senders(cls):
        r'''Senders iterator.
        '''
        for plugin in cls.all():
            if plugin.interface.need_metrics: yield plugin

    def __init__(self, name):
        r'''In: dictionary with plugin configuration.
        name - Plugin name.
        '''
        self.name      = name
        self.interface = None
        self.module    = None
        self.__class__.instances.append(self)

    def test(self):
        r'''Try to execute a plugin test function.
        '''
        if not self.module.have_test():
            self.interface.log_warning('No function "test".')
            return True
        self.interface.log_debug('Execute function "test".')
        self.interface.current_time = int(time.time())
        if self.module.test(self.interface) is False: return False
        return True

    def setup(self):
        r'''Try to execute a plugin setup function.
        '''
        if not self.module.have_setup(): return False
        interface = self.interface
        interface.log_debug('Execute function "setup".')
        interface.current_time = int(time.time())
        error = None
        try:
            if self.module.setup(interface) is False:
                raise RuntimeError('The function "setup" return False.')
        except Exception as err: error = str(err)
        interface.previous_time = interface.current_time
        if error:
            interface.log_error('The function "setup" was failed.')
            interface.log_debug(error)
            self.destroy()
            return False
        return True

    def execute(self):
        r'''Try to execute a plugin execute function.
        '''
        if not self.module.have_execute():
            self.interface.log_critical('No function "execute" in module.')
            self.destroy()
            return False

        interface = self.interface
        interface.next_time = 0
        interface.current_time = int(time.time())
        if interface.previous_time:
            interface.sleep_time = interface.current_time - interface.previous_time
        else:
            interface.sleep_time = 0
        error = None
        try:
            if self.module.execute(interface) is False:
                raise RuntimeError('The function "execute" return False.')
        except Exception as err: error = str(err)
        if error:
            interface.log_error('The function "execute" was failed.')
            interface.log_debug(error)
            interface.successes_in_row = 0
            interface.failures_in_row += 1
            result = False
        else:
            interface.successes_in_row += 1
            interface.failures_in_row   = 0
            result = True

        interface.previous_time = interface.current_time
        if not interface.next_time:
            interface.next_time = interface.previous_time + interface.exec_period

        if interface.outbox:
            interface.log_debug('Send {} messages to other plugins.'
                                .format(len(interface.outbox)))
            for target_plugin in self.__class__.senders():
                if target_plugin is self: continue
                if target_plugin.interface.inbox_locker:
                    with target_plugin.interface.inbox_locker:
                        target_plugin.interface.inbox += interface.outbox
                else:
                    target_plugin.interface.inbox += interface.outbox
            interface.outbox = []

        if (not result
                and interface.fail_limit
                and interface.failures_in_row > interface.fail_limit):
            interface.log_warning('Fail limit was reached.')
            self.destroy()

        return result

    def destroy(self):
        r'''Try to execute a plugin destroy function and delete plugin instance.
        '''
        if self.module is not None and self.module.have_destroy():
            interface = self.interface
            interface.log_debug('Execute function "destroy".')
            error = None
            try:
                if self.module.destroy(interface) is False:
                    raise RuntimeError('The function "destroy" return False.')
            except Exception as err: error = str(err)
            if error:
                interface.log_error('The function "destroy" was failed.')
                interface.log_debug(error)

        try: i = self.__class__.instances.index(self)
        except ValueError: return False
        self.__class__.instances[i] = None
        return True


##############################################################################

#     # def put_to_storage(self, data):
#     #     r'''Put data to storage.
#     #     '''
#     #     return self._storage.put(data)

#     # def get_from_storage(self):
#     #     r'''Get back stored data.
#     #     '''
#     #     return self._storage.get()

# class DataStorage():
#     r'''Store data in file.
#     cls.init_basedir() - Init the storage with specified path.
#     get()          - Get data from storage.
#     put(data)      - Put data in storage.
#     saved          - Is the data saved to storage.
#     time           - Timestamp of storage data.
#     error          - Error message if something was wrong.

#     ----- examples -----
#     Storage.init('/tmp/mystor')
#     A = Storage('myprog')
#     A.put([1, 2, 3])
#     print(A.get())        # [1, 2, 3]
#     '''
#     basedir = None

#     @classmethod
#     def init_basedir(cls, path='/tmp'):
#         cls.basedir = plib.absolute_path(path)
#         if not os.path.isdir(cls.basedir):
#             os.makedirs(cls.basedir)

#     def __init__(self, name):
#         basedir = self.__class__.basedir
#         if basedir is None:
#             raise RuntimeError('Try to use storage without initiation.')
#         self.name  = str(name)
#         self._file = os.path.join(basedir, self.name + '.pickle')
#         self._load_from_file()
#         if self.error: LOG.error('Load storage error. {}'.format(self.error))

#     def _load_from_file(self):
#         self.is_saved  = False
#         self.error     = None
#         self.data_time = None
#         self._data     = None
#         try:
#             if os.path.isfile(self._file):
#                 with open(self._file, 'rb') as fd: pack = pickle.load(fd)
#         except(OSError, EnvironmentError, pickle.PicklingError) as err:
#             self.error = str(err)
#             return
#         self.is_saved  = True
#         self.data_time = pack[0]
#         self._data     = pack[1]

#     def get_data(self): return deepcopy(self._data)

#     def _save_to_file(self):
#         self.error    = None
#         self.is_saved = False
#         try:
#             pack = pickle.dumps((self.data_time, self._data),
#                                 pickle.HIGHEST_PROTOCOL)
#             with open(self._file, 'wb') as fd: fd.write(pack)
#         except(OSError, EnvironmentError, pickle.PicklingError) as err:
#             self.error = str(err)
#             return
#         self.is_saved = True

#     def put_data(self, data):
#         self.data_time = time.time()
#         self._data     = data
#         self._save_to_file()
#         if self.error: LOG.error('Save storage error. {}'.format(self.error))


def _unittest():
    '''Tests
    '''
    from test_lib import tmpdir, make_tmpfile, read_tmpfile, clear_tmpdir
    log_handler = logging.StreamHandler()
    log_handler.setFormatter(logging.Formatter('%(levelname)s %(message)s'))
    LOG.addHandler(log_handler)
    LOG.setLevel(logging.CRITICAL)

    ### def use_plugin_dirs()

    ### def set_exec_mode()

    ### def add_plugin()

    ### def test_plugin()

    ### def run()

    ### class _SignalCatcher()
    S = _SignalCatcher()
    assert (S.got_sigint, S.got_sigterm, S.got_sigusr1, S.must_work) == \
        (False, False, False, True)

    S._register_sigusr1(signal.SIGUSR1, None)
    assert (S.got_sigint, S.got_sigterm, S.got_sigusr1, S.must_work) == \
        (False, False, True, True)

    S._register_signal(signal.SIGINT, None)
    assert (S.got_sigint, S.got_sigterm, S.got_sigusr1, S.must_work) == \
        (True, False, True, False)

    S.reset_signals()
    assert (S.got_sigint, S.got_sigterm, S.got_sigusr1, S.must_work) == \
        (False, False, False, False)

    ### class Module()
    clear_tmpdir()
    Module.use_source_dirs(tmpdir)
    module_name = 'testplugin'
    make_tmpfile(
        module_name + '.py',
        'testvar = "foo"\n'
        'def destroy():\n'
        '    pass\n'
    )
    module = Module(module_name)
    assert module.is_imported() is True
    assert module.module_object.testvar == 'foo'
    assert module.have_execute() is False
    assert module.have_destroy() is True

    ### def _execute_plugins_once()

    ### def _execute_plugins_in_single_mode()

    ### def _execute_plugins_in_threads_mode()

    ### class _PluginThread()

    ### class PluginInterface()
    conf = {
        'name':          'testplugin',
        'module':        'testplugin',
        'exec_period':   10,
        'fail_limit':    10,
        'plugin_metric': 'testplugin',
        'settings':      {'set1': 'bar', 'set3': 3},
    }
    interface = PluginInterface(conf)

    interface.sleep(10)
    assert interface.next_time - int(time.time()) > 5

    assert interface.settings == {'set1': 'bar', 'set3': 3}
    interface.apply_default_settings({"set1": "foo", "set2": 42},
                                     allow_strange=True)
    assert interface.settings == {'set1': 'bar', 'set2': 42, 'set3': 3}

    assert interface.need_metrics is False
    interface.i_need_metrics()
    assert interface.need_metrics is True

    interface.inbox = [(200, 'someplugin.bar', 'gauge', 24)]
    assert list(interface.recieve_metric()) == [{'mtime': 200,
                                                 'mname': 'someplugin.bar',
                                                 'mtype': 'gauge',
                                                 'mval': 24}]
    interface.send_metric(mtime=100, mname='foo', mval=42)
    assert interface.outbox == [(100, 'testplugin.foo', 'gauge', 42)]

    ### class Plugin()
    clear_tmpdir()
    Module.use_source_dirs(tmpdir)
    module_name = 'class_plugin'
    make_tmpfile(
        module_name + '.py',
        'default = {"set1": "foo", "set2": 42}\n'
        'def writethis(string):\n'
        '   with open("' + tmpdir + '/file", "w") as fd:\n'
        '       fd.write(string)\n'
        'def setup(pi):\n'
        '    pi.apply_default_settings(default, allow_strange=True)\n'
        '    writethis("setup")\n'
        'def execute(pi):\n'
        '    writethis("execute")\n'
        'def destroy(pi):\n'
        '    writethis("destroy")\n'
        'def test(pi):\n'
        '    return True\n'
    )
    conf = {
        'name':          module_name,
        'module':        module_name,
        'exec_period':   10,
        'fail_limit':    10,
        'plugin_metric': module_name,
        'settings':      {'set1': 'bar'},
    }

    plugin = Plugin(conf['name'])
    interface = PluginInterface(conf)
    plugin.interface = interface
    plugin.module = Module(conf['module'])

    assert len(list(Plugin.all())) == 1
    assert len(list(Plugin.collectors())) == 1

    assert plugin.test() is True

    assert interface.settings == {'set1': 'bar'}
    plugin.setup()
    assert interface.settings == {'set1': 'bar', 'set2': 42}
    assert read_tmpfile('file') == 'setup'

    plugin.execute()
    assert read_tmpfile('file') == 'execute'

    plugin.destroy()
    assert read_tmpfile('file') == 'destroy'
    assert len(list(Plugin.all())) == 0

    # ### class DataStorage()
    # LOG.setLevel(logging.DEBUG)
    # stordir = os.path.join(tmpdir, 'storage')
    # assert Storage.init(stordir) is True
    # A = Storage('plug1')
    # A.put([1, 2, 3])
    # assert A.get() == [1, 2, 3]
    # del(A)
    # A = Storage('plug1')
    # assert A.get() == [1, 2, 3]
    # clear_tmpdir()

    return 0


if __name__ == '__main__' and '--unittest' in sys.argv: sys.exit(_unittest())
