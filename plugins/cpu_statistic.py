#!/usr/bin/env python3
__doc__ = '''
CPU statistic. Parse /proc/stat.

----- settings -----
plugins:
  cpu_statistic:
    module: cpu_statistic
    settings:
      stat_file: /proc/stat  # Data source.
      total: true            # Report total counters.
      by_cores: false        # Report counters by cores.
      states:                # States for report.
#       - use                # Totaly use of cpu or core.
#       - idle               # Room heating.
        - user               # Normal processes executing in user mode.
#       - nice               # Niced processes executing in user mode.
        - system             # Processes executing in kernel mode.
#       - iowait             # Waiting for I/O to complete.
#       - irq                # Servicing interrupts.
#       - softirq            # Servicing softirqs.
      abs: false             # Output absolute values (ticks per second).
      prc: true              # Output values in percents.
      prec: 0                # Precision.
'''
import os
import sys
import pylegraf_lib as plib

DEFAULT_SETTINGS = {
    'stat_file': '/proc/stat',
    'total':     True,
    'by_cores':  False,
    'states':    ['user', 'system'],
    'abs':       False,
    'prc':       True,
    'prec':      0,
}
VALID_STATES = {'use', 'idle', 'user', 'nice', 'system', 'iowait', 'irq', 'softirq'}
PREVIOUS_STATISTIC = {}


def setup(pi):
    pi.apply_default_settings(DEFAULT_SETTINGS)
    s = pi.settings

    if not os.path.isfile(s['stat_file']):
        pi.log_critical('Wrong "stat_file".')
        return False

    if not s['total'] and not s['by_cores']:
        pi.log_critical('Select at least one of the options: "total", "by_cores".')
        return False

    s['states'] = [str(a).lower().strip() for a in s['states']]
    s['states'] = set(s['states']) - {''}
    wtf = s['states'] - VALID_STATES
    if wtf:
        pi.log_error('Unknown states "{}" will be ignored.'
                     .format(', '.join(wtf)))
        s['states'] = s['states'] - wtf

    if not s['abs'] and not s['prc']:
        pi.log_critical('Select at least one of the options: "abs", "prc".')
        return False


def execute(pi):
    s = pi.settings
    global PREVIOUS_STATISTIC

    statistic = read_file(s['stat_file'])
    if 'total' not in statistic or len(statistic) < 2:
        pi.log_critical('Wrong format of stat file.')
        return False

    if not s['by_cores']: statistic = {'total': statistic['total']}
    if not s['total']: del(statistic['total'])

    # If this is a first successfull execution, just save statistic.
    if pi.successes_in_row == 0:
        PREVIOUS_STATISTIC = statistic
        return True

    # In case of hot plugging processors, does the new processor
    # come with a new number or does the numbering break?

    # Calculate delta of statistic.
    delta = {}
    for cpu_name in statistic.keys():
        delta[cpu_name] = {}
        for field_name in statistic[cpu_name].keys():
            delta[cpu_name][field_name] = (
                statistic[cpu_name][field_name]
                - PREVIOUS_STATISTIC[cpu_name][field_name])

    # Out result
    for cpu_name, spu_statistic in delta.items():
        for field_name in s['states']:
            if s['abs'] and pi.sleep_time:
                mname = plib.join_metric((cpu_name, field_name, 'abs'))
                mval = delta[cpu_name][field_name]
                mval = round(mval / pi.sleep_time, s['prec'])
                if s['prec'] == 0: mval = int(mval)
                pi.send_metric(mname=mname, mval=mval)
            if s['prc']:
                mname = plib.join_metric((cpu_name, field_name, 'prc'))
                mval = plib.in_percents(delta[cpu_name][field_name],
                                        delta[cpu_name]['summ'],
                                        s['prec'])
                if s['prec'] == 0: mval = int(mval)
                pi.send_metric(mname=mname, mval=mval)

    PREVIOUS_STATISTIC = statistic
    return True


def read_file(name):
    with open(name, 'r') as fd: source_data = fd.readlines()
    statistic = {}
    for line in source_data:
        fields = line.split()
        if len(fields) < 8: continue
        if not fields[0].startswith('cpu'): continue
        cpu_name = fields[0]
        fields = tuple(map(int, fields[1:8]))
        counters = dict(zip(
            ('user', 'nice', 'system', 'idle', 'iowait', 'irq', 'softirq'),
            fields))
        counters['summ'] = sum(fields)
        counters['use'] = counters['summ'] - counters['idle']
        statistic[cpu_name] = counters
    statistic['total'] = statistic['cpu']
    del(statistic['cpu'])
    return statistic


def test(pi):

    statistic = read_file(DEFAULT_SETTINGS['stat_file'])
    if 'total' not in statistic or len(statistic) < 2:
        print('Wrong format of stat file.')
        return 1

    return True


#  CPU    USER     NICE  SYSTEM     IDLE IOWAIT IRQ SOFTIRQ
#  cpu 9071901 33283164 2285012 59988393 242328   0 5286275 0 0 0
# cpu0 2477796  3390853 1007077 26098391 126028   0 1861624 0 0 0
# cpu1 2179590 11530306  458028 10960739  38344   0 1276748 0 0 0
# cpu2 2559581  5023268  465592 13836693  46038   0 1392828 0 0 0
# cpu3 1854933 13338736  354315  9092568  31917   0  755074 0 0 0
