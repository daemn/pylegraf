#!/usr/bin/env python3
__doc__ = '''
Disk statistic. Parse /proc/diskstats.

----- settings -----
plugins:
  disk_statistic:
    module: disk_statistic
    settings:
      stat_file: /proc/diskstats  # Source file.
      opers: true                 # Operations.
      merge: false                # Merge operations.
      sectors: false              # Sectors.
      octets: true                # Octets.
      time: false                 # Time spent various operations.
      io: true                    # Time spent i/o.
      only_disks: true            # Show only hw disks.  ###TODO
      include: []                 # Include this devices.  ###TODO
      exclude: []                 # Exclude this devices.  ###TODO
      send_zeros: false           # Send metrics with zero value.
'''
import sys
import os
import pylegraf_lib as plib

DEFAULT_SETTINGS = {
    'stat_file':   '/proc/diskstats',
    'opers':      True,
    'merge':      False,
    'sectors':    False,
    'octets':     True,
    'time':       False,
    'io':         True,
    'only_disks': True,
    'include':    [],
    'exclude':    [],
    'send_zeros': False,
}
PREVIOUS_STATISTIC = {}
FIELD_NAMES = (  # fields in /proc/diskstats
    ('dev',     'major'),
    ('dev',     'minor'),
    ('dev',     'name'),
    ('opers',   'read'),
    ('merge',   'read'),
    ('sectors', 'read'),
    ('time',    'read'),
    ('opers',   'write'),
    ('merge',   'write'),
    ('sectors', 'write'),
    ('time',    'write'),
    ('io',      'quantity'),
    ('io',      'time'),
    ('io',      'weighted_time'),
    ('opers',   'discard'),
    ('merge',   'discard'),
    ('sectors', 'discard'),
    ('time',    'discard'),
    ('opers',   'flush'),
    ('time',    'flush'),
)
MAJOR_KEYS = {a[0] for a in FIELD_NAMES} | {'octets'}
# dev_name | major_key | minor_key | value
#   sda1   |  sectors  |   read    |  100


def setup(pi):
    pi.apply_default_settings(DEFAULT_SETTINGS)
    s = pi.settings

    if not os.path.isfile(s['stat_file']):
        pi.log_critical('Wrong "stat_file".')
        return False

    if not any((s['opers'], s['merge'], s['sectors'], s['octets'],
                s['time'], s['io'])):
        pi.log_critical('No kind of report was selected.')
        return False

    s['exclude'] = set([str(a).lower().strip() for a in s['exclude']]) - {''}

    return True


def execute(pi):
    s = pi.settings
    global PREVIOUS_STATISTIC

    statistic = read_file(s['stat_file'])

    # Filter devices

    # If this is a first successfull execution, just save statistic.
    if pi.successes_in_row == 0 or pi.sleep_time <= 0:
        PREVIOUS_STATISTIC = statistic
        return True

    # Collect data for sending.
    send_data = []
    for dev_name, dev_stat in statistic.items():
        try: prev_dev_stat = PREVIOUS_STATISTIC[dev_name]
        except KeyError: continue
        dev_metric = dev_stat['dev']['dev_metric']

        if s['time']:
            for minor_key, value in dev_stat['time'].items():
                mname = '.'.join((dev_metric, 'time', minor_key))
                prev_value = prev_dev_stat['time'][minor_key]
                mval = (value - prev_value) // pi.sleep_time
                send_data.append((mname, mval))

        if s['io']:
            # io.time (ms)
            mname = '.'.join((dev_metric, 'io', 'time'))
            prev_value = prev_dev_stat['io']['time']
            mval = (dev_stat['io']['time'] - prev_value) // pi.sleep_time
            send_data.append((mname, mval))
            # io.prc (%)
            mname = '.'.join((dev_metric, 'io', 'prc'))
            prev_value = prev_dev_stat['io']['time']
            mval = dev_stat['io']['time'] - prev_value
            mval = plib.in_percents(mval, pi.sleep_time * 1000, 1)
            send_data.append((mname, mval))
            # io.weighted_time (ms)
            mname = '.'.join((dev_metric, 'io', 'weighted_time'))
            prev_value = prev_dev_stat['io']['weighted_time']
            mval = (dev_stat['io']['weighted_time'] - prev_value) // pi.sleep_time
            send_data.append((mname, mval))
            # io.weighted_prc (%)
            mname = '.'.join((dev_metric, 'io', 'weighted_prc'))
            prev_value = prev_dev_stat['io']['weighted_time']
            mval = dev_stat['io']['weighted_time'] - prev_value
            mval = plib.in_percents(mval, pi.sleep_time * 1000, 1)
            send_data.append((mname, mval))
            # io.quantity
            mname = '.'.join((dev_metric, 'io', 'quantity'))
            mval = dev_stat['io']['quantity']
            send_data.append((mname, mval))

        for major_key in filter(lambda a: s[a],
                                ('opers', 'merge', 'sectors', 'octets')):
            for minor_key, value in dev_stat[major_key].items():
                mname = '.'.join((dev_metric, major_key, minor_key))
                prev_value = prev_dev_stat[major_key][minor_key]
                mval = (value - prev_value) / pi.sleep_time
                mval = round(mval, 1)
                send_data.append((mname, mval))

    # Send data
    for (mname, mval) in send_data:
        if not s['send_zeros'] and mval == 0: continue
        pi.send_metric(mname=mname, mval=mval)

    PREVIOUS_STATISTIC = statistic
    return True


def read_file(filename):
    statistic = {}
    with open(filename, 'r') as fd: lines = fd.readlines()
    for line in lines:
        fields = line.split()
        dev_name = fields[2]

        dev_stat = {a: {} for a in MAJOR_KEYS}
        for index, (key1, key2) in enumerate(FIELD_NAMES[:len(fields)]):
            try: dev_stat[key1][key2] = int(fields[index])
            except ValueError: continue
        dev_stat['octets'] = {k: v * 512 for k, v in dev_stat['sectors'].items()}
        dev_stat['dev']['dev_metric'] = plib.convert_to_metric(dev_name)

        statistic[dev_name] = dev_stat
    return statistic


def test(pi):
    A = read_file('/proc/diskstats')
    assert len(A) > 0
    return True


### /proc/diskstats
### 0  1     2     3     4     5     6     7    8     9    10 11     12     13 14 15 16 17   18   19
#
#   8 48   sdd   481     0    82    41     0    0     0     0  0    780     41  0  0  0  0    0    0
#   8 49  sdd1     7     0    46     2     0    0     0     0  0     44      2  0  0  0  0    0    0
#   8 16   sdb  9287  3857  5689 14655  2020  280 23198 19502  0 919352 282043  0  0  0  0 2769 7256
#   8 17  sdb1  8807  3857  5645 13465  1972  280 23198 17770  0 764084 191195  0  0  0  0    0    0
#   8  0   sda 99709 30459 25902  8655 14818  554 20212  1862  0 423836  29426  0  0  0  0 2519  258
#   8  1  sda1     5     0    40     0     0    0     0     0  0     00      0  0  0  0  0    0    0
#   8  2  sda2    47    56    17     0     2    0     2     0  0     40      0  0  0  0  0    0    0
#   8  3  sda3     7     0    44     4     0    0     0     0  0     40      4  0  0  0  0    0    0
#   8  4  sda4     9     0    42     5     0    0     0     0  0     84      5  0  0  0  0    0    0
#   8  5  sda5 26408 12445 18626  3405 14626  381 17574  1505  0 404532  18430  0  0  0  0    0    0
#   8  6  sda6 72753 17958  7240  5212    58  163  2646   252  0  25852   7714  0  0  0  0    0    0
#   8 32   sdc   480     0    92    72     0    0     0     0  0   1116     72  0  0  0  0    0    0
#   8 33  sdc1     5     0    40     3     0    0     0     0  0     24      3  0  0  0  0    0    0
# 254  0  dm-0 39422     0 18600  4880 18702    0 17674  2778  0 405524  32668  0  0  0  0    0    0
#   7  0 loop0    46     0    46     7     2    0     2     0  0     88      7  0  0  0  0    0    0
#   7  1 loop1     0     0     0     0     0    0     0     0  0      0      0  0  0  0  0    0    0
#   7  2 loop2     0     0     0     0     0    0     0     0  0      0      0  0  0  0  0    0    0

#  0  major     major number
#  1  minor     minor mumber
#  2  dev       device name
#  3  op_r      reads completed successfully
#  4  mrg_r     reads merged
#  5  sec_r     sectors read
#  6  time_r    time spent reading (ms)
#  7  op_w      writes completed
#  8  mrg_w     writes merged
#  9  sec_w     sectors written
# 10  time_w    time spent writing (ms)
# 11  io_quan   I/Os currently in progress
# 12  io_time   time spent doing I/Os (ms)
# 13  io_wtime  weighted time spent doing I/Os (ms)
# 14  op_d      discards completed successfully
# 15  mrg_d     discards merged
# 16  sec_d     sectors discarded
# 17  time_d    time spent discarding
# 18  op_f      flush requests completed successfully
# 19  time_f    time spent flushing

### Все показатели, кроме io.quantity, всреднем за секунду.
# opers.read       op_r        Операций чтения
# opers.write      op_w        Операций записи
# opers.discard    op_d        Операций отмены
# opers.flush      op_f        Операций очистки
# merge.read       mrg_r       Операций чтения совмещено
# merge.write      mrg_w       Операций записи совмещено
# merge.discard    mrg_d       Операций отмены совмещено
# sectors.read     sec_r       Секторов прочитано
# sectors.write    sec_w       Секторов записано
# sectors.discard  sec_d       Секторов сброшено
# octets.read      sec_r * 512 Байт прочитано
# octets.write     sec_w * 512 Байт записано
# octets.discard   sec_d * 512 Байт сброшено
# time.read        time_r      Время, потраченное на чтение (ms)
# time.write       time_w      Время, потраченное на запись (ms)
# time.discard     time_d      Время, потраченное на сброс (ms)
# time.flush       time_f      Время, потраченное на очистку (ms)
# io.time          io_time     Общее время ввода/вывода (ms)
# io.prc           io_time     Общее время ввода/вывода (%)
# io.weighted_time io_wtime    Сумма времени всех операций ввода/вывода (ms)
# io.weighted_prc  io_wtime    Сумма времени всех операций ввода/вывода (%)
# io.quantity      io_quan     Текущее количество операций ввода/вывода
