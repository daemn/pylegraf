#!/usr/bin/env python3
__doc__ = '''
Just print all metrics on screen.

----- settings -----
plugins:
  echo:
    module: echo
    settings:
      save_to_file: ''  # Out notes to file instead screen.
      out_only: []      # Out only these notes. List of regular expressions is allowed.
'''
import re
re_only = []


# Function "setup" used for check settings and setup plugin environment.
# Link to PluginInterface will where given as argument.
def setup(pi):
    global re_only

    # Merge default and users settings.
    default_settings = {
        'save_to_file': '',
        'out_only': [],
    }
    if not pi.apply_default_settings(default_settings): return False
    s = pi.settings

    # Check settings and prepare for use.
    if s['out_only']:
        for regexp in map(str, s['out_only']):
            re_only.append(re.compile(regexp))

    # Method i_need_metrics register the plugin as reciever of metrics.
    pi.i_need_metrics()


# Function "execute" is requred. It used for collecting or processing
# (sending) metrics.
# Link to PluginInterface will where given as argument.
def execute(pi):

    # Recieve metrics from inbox.
    notes = list(pi.recieve_metric())

    # Filter metrics if needed.
    if re_only: notes = list(filter(is_allowed, notes))

    # Transform notes.
    rows = []
    for note in notes:
        rows.append(' | '.join([str(note[a])
                               for a in ('mtime', 'mname', 'mtype', 'mval')]))

    # Out metrics.
    if pi.settings['save_to_file']:
        with open(pi.settings['save_to_file'], 'a') as fd:
            for row in rows: fd.write(row + '\n')
    else:
        for row in rows: print(row)


def is_allowed(note):
    '''Return True if note is allowed by filter.
    '''
    for regexp in re_only:
        if regexp.search(note['mname']): return True
    return False
