#!/usr/bin/env python3
__doc__ = '''
Execute script and interpret out as metrics.
Work in Python version 3.5 and more.

Valid script output rows formats:

METRIC VALUE           | Two fields is METRIC and VALUE always.
METRIC VALUE TYPE      | If third field is string, then this is TYPE.
TIME METRIC VALUE      | If third field is number, then first field is TIME.
TIME METRIC VALUE TYPE | Four fields is TIME, METRIC, VALUE and TYPE always.
# anything             | If first char is '#', then this is comment.

TIME is seconds since start of Unix epoch (01.01.1970).
METRIC is string or number. Alphabeticals, numbers, dot and underline is allowed.
VALUE is integer or float number.
TYPE is 'c' (count) or 'g' (gauge). Default TYPE is "gauge".
Comments and incomprehensible rows will be ignored.
You can use single or double quotes for fields. Eg in case of empty METRIC.

----- settings -----
plugins:
  execute_script:
    module: execute_script
    settings:
      script:  ''  # Some script for execute.
      timeout: 30  # Stop script after <timeout> seconds.
'''
import sys
import subprocess
from subprocess import PIPE
import pylegraf_lib as plib

DEFAULT_SETTINGS = {
    'script':  '',
    'timeout': 7,
}


def setup(pi):
    py_version = float(str(sys.version_info.major) + '.' +
                       str(sys.version_info.minor))
    if py_version < 3.5:
        pi.log_critical('Too old python version. Require python >= 3.5')
        return False

    pi.apply_default_settings(DEFAULT_SETTINGS)

    pi.settings['script'] = pi.settings['script'].strip().split()
    if not pi.settings['script']:
        pi.log_critical('Script is not defined.')
        return False

    if pi.settings['timeout'] < 1:
        pi.log_warning('Too small timeout. Use default value.')
        pi.settings['timeout'] = DEFAULT_SETTINGS['timeout']

    return True


def execute(pi):
    # script_out = subprocess.run(pi.settings['script'],
    #                             capture_output=True,
    #                             timeout=pi.settings['timeout'])
    script_out = subprocess.run(pi.settings['script'],
                                stdout=PIPE,
                                stderr=PIPE,
                                timeout=pi.settings['timeout'])
    out_lines = script_out.stdout.decode().split('\n')
    for line in out_lines:
        metric = search_metric(line)
        if metric: pi.send_metric(**metric)


def search_metric(row):
    row = row.strip()
    if row.startswith('#'): return None
    fields = [a.strip('\'"').lower() for a in row.split()]
    mtime = None
    mtype = 'g'
    try:
        if len(fields) == 2:
            mname, mval = fields[:2]
        elif len(fields) == 3:
            if fields[2][0] in 'cg': mname, mval, mtype = fields[:3]
            else: mtime, mname, mval = fields[:3]
        elif len(fields) == 4:
            mtime, mname, mval, mtype = fields[:4]
        else:
            raise ValueError
        if mtime is not None: mtime = int(mtime)
        mval = plib.mask_chars(mval, ',', '.')
        if mval.find('.') > -1: mval = float(mval)
        else: mval = int(mval)
        if mtype[0] == 'c': mtype = 'count'
        elif mtype[0] == 'g': mtype = 'gauge'
        else: raise ValueError
    except(AttributeError, TypeError, IndexError, ValueError):
        return None
    return {'mtime': mtime, 'mname': mname, 'mval': mval, 'mtype': mtype}


def test(pi):
    f = search_metric
    for a, b in (
        ['testplg.foo.bar 10 g', (None, 'testplg.foo.bar', 10,    'gauge')],
        ['testplg.wroom 20',     (None, 'testplg.wroom',   20,    'gauge')],
        ['foo 11',               (None, 'foo',             11,    'gauge')],
        ['bar 11.22',            (None, 'bar',             11.22, 'gauge')],
        ['123 foo 11',           (123,  'foo',             11,    'gauge')],
        ['123 "" 3',             (123,  '',                3,     'gauge')],
        ['bar 11.22 c',          (None, 'bar',             11.22, 'count')],
    ):
        assert f(a) == {
            'mtime': b[0], 'mname': b[1], 'mval': b[2], 'mtype': b[3]}
    for a in (
        'no metrics',
        '# comment',
        '',
        '3',
        '"no" ""',
        'a a a',
        '',
        '3 4 5 6',
    ):
        assert f(a) is None

    return True
