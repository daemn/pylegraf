#!/usr/bin/env python3
__doc__ = '''
Report state of filesystems space.

----- settings -----
plugins:
  filesystem_space:
    module: filesystem_space
    settings:
      mounts_file: /proc/mounts  # Path to mounts file. Default is '/proc/mounts'.
      rw_only: true         # Report only rw filesystems.
      include_device: []    # Don`t exclude this devices.
      include_path: []      # Don`t exclude this mount points.
      include_fs: []        # Don`t exclude this filesystems.
      exclude_device: []    # Exclude this devices.
      exclude_path: []      # Exclude this mount points.
      exclude_fs: []        # Exclude this filesystems.
      metric_name_is: path  # Metric name base on "device", "device_path" or "path".
      root_path_alias: rootdir  # Name mount point "/" as this alias.
      bytes: true           # Report about bytes.
      inodes: false         # Report about inodes.
      used: true            # Resource used.
      free: false           # Resource free for all.
      avail: false          # Available for unprivileged users.
      abs: false            # Report absolutly values.
      prc: true             # Report percents.
      prec: 1               # Round precision.
      send_zeros: false     # Report values equal zeros.
'''
import pylegraf_lib as plib
import sys
import os

DEFAULT_SETTINGS = {
    'mounts_file':     '/proc/mounts',
    'rw_only':         True,
    'include_device':  [],
    'include_path':    [],
    'include_fs':      [],
    'exclude_device':  [],
    'exclude_path':    [],
    'exclude_fs':      [],
    'metric_name_is':  'path',
    'root_path_alias': 'rootdir',
    'bytes':      True,
    'inodes':     False,
    'used':       True,
    'free':       False,
    'avail':      False,
    'abs':        False,
    'prc':        True,
    'prec':       1,
    'send_zeros': False,
}
CACHE = {}
REPORT_METRICS = []


def setup(pi):
    global REPORT_METRICS
    pi.apply_default_settings(DEFAULT_SETTINGS)
    s = pi.settings

    for a in ('include_device', 'include_path', 'include_fs',
              'exclude_device', 'exclude_path', 'exclude_fs'):
        s[a] = set(map(str, s[a])) - {''}

    s['metric_name_is'] = str(s['metric_name_is']).strip()
    if s['metric_name_is'] not in ('device', 'device_path', 'path'):
        pi.log_error('Wrong "metric_name_is" option. Use default -- "path".')
        s['metric_name_is'] = 'path'

    if s['metric_name_is'] == 'path':
        s['root_path_alias'] = str(s['root_path_alias']).strip()
        s['root_path_alias'] = plib.convert_to_metric(s['root_path_alias'])
        if not s['root_path_alias']:
            pi.log_critical('Wrong name of the root mount point.')
            return False

    if (not any([s['bytes'], s['inodes']]) or not
            any([s['used'], s['free'], s['avail']]) or not
            any([s['abs'], s['prc']])):
        pi.log_critical('Nothing to report.')
        return False

    for bi in ('bytes', 'inodes'):
        for afu in ('avail', 'free', 'used'):
            for ap in ('abs', 'prc'):
                if all((s[bi], s[afu], s[ap])):
                    REPORT_METRICS.append({
                        'name': '.'.join([bi, afu, ap]),
                        'bi':   bi,
                        'afu':  afu,
                        'ap':   ap,
                    })
    if not REPORT_METRICS:
        pi.log_critical('No one metric was selected.')
        return False

    pi.log_debug('Selected metrics: "{}"'
                 .format(', '.join([a['name'] for a in REPORT_METRICS])))


def execute(pi):
    global CACHE
    s = pi.settings

    # Get mounts
    mount_lines = read_mount_lines(s['mounts_file'])

    # Get details of mounts
    mounts = {}
    for line in mount_lines:
        line = line.strip()
        if line in CACHE: mounts[line] = CACHE[line]
        else: mounts[line] = get_mount_info(line)

    # Use user filters
    for mount in mounts.values():
        if not mount['report']: continue
        if s['rw_only'] and not mount['rw']:
            mount['report'] = False
            continue
        if (mount['device'] in s['include_device']
                or mount['path'] in s['include_path']
                or mount['fs'] in s['include_fs']):
            continue
        if (mount['device'] in s['exclude_device']
                or mount['path'] in s['exclude_path']
                or mount['fs'] in s['exclude_fs']):
            mount['report'] = False

    # Get filesystem status
    for mount in mounts.values():
        if mount['report']: mount = get_filesystem_status(mount)

    # Make metric names
    used_mount_names = []
    for mount in mounts.values():
        if not mount['report']: continue
        if 'name' in mount: used_mount_names.append(mount['name'])
    for mount in mounts.values():
        if not mount['report']: continue
        if 'name' in mount: continue
        mount['name'] = mount[s['metric_name_is']].lstrip('/')
        if s['metric_name_is'] == 'path' and mount['path'] == '/':
            mount['name'] = s['root_path_alias']
        mount['name'] = plib.convert_to_metric(mount['name'])
        mount['name'] = plib.pick_up_an_unused_name(
            mount['name'], used_mount_names)
        used_mount_names.append(mount['name'])

    CACHE = mounts

    # Report
    for mount in mounts.values():
        if not mount['report']: continue
        for metric in REPORT_METRICS:
            if metric['bi'] == 'inode' and not mount['inode']['size']: continue
            mname = '.'.join((mount['name'], metric['name']))
            if metric['ap'] == 'abs':
                mval = mount[metric['bi']][metric['afu']]
            else:
                mval = plib.in_percents(
                    mount[metric['bi']][metric['afu']],
                    mount[metric['bi']]['size'],
                    s['prec'])
                if s['prec'] < 1: mval = int(mval)
            if not s['send_zeros'] and mval == 0: continue
            pi.send_metric(mname=mname, mval=mval)


def read_mount_lines(filename='/proc/mounts'):
    r'''Return mount list from /proc/mounts as default.
    '''
    with open(filename, 'r') as fd: return(fd.readlines())


def get_mount_info(mount_line):
    r'''Parse string in /proc/mounts format.
    '''
    fields = mount_line.split()
    mount = {}
    mount['device_path'] = fields[0]
    mount['device']  = os.path.basename(mount['device_path'])
    mount['path']    = fields[1]
    mount['fs']      = fields[2]
    mount['options'] = fields[3].split(',')
    mount['line']    = mount_line
    mount['rw']      = not ('ro' in mount['options'])
    mount['report']  = True
    return mount


def get_filesystem_status(mount):
    r'''Request filesystem info.
    '''
    stat = os.statvfs(mount['path'])
    if stat.f_blocks == 0:
        mount['report'] = False
        return mount

    blsize = stat.f_bsize
    mount['bytes']  = {}
    mount['inodes'] = {}
    mount['bytes']['size']   = stat.f_blocks * blsize
    mount['bytes']['avail']  = stat.f_bavail * blsize
    mount['bytes']['free']   = stat.f_bfree * blsize
    mount['bytes']['used']   = mount['bytes']['size'] - mount['bytes']['free']
    mount['inodes']['size']  = stat.f_files
    mount['inodes']['avail'] = stat.f_favail
    mount['inodes']['free']  = stat.f_ffree
    mount['inodes']['used']  = stat.f_files - stat.f_ffree
    return mount


def test(pi):

    mount_lines = read_mount_lines('/proc/mounts')
    if not mount_lines: return 1
    for line in mount_lines:
        mount = get_mount_info(line)
        if mount['report']: mount = get_filesystem_status(mount)

    return True


### local, not tmp filesystems
#
# $ sudo df -hTlP --no-sync -x tmpfs -x devtmpfs
# $ sudo df -l -x tmpfs -x devtmpfs --output=source,target | tail -n +2

### os.statvfs('/home').<attr_name>
#
# f_bsize=4096, f_frsize=4096,
# f_blocks=25065746, f_bfree=21027571, f_bavail=19743527,
# f_files=6406144, f_ffree=6016068, f_favail=6016068,
# f_flag=4096, f_namemax=255

### man 3 statvfs
#
# unsigned long  f_bsize    Filesystem block size
# unsigned long  f_frsize   Fragment size
# fsblkcnt_t     f_blocks   Size of fs in f_frsize units
# fsblkcnt_t     f_bfree    Number of free blocks
# fsblkcnt_t     f_bavail   Number of free blocks for unprivileged users
# fsfilcnt_t     f_files    Number of inodes
# fsfilcnt_t     f_ffree    Number of free inodes
# fsfilcnt_t     f_favail   Number of free inodes for unprivileged users
# unsigned long  f_fsid     Filesystem ID
# unsigned long  f_flag     Mount flags
# unsigned long  f_namemax  Maximum filename length

### cat /proc/mounts
#
# sysfs                  /sys                            sysfs      rw,nosuid,nodev,noexec,relatime
# proc                   /proc                           proc       rw,nosuid,nodev,noexec,relatime
# udev                   /dev                            devtmpfs   rw,nosuid,noexec,relatime,size=8147060k,nr_inodes=2036765,mode=755
# devpts                 /dev/pts                        devpts     rw,nosuid,noexec,relatime,gid=5,mode=620,ptmxmode=000
# tmpfs                  /run                            tmpfs      rw,nosuid,nodev,noexec,relatime,size=1633904k,mode=755
# /dev/mapper/gvssd-root /                               ext4       rw,relatime,errors=remount-ro
# securityfs             /sys/kernel/security            securityfs rw,nosuid,nodev,noexec,relatime
# tmpfs                  /dev/shm                        tmpfs      rw,nosuid,nodev
# tmpfs                  /run/lock                       tmpfs      rw,nosuid,nodev,noexec,relatime,size=5120k
# tmpfs                  /sys/fs/cgroup                  tmpfs      ro,nosuid,nodev,noexec,size=4096k,nr_inodes=1024,mode=755
# cgroup2                /sys/fs/cgroup/unified          cgroup2    rw,nosuid,nodev,noexec,relatime,nsdelegate
# cgroup                 /sys/fs/cgroup/systemd          cgroup     rw,nosuid,nodev,noexec,relatime,xattr,name=systemd
# pstore                 /sys/fs/pstore                  pstore     rw,nosuid,nodev,noexec,relatime
# efivarfs               /sys/firmware/efi/efivars       efivarfs   rw,nosuid,nodev,noexec,relatime
# none                   /sys/fs/bpf                     bpf        rw,nosuid,nodev,noexec,relatime,mode=700
# cgroup                 /sys/fs/cgroup/devices          cgroup     rw,nosuid,nodev,noexec,relatime,devices
# cgroup                 /sys/fs/cgroup/cpuset           cgroup     rw,nosuid,nodev,noexec,relatime,cpuset
# cgroup                 /sys/fs/cgroup/freezer          cgroup     rw,nosuid,nodev,noexec,relatime,freezer
# cgroup                 /sys/fs/cgroup/perf_event       cgroup     rw,nosuid,nodev,noexec,relatime,perf_event
# cgroup                 /sys/fs/cgroup/net_cls,net_prio cgroup     rw,nosuid,nodev,noexec,relatime,net_cls,net_prio
# cgroup                 /sys/fs/cgroup/rdma             cgroup     rw,nosuid,nodev,noexec,relatime,rdma
# cgroup                 /sys/fs/cgroup/cpu,cpuacct      cgroup     rw,nosuid,nodev,noexec,relatime,cpu,cpuacct
# cgroup                 /sys/fs/cgroup/pids             cgroup     rw,nosuid,nodev,noexec,relatime,pids
# cgroup                 /sys/fs/cgroup/blkio            cgroup     rw,nosuid,nodev,noexec,relatime,blkio
# cgroup                 /sys/fs/cgroup/memory           cgroup     rw,nosuid,nodev,noexec,relatime,memory
# systemd-1              /proc/sys/fs/binfmt_misc        autofs     rw,relatime,fd=28,pgrp=1,timeout=0,minproto=5,maxproto=5,direct,pipe_ino=13021
# mqueue                 /dev/mqueue                     mqueue     rw,nosuid,nodev,noexec,relatime
# hugetlbfs              /dev/hugepages                  hugetlbfs  rw,relatime,pagesize=2M
# debugfs                /sys/kernel/debug               debugfs    rw,nosuid,nodev,noexec,relatime
# tracefs                /sys/kernel/tracing             tracefs    rw,nosuid,nodev,noexec,relatime
# /dev/sdb1              /home                           ext4       rw,relatime
# /dev/sda2              /boot/efi                       vfat       rw,relatime,fmask=0077,dmask=0077,codepage=437,iocharset=ascii,shortname=mixed,utf8,errors=remount-ro
# //10.10.10.10/share    /mnt/share                      cifs       rw,nosuid,nodev,relatime,vers=3.1.1,cache=strict,username=daemn,uid=1000,forceuid,gid=1000,forcegid,addr=10.10.10.10,file_mode=0755,dir_mode=0755,soft,nounix,serverino,mapposix,rsize=4194304,wsize=4194304,bsize=1048576,echo_interval=60,actimeo=1
# tmpfs                  /run/user/115                   tmpfs      rw,nosuid,nodev,relatime,size=1633900k,nr_inodes=408475,mode=700,uid=115,gid=122
# tmpfs                  /run/user/1000                  tmpfs      rw,nosuid,nodev,relatime,size=1633900k,nr_inodes=408475,mode=700,uid=1000,gid=1000
# fusectl                /sys/fs/fuse/connections        fusectl    rw,nosuid,nodev,noexec,relatime
# nsfs                   /run/docker/netns/eda65cf07e73  nsfs       rw
# nsfs                   /run/docker/netns/dfee11e80a47  nsfs       rw
# overlay                /var/lib/docker/.../merged      overlay    rw,relatime,lowerdir=/var/lib/docker/overlay2/l/OJIEJKBXGFD7TJVDK3YJYRPVEF:/var/lib/docker/overlay2/l/6BNHJTS7MJPP4OYPKHQAQUTYZD:/var/lib/docker/overlay2/l/KYY7HG567NYCS7ONBXGSDNDSUF:/var/lib/docker/overlay2/l/75ACJVQDAYFCGWCALCBFXNCMM5:/var/lib/docker/overlay2/l/ZDP224CXXE3UCNX5LWYTFWQ7JB:/var/lib/docker/overlay2/l/NFJMGUTORP237SQFXWZNJGP2QC:/var/lib/docker/overlay2/l/TO5URSRRZVV4QI7MFE367JJTT4,upperdir=/var/lib/docker/overlay2/9b062479165e81118e244f06eca7c457d342b96243578354aa618eeab9dfad3e/diff,workdir=/var/lib/docker/overlay2/9b062479165e81118e244f06eca7c457d342b96243578354aa618eeab9dfad3e/work
# overlay                /var/lib/docker/.../merged      overlay    rw,relatime,lowerdir=/var/lib/docker/overlay2/l/YH6DHYJUNZU3X7VDB4EYPO75Y6:/var/lib/docker/overlay2/l/ECN67QWAP2OHEFXLXGGQWNPKUL:/var/lib/docker/overlay2/l/54XDCTYT4TKGXR2CVC4HHMRJ6F:/var/lib/docker/overlay2/l/3723XTIK7TNTLYNGESOAQCQDYB:/var/lib/docker/overlay2/l/TO5URSRRZVV4QI7MFE367JJTT4,upperdir=/var/lib/docker/overlay2/99122b8d9804dc5d6ae8d2628770b1a6c96b08ddd21afb90c707e1422deb6224/diff,workdir=/var/lib/docker/overlay2/99122b8d9804dc5d6ae8d2628770b1a6c96b08ddd21afb90c707e1422deb6224/work
# shm                    /var/lib/docker/.../mounts/shm  tmpfs      rw,nosuid,nodev,noexec,relatime,size=65536k
# shm                    /var/lib/docker/.../mounts/shm  tmpfs      rw,nosuid,nodev,noexec,relatime,size=65536k
