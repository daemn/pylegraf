#!/usr/bin/env python3
__doc__ = '''
li.ru parser. li.ru -- liveinternet.ru short statistic page.

----- settings -----
plugins:
  liru_parser:
    module: liru_parser
    exec_period: 23
    settings:
      url: 'https://www.li.ru/stat/'  # Liveinternet URL
      headers:               # HTTP headers.
        Accept-Language: en
        User-Agent: >-
          Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5)
          AppleWebKit/537.36 (KHTML, like Gecko)
          Chrome/32.0.1700.77 Safari/537.36
      login: ''              # Liveinternet login.
      password: ''           # Liveinternet password.
      save_bad_pages: false  # Save strange pages for investigations.
      reports:
#       - pageviews          # Quantity of loadings of site pages.
        - pageviews_per_min  # Quantity of loadings of site pages per one minute.
#       - sessions           # Series of several pageviews from one visitor,
                             # between adjacent reviews there must be not more
                             # than 15 minutes.
#       - visitors           # Quantity of unique site visitors (more precisely,
                             # quantity of browsers). Identification takes place
                             # with the help of cookies.
        - visitors_per_min   # Quantity of unique site visitors per one minute.
#       - hosts              # Quantity of unique IP-addresses, from which the
                             # site pages were requested. Several computers on
                             # one proxy-server are considered to be one host.
#       - hosts_per_min      # Quantity of unique IP-addresses per one minute.
#       - reloads            # Repeated loadings of one page by the same visitor
                             # during 1 second. Not with standing the fact that
                             # this parameter is segregated into a separate
                             # string, reloads are included into “pageviews”.
#       - reloads_per_min    # Reloadings of one page by the same visitor per
                             # one minute.
#       - average_online          # Average daily quantity of visitors per 15
                                  # minutes.
#       - average_active_online   # Average daily quantity of visitors per 15
                                  # minutes, who reviewed more than one page per
                                  # session.
#       - average_duration_min    # Average duration of sessions in minutes.
                                  # Session duration is a time interval between
                                  # the first and the last session pageview.
                                  # Session duration from one pageview equals
                                  # zero.
#       - pageviews_on_a_visitor  # Average quantity of pageviews made by one
                                  # visitor. It is calculated as a difference of
                                  # total quantity of pageviews, divided on
                                  # quantity of unique visitors.
'''
import pylegraf_lib as plib
import requests
import re
import os
import time
import pickle
DEFAULT = {
    'url': 'https://www.li.ru/stat/',
    'headers': {
        'Accept-Language': 'en',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5)'
                      'AppleWebKit/537.36 (KHTML, like Gecko)'
                      'Chrome/32.0.1700.77 Safari/537.36',
    },
    'login':    '',
    'password': '',
    'save_bad_pages': False,
    'reports': ['pageviews_per_min', 'visitors_per_min']
}
VALID_REPORTS = {'pageviews',
                 'pageviews_per_min',
                 'sessions',
                 'visitors',
                 'visitors_per_min',
                 'hosts',
                 'hosts_per_min',
                 'reloads',
                 'reloads_per_min',
                 'average_online',
                 'average_active_online',
                 'average_duration_min',
                 'pageviews_on_a_visitor'}
RE = {}
for name, regexp in (
        ('table',     r'<table.+?</table>'),
        ('today_page', r'difference with the yesterday value at the same time'),
        ('pageviews', r'>Pageviews<.+?>([0-9,.]+)<'),
        ('sessions',  r'>Sessions<.+?>([0-9,.]+)<'),
        ('visitors',  r'>Visitors<.+?>([0-9,.]+)<'),
        ('hosts',     r'>Hosts<.+?>([0-9,.]+)<'),
        ('reloads',   r'>Reloads<.+?>([0-9,.]+)<'),
        ('average_online',         r'>Average online<.+?>([0-9,.]+)<'),
        ('average_active_online',  r'>Average active online<.+?>([0-9,.]+)<'),
        ('average_duration_min',   r'>Average duration \(min\)<.+?>([0-9,.]+)<'),
        ('pageviews_on_a_visitor', r'>Pageviews/visitor<.+?>([0-9,.]+)<')):
    RE[name] = re.compile(regexp, re.DOTALL)
CACHE = {}
MAXIMUM = {}
STORAGE = '/tmp/liru_parser'
FAIL_NUMBER = 0


def setup(pi):
    global CACHE

    pi.apply_default_settings(DEFAULT)
    s = pi.settings

    if not s['url']:
        pi.log_critical('Empty url.')
        return False

    if not s['login']:
        pi.log_critical('Empty login.')
        return False

    if not s['password']:
        pi.log_critical('Empty password.')
        return False

    for header_name in s['headers'].keys():
        s['headers'][header_name] = str(s['headers'][header_name]).strip()

    # Check selected reports.
    s['reports'] = set([str(a).strip() for a in s['reports']]) - {''}
    wtf = s['reports'] - VALID_REPORTS
    if wtf:
        pi.log_warning('Unknown reports "{}" will be ignored.'.format(', '.join(wtf)))
        s['reports'] -= wtf
    if not s['reports']:
        pi.log_critical('Select at least one report.')

    # Make url for request statistic.
    s['url'] = '{0[url]}?nograph=yes&url={0[login]}&password={0[password]}'.format(s)

    # Prepare storage directory and check saved cache.
    if not os.path.isdir(STORAGE): os.makedirs(STORAGE)
    if os.path.isfile(STORAGE + '/cache.pickle'):
        try:
            with open(STORAGE + '/cache.pickle', 'rb') as fd:
                CACHE = pickle.load(fd)
        except(OSError, EnvironmentError, pickle.UnpicklingError): pass

    pi.sleep(3)
    return True


def execute(pi):
    s = pi.settings
    global STAT
    global FAIL_NUMBER

    # Request statistic page.
    page_text = request_page(url=s['url'], headers=s['headers'])
    current_time = int(time.time())
    current_min = int(current_time / 60) * 60
    previous_min = current_min - 60
    if page_text is None:
        pi.log_error('Download page error.')
        FAIL_NUMBER += 1
        if FAIL_NUMBER < 30: pi.sleep(5)
        else: pi.sleep(300)
        return

    # Search statistic data in html page.
    current_stat = get_statistic(page_text)
    if current_stat is None:
        pi.log_error('No statistic on the page ({}.html).'.format(current_time))
        FAIL_NUMBER += 1
        if FAIL_NUMBER < 30: pi.sleep(10)
        else: pi.sleep(300)
        if s['save_bad_pages']:
            bad_pages_dir = STORAGE + '/bad_pages_' + str(current_time)[:5]
            if not os.path.isdir(bad_pages_dir): os.makedirs(bad_pages_dir)
            bad_page_file = bad_pages_dir + '/' + str(current_time) + '.html'
            with open(bad_page_file, 'w') as df: df.write(page_text)
        return

    FAIL_NUMBER = 0

    # Save current statistic in cache.
    CACHE[current_min] = {'time': current_time, 'stat': current_stat}
    for key in list(CACHE.keys()):
        if key != previous_min and key != current_min: del(CACHE[key])

    # Calculate statistic per minute.
    if len(CACHE) == 2:
        previous_stat = CACHE[previous_min]['stat']
        stat_age = CACHE[current_min]['time'] - CACHE[previous_min]['time']
    else:
        previous_stat = {
            'pageviews': None,
            'sessions':  None,
            'visitors':  None,
            'hosts':     None,
            'reloads':   None,
            'average_online':         None,
            'average_active_online':  None,
            'average_duration_min':   None,
            'pageviews_on_a_visitor': None,
        }
        stat_age = None
    for report in ['pageviews_per_min',
                   'visitors_per_min',
                   'hosts_per_min',
                   'reloads_per_min']:
        src_report = report.rstrip('_per_min')
        try: delta = current_stat[src_report] - previous_stat[src_report]
        except TypeError:
            current_stat[report] = None
            continue
        if delta < 0: delta = current_stat[src_report]
        current_stat[report] = delta / (stat_age / 60)

    # Temporary checking of pageviews_per_min.  ###DEBUG
    if (current_stat['pageviews_per_min'] is not None
            and current_stat['pageviews_per_min'] > 3000):
        pi.log_error('Too much pageviews per minite ({}.html).'.format(current_time))
        FAIL_NUMBER += 1
        if FAIL_NUMBER < 30: pi.sleep(10)
        else: pi.sleep(300)
        if s['save_bad_pages']:
            bad_pages_dir = STORAGE + '/bad_pages_' + str(current_time)[:5]
            if not os.path.isdir(bad_pages_dir): os.makedirs(bad_pages_dir)
            bad_page_file = bad_pages_dir + '/' + str(current_time) + '.html'
            with open(bad_page_file, 'w') as df: df.write(page_text)
        return

    # Report statistic.
    for report in current_stat.keys():
        if report in s['reports'] and current_stat[report] is not None:
            mname = plib.join_metric((report, 'avg'))
            mval  = current_stat[report]
            pi.send_metric(mtime=current_min, mname=mname, mval=mval, mtype='gauge')

    return True


def request_page(url, headers):
    r'''Return requested web page or None.
    '''
    try: response = requests.get(url, headers=headers)
    except Exception: return None
    if not response.ok: return None
    return response.text


def get_statistic(text):
    r'''Search statistic data in html page.
    '''
    # Search table.
    table_text = None
    while text:
        table_match = RE['table'].search(text)
        if table_match is None: return None
        if RE['pageviews'].search(table_match.group(0)):
            table_text = table_match.group(0)
            break
        text = text[table_match.end():]

    # Is this today page?
    if RE['today_page'].search(table_text) is None: return None

    # Fetch statistic from the table.
    stat = {
        'pageviews': None,
        'sessions':  None,
        'visitors':  None,
        'hosts':     None,
        'reloads':   None,
        'average_online':         None,
        'average_active_online':  None,
        'average_duration_min':   None,
        'pageviews_on_a_visitor': None,
    }
    for report in stat.keys():
        match = RE[report].search(table_text)
        if match is None: continue
        try: stat[report] = to_number(match.group(1))
        except ValueError: continue

    return stat


def to_number(string):
    r'''Convert string to number ("34,456.2" -> 34456.2).
    '''
    string = str(string).replace(',', '')
    if string.find('.') > -1: return float(string)
    else: return int(string)


def destroy(pi):
    try:
        with open(STORAGE + '/cache.pickle', 'wb') as fd:
            pickle.dump(CACHE, fd, pickle.HIGHEST_PROTOCOL)
    except(OSError, EnvironmentError, pickle.UnpicklingError): pass


def test(pi):
    from copy import deepcopy
    # from pprint import pprint  ###DEBUG

    ### def get_statistic()

    assert get_statistic(
        '<table cellpadding=3 cellspacing=0 border=0 bgcolor=#f0f0f0>'
        '<tr><td bgcolor=#ffffff colspan=3><img  '
        'src="/i/_.gif" width=220 height=1 alt=""></td>'
        '<tr align=right bgcolor="#e4e4e4">'
        '<td><input type=checkbox name=id value="0" checked></td>'
        '<td align=left>Pageviews</td>\n\n'
        '<td>492,614</td>'
        '<tr align=right>'
        '<td><input type=checkbox name=id value="5"></td>'
        '<td align=left>Sessions</td>'
        '<td>145,839</td>'
        '<tr align=right bgcolor="#e4e4e4">'
        '<td><input type=checkbox name=id value="8" checked></td>'
        '<td align=left>Visitors</td>'
        '<td>119,541</td>'
        '<tr align=right>'
        '<td><input type=checkbox name=id value="7"></td>'
        '<td align=left>Hosts</td>'
        '<td>112,101</td>'
        '<tr align=right bgcolor="#e4e4e4">'
        '<td><input type=checkbox name=id value="4"></td>'
        '<td align=left>Reloads</td>'
        '<td>604</td>'
        '<tr align=right>'
        '<td><input type=checkbox name=id value="9"></td>'
        '<td align=left>Average online</td>'
        '<td>1,823</td>'
        '<tr align=right bgcolor="#e4e4e4">'
        '<td><input type=checkbox name=id value="10"></td>'
        '<td align=left>Average active online</td>'
        '<td>1,126</td>'
        '<tr align=right>'
        '<td><input type=checkbox name=id value="6"></td>'
        '<td align=left>Average duration (min)</td>'
        '<td>3</td>'
        '<tr align=right bgcolor="#e4e4e4">'
        '<td><input type=checkbox name=id value="ratio"></td>'
        '<td align=left>Pageviews/visitor</td>'
        '<td>4.1</td>'
        '<tr><td bgcolor=#ffffff colspan=4><input type=submit name=show value="rebuild graph"></td>'
        '</table>'
    ) is None

    assert get_statistic(
        '<table cellpadding=3 cellspacing=0 border=0 bgcolor=#f0f0f0>'
        '<tr><td bgcolor=#ffffff colspan=3><img'
        'src="/i/_.gif" width=220 height=1 alt=''></td>'
        '<tr align=right bgcolor="#e4e4e4">'
        '<td><input type=checkbox name=id value="0" checked></td>'
        '<td align=left>Pageviews</td>'
        '<td>21,787<br>'
        '<a href="mins.html" title="difference with the yesterday value at '
        'the same time" target=_blank onClick=\'return open_mins("mins.html")\'>'
        '<font color="#007700">+1,806</font></a></td>'
        '<tr align=right bgcolor="#e4e4e4">'
        '<td><input type=checkbox name=id value="8" checked></td>'
        '<td align=left>Visitors</td>'
        '<td>6,555<br>'
        '<a href="mins_vis.html" title="difference with the yesterday value at '
        'the same time" target=_blank onClick=\'return open_mins("mins_vis.html")\'>'
        '<font color="#007700">+199</font></a></td>'
        '<tr align=right>'
        '<td><input type=checkbox name=id value="7"></td>'
        '<td align=left>Hosts</td>'
        '<td>6,604</td>'
        '<tr align=right bgcolor="#e4e4e4">'
        '<td><input type=checkbox name=id value="4"></td>'
        '<td align=left>Reloads</td>'
        '<td>31</td>'
        '</table>'
    ) == {
        'average_active_online': None,
        'average_duration_min': None,
        'average_online': None,
        'hosts': 6604,
        'pageviews': 21787,
        'pageviews_on_a_visitor': None,
        'reloads': 31,
        'sessions': None,
        'visitors': 6555
    }

    ### def request_page()

    global request_page
    request_page(url='http://google.com', headers={})

    ### def setup()

    pi.settings = deepcopy(DEFAULT)
    pi.settings['login']    = 'foo'
    pi.settings['password'] = 'bar'
    pi.settings['reports']  = [
        'fakereport',
        'pageviews',
        'pageviews_per_min',
        'sessions',
        'visitors',
        'visitors_per_min',
        'hosts',
        'hosts_per_min',
        'reloads',
        'reloads_per_min',
        'average_online',
        'average_active_online',
        'average_duration_min',
        'pageviews_on_a_visitor',
    ]
    assert setup(pi) is True
    assert pi.settings['reports'] == {
        'pageviews',
        'pageviews_per_min',
        'sessions',
        'visitors',
        'visitors_per_min',
        'hosts',
        'hosts_per_min',
        'reloads',
        'reloads_per_min',
        'average_online',
        'average_active_online',
        'average_duration_min',
        'pageviews_on_a_visitor',
    }

    pi.settings = deepcopy(DEFAULT)
    pi.settings['login']    = 'foo'
    pi.settings['password'] = 'bar'
    pi.settings['reports']  = [
        'pageviews_per_min',
        'hosts',
    ]
    assert setup(pi) is True
    assert pi.settings['reports'] == {
        'hosts',
        'pageviews_per_min',
    }

    ### def execute()

    pi.settings = deepcopy(DEFAULT)
    pi.settings['login']    = 'foo'
    pi.settings['password'] = 'bar'
    pi.settings['save_bad_pages'] = True
    pi.settings['reports']  = [
        'pageviews',
        'pageviews_per_min',
        'sessions',
        'visitors',
        'visitors_per_min',
        'hosts_per_min',
        'reloads',
        'reloads_per_min',
        'average_online',
        'average_active_online',
        'average_duration_min',
        'pageviews_on_a_visitor',
    ]

    def request_page(url, headers):
        return 'no page'

    pi.outbox = []
    execute(pi)
    assert pi.outbox == []

    def request_page(url, headers):
        return (
            '<table cellpadding=3 cellspacing=0 border=0 bgcolor=#f0f0f0>'
            '<tr><td bgcolor=#ffffff colspan=3><img'
            'src="/i/_.gif" width=220 height=1 alt=''></td>'
            '<tr align=right bgcolor="#e4e4e4">'
            '<td><input type=checkbox name=id value="0" checked></td>'
            '<td align=left>Pageviews</td>'
            '<td>21,787<br>'
            '<a href="mins.html" title="difference with the yesterday value at '
            'the same time" target=_blank onClick=\'return open_mins("mins.html")\'>'
            '<font color="#007700">+1,806</font></a></td>'
            '<tr align=right bgcolor="#e4e4e4">'
            '<td><input type=checkbox name=id value="8" checked></td>'
            '<td align=left>Visitors</td>'
            '<td>6,555<br>'
            '<a href="mins_vis.html" title="difference with the yesterday value at '
            'the same time" target=_blank onClick=\'return open_mins("mins_vis.html")\'>'
            '<font color="#007700">+199</font></a></td>'
            '<tr align=right>'
            '<td><input type=checkbox name=id value="7"></td>'
            '<td align=left>Hosts</td>'
            '<td>6,604</td>'
            '<tr align=right bgcolor="#e4e4e4">'
            '<td><input type=checkbox name=id value="4"></td>'
            '<td align=left>Reloads</td>'
            '<td>31</td>'
            '</table>'
        )

    pi.outbox = []
    execute(pi)
    outbox = [(a[1], a[2], a[3]) for a in pi.outbox]
    assert outbox == [
        ('liru_parser.pageviews.avg', 'gauge', 21787),
        ('liru_parser.visitors.avg',  'gauge', 6555),
        ('liru_parser.reloads.avg',   'gauge', 31),
    ]

    return True

# Просмотры (Pageviews): количество загрузок страниц сайта. При выводе
# статистики за текущий день показывается разница с количеством просмотров вчера
# на это же время дня.
#
# Посетители (Visitors): количество уникальных посетителей сайта (точнее,
# количество браузеров). Идентификация происходит с помощью cookies. При выводе
# статистики за текущий день показывается разница с количеством посетителей
# вчера на это же время дня.
#
# Сессия (Sessions): серия из нескольких просмотров от одного посетителя, причем
# между соседними просмотрами должно пройти не более 15 минут.
#
# Хосты (Hosts): количество уникальных IP-адресов, с которых запрашивались
# страницы сайта. Несколько компьютеров за одним прокси-сервером считаются за
# один хост.
#
# Перезагрузки (Reloads): повторные загрузки одной страницы одним и тем же
# посетителем в течение 1 секунды. Несмотря на то, что этот параметр выделен в
# отдельную строку, перезагрузки включаются в "просмотры". Большое количество
# перезагрузок (более 10% от общего количества просмотров) говорит о том, что на
# страницах сайта установлено два экземпляра кода счетчика.
#
# В среднем online (Average online): среднесуточное количество посетителей за 15
# минут.
#
# В среднем активных online (Average active online): среднесуточное количество
# посетителей за 15 минут, просмотревших более одной страницы за сессию.
#
# Средняя длительность (Average duration (min)): средняя длительность сессий в
# минутах. Длительность сессии -- промежуток времени между первым и последним
# просмотром в сессии. Длительность сессии из одного просмотра равна нулю.
#
# Просмотров на посетителя (Pageviews/visitor): среднее количество просмотров,
# совершаемое одним посетителем. Вычисляется как разность общего количества
# просмотров и просмотров без cookies, делённая на количество уникальных
# посетителей.
