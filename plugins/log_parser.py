#!/usr/bin/env python3
__doc__ = '''
Parse logs and search for values.

----- example -----
foo.log:

0.3s | take a 45 red baloons
0.5s | take a 2 blue baloons
0.5s | take a baloons

settings:

file: foo.log
count: true
avg: true
lines:
  colored: " (red|blue) "
  colorless: "a baloons"
  others: ".*"
values:
  time: "([0-9.]+)s |"
  quantity: "take a ([0-9]+) "

result:

host.log_parser.colored.time.count,     count, 2
host.log_parser.colored.quantity.count, count, 2
host.log_parser.colored.time.avg,       gauge, 0.4
host.log_parser.colored.quantity.avg,   gauge, 23.5
host.log_parser.colorless.time.count,   count, 1
host.log_parser.colorless.time.avg,     gauge, 0.5

----- settings -----
plugins:
  log_parser:
    module: log_parser
    settings:
      file: []           # Read data from these files.
      send_zeros: false  # Send metrics with zero value.
      send_other: false  # Send unmatched lines as metric "other".
      count: false       # Report the number of occurences.
      cps:   true        # Same, but per second.
      summ:  false       # Summ of values.
      avg:   true        # Average of values.
      min:   false       # Minimum value.
      max:   false       # Maximum value.
      accept_lines: []   # Don`t drop this lines in setting drop_lines.
      drop_lines:   []   # Drop this lines. All other lines will be used for
                         # searching.
      lines: {}          # Lines for search.
      values: {}         # Values for search in lines. The desired value must be
                         # enclosed in parentheses.
'''
import os
import re


def setup(pi):
    default_settings = {
        'file':         [],
        'send_zeros':   False,
        'send_other':   False,
        'count':        False,
        'cps':          True,
        'summ':         False,
        'avg':          True,
        'min':          False,
        'max':          False,
        'accept_lines': [],
        'drop_lines':   [],
        'lines':        {},
        'values':       {},
    }
    if not pi.apply_default_settings(default_settings): return False
    s = pi.settings

    # Check metrics for send
    if not any((s['count'], s['cps'], s['summ'], s['avg'], s['min'], s['max'])):
        pi.log_critical('No parameters for send.')
        return False

    # Prepare lines filter
    s['accept_lines'] = [a for a in map(str, s['accept_lines']) if a]
    s['drop_lines'] = [a for a in map(str, s['drop_lines']) if a]
    lines_filter = Filter()
    for regexp in s['accept_lines']: lines_filter.add_accept_expression(regexp)
    for regexp in s['drop_lines']: lines_filter.add_drop_expression(regexp)

    # Prepare searcher
    searcher = Searcher()
    searcher.other = s['send_other']
    if len(s['lines']) == 0:
        pi.log_critical('No lines for search.')
        return False
    for line_name, line_expr in s['lines'].items():
        searcher.add_line(line_name, line_expr)

    if len(s['values']) == 0:
        pi.log_critical('No values for search.')
        return False
    for value_name, value_expr in s['values'].items():
        if not re.search(r'\(.*\)', value_expr):
            pi.log_critical('The numbers of desired value "{}" must be enclosed'
                            ' in parentheses.'.format(value_name))
            return False
        searcher.add_value(value_name, value_expr)

    # Prepare counters
    for line_name in s['lines'].keys():
        for value_name in s['values'].keys():
            MetaCounter(line_name + '.' + value_name)
    if s['send_other']:
        for value_name in s['values'].keys():
            MetaCounter('other.' + value_name)

    # Prepare LogFollower
    s['file'] = [a for a in map(str, s['file']) if a]
    if len(s['file']) == 0:
        pi.log_critical('Source file is not specified.')
        return False
    for file in s['file']:
        if not os.path.isfile(file):
            pi.log_warning('Start with no source file "{}".'.format(file))
        LogFollower(file)

    return True


def execute(pi):
    s = pi.settings

    new_lines = []
    for log in LogFollower.instances: new_lines += log.read_lines()
    if not new_lines: return

    lines_filter = Filter.instances[0]
    new_lines = list(filter(lines_filter.check, new_lines))

    data_lines = []
    searcher = Searcher.instances[0]
    for line in new_lines: data_lines += searcher.find_data(line)
    if not data_lines: return
    del(new_lines)

    counters = {}
    for counter in MetaCounter.instances: counters[counter.name] = counter
    for name, value in data_lines: counters[name].calc(value)
    del(data_lines)

    for name in counters.keys():
        values = counters[name].counters()
        if values['count'] == 0 and not s['send_zeros']: continue
        if s['count']:
            pi.send_metric(
                mname=name + '.count',
                mval=values['count'],
                mtype='count'
            )
        if s['cps']:
            try: mval = values['count'] / pi.sleep_time
            except ZeroDivisionError: mval = values['count']
            pi.send_metric(
                mname=name + '.cps',
                mval=mval
            )
        for req_value in ('summ', 'avg', 'min', 'max'):
            if s[req_value]:
                pi.send_metric(
                    mname=name + '.' + req_value,
                    mval=values[req_value]
                )


class LogFollower():
    instances = []

    def __init__(self, filename):
        self.filename = filename
        self.position = None
        self.size     = None
        if self._check_file_size(): self.position = self.size
        self.__class__.instances.append(self)

    def _check_file_size(self):
        try: self.size = os.path.getsize(self.filename)
        except OSError: return False
        return True

    def read_lines(self):
        lines = []
        if not self._check_file_size():
            self.position = None
            return lines
        if self.position is None:
            self.position = self.size
            return lines
        if self.position > self.size: self.position = 0
        try:
            with open(self.filename, 'r') as fd:
                if self.position > 0: fd.seek(self.position)
                while self.position < self.size:
                    lines.append(fd.readline().strip())
                    self.position = fd.tell()
        except OSError: pass
        return lines


class Filter():
    instances = []

    def __init__(self):
        self.accept = []
        self.drop   = []
        self.__class__.instances.append(self)

    def add_accept_expression(self, regexp):
        self.accept.append(re.compile(regexp))

    def add_drop_expression(self, regexp):
        self.drop.append(re.compile(regexp))

    def check(self, string):
        for regexp in self.accept:
            if regexp.search(string): return True
        for regexp in self.drop:
            if regexp.search(string): return False
        return True


class Searcher():
    instances = []

    def __init__(self):
        self.lines  = {}
        self.values = {}
        self.__class__.instances.append(self)
        self.other = False

    def add_line(self, name, regexp):
        self.lines[name] = re.compile(regexp)

    def add_value(self, name, regexp):
        self.values[name] = re.compile(regexp)

    def find_data(self, string):
        result = []
        line_name = self._check_lines(string)
        if line_name is None: return result
        values = self._check_values(string)
        if values is None: return result
        for value_name, number in values.items():
            result.append([line_name + '.' + value_name, number])
        return result

    def _check_lines(self, string):
        for line_name, line_expr in self.lines.items():
            if line_expr.search(string): return line_name
        if self.other: return 'other'
        return None

    def _check_values(self, string):
        values = {}
        for value_name, value_expr in self.values.items():
            match = value_expr.search(string)
            if not match: continue
            values[value_name] = self._turn_into_a_number(match.group(1))
        if not values: return None
        return values

    def _turn_into_a_number(self, string):
        string = string.replace(',', '.')
        if string.find('.') > -1: return float(string)
        else: return int(string)


class MetaCounter():
    instances = []

    def __init__(self, name):
        self.name = name
        self._reset_counters()
        self.__class__.instances.append(self)

    def _reset_counters(self):
        self.count = 0
        self.summ  = 0
        self.avg   = None
        self.min   = None
        self.max   = None

    def calc(self, value):
        self.count += 1
        self.summ += value
        try:
            self.min = min(self.min, value)
            self.max = max(self.max, value)
        except TypeError:
            self.min = value
            self.max = value

    def counters(self):
        try: self.avg = self.summ / self.count
        except ZeroDivisionError: self.avg = 0
        result = {
            'count': self.count,
            'summ':  self.summ,
            'avg':   self.avg,
            'min':   self.min,
            'max':   self.max,
        }
        self._reset_counters()
        return result


def test(pi):
    from test_lib import tmpdir, make_tmpfile, add_to_tmpfile, clear_tmpdir
    from copy import deepcopy
    test_filename = 'test_log'
    test_file = tmpdir + '/' + test_filename
    test_settings = {
        'file':       test_file,
        'send_zeros': False,
        'send_other': True,
        'count':      True,
        'cps':        True,
        'summ':       True,
        'avg':        False,
        'min':        False,
        'max':        False,
        'lines': {
            'colored':   " (red|blue) ",
            'colorless': "a baloons",
        },
        'values': {
            'time':     "([0-9.]+)s |",
            'quantity': "take a ([0-9]+) ",
        }
    }
    clear_tmpdir()

    ### def setup()

    # Empty file name
    pi.settings = deepcopy(test_settings)
    pi.settings['file'] = ''
    assert setup(pi) is False

    # Many file names
    pi.settings = deepcopy(test_settings)
    pi.settings['file'] = ['a', 'b', 'c']
    assert setup(pi) is True

    # Nothing to report
    pi.settings = deepcopy(test_settings)
    pi.settings['count'] = False
    pi.settings['cps'] = False
    pi.settings['summ'] = False
    assert setup(pi) is False

    # Incorrect search line
    pi.settings = deepcopy(test_settings)
    pi.settings['lines'] = ['something']
    assert setup(pi) is False

    # Incorrect search value
    pi.settings = deepcopy(test_settings)
    pi.settings['values'] = 2
    assert setup(pi) is False

    pi.settings = deepcopy(test_settings)
    pi.settings['values'] = {'val1': '([0-9.] '}
    assert setup(pi) is False

    ### class LogFollower()

    # No log file
    log = LogFollower(test_file)
    assert log.read_lines() == []

    # Empty log file
    make_tmpfile(test_filename)
    assert log.read_lines() == []

    # Add one line
    add_to_tmpfile(test_filename, '0.3s | take a 45 red baloons\n')
    assert log.read_lines() == ['0.3s | take a 45 red baloons']

    # Add two new lines
    add_to_tmpfile(test_filename, '0.5s | take a 2 blue baloons\n')
    add_to_tmpfile(test_filename, '0.5s | take a baloons\n')
    assert log.read_lines() == ['0.5s | take a 2 blue baloons',
                                '0.5s | take a baloons']

    # Renew file with one line
    make_tmpfile(test_filename, '0.3s | take a 45 red baloons\n')
    assert log.read_lines() == ['0.3s | take a 45 red baloons']

    # Remove log file
    clear_tmpdir()
    assert log.read_lines() == []

    # First correct read file. Line is ignored
    make_tmpfile(test_filename, '0.3s | take a 45 red baloons\n')
    assert log.read_lines() == []

    # Add line
    add_to_tmpfile(test_filename, '0.5s | take a 2 blue baloons\n')
    assert log.read_lines() == ['0.5s | take a 2 blue baloons']

    LogFollower.instances = []

    ### class Filter()

    lines_filter = Filter()
    lines_filter.add_drop_expression('fo+')
    assert lines_filter.check('fooo') is False
    assert lines_filter.check('f-ooo') is True
    Filter.instances = []

    lines_filter.add_accept_expression('fo+')
    lines_filter.add_drop_expression('.*')
    assert lines_filter.check('fooo') is True
    assert lines_filter.check('f-ooo') is False
    Filter.instances = []

    ### class Searcher()

    pi.settings = deepcopy(test_settings)
    searcher = Searcher()
    for line_name, line_expr in pi.settings['lines'].items():
        searcher.add_line(line_name, line_expr)
    for value_name, value_expr in pi.settings['values'].items():
        searcher.add_value(value_name, value_expr)

    data_lines = []
    data = [
        '0.1s | take a 7 red baloons',
        '0.3s | take a 11 blue baloons',
        '0.5s | take a baloons',
    ]
    for line in data: data_lines += searcher.find_data(line)
    assert data_lines == [
        ['colored.time', 0.1],
        ['colored.quantity', 7],
        ['colored.time', 0.3],
        ['colored.quantity', 11],
        ['colorless.time', 0.5],
    ]

    searcher.other = True
    assert searcher.find_data('0.5s | no baloons') == [['other.time', 0.5]]

    Searcher.instances = []

    ### class MetaCounter()

    data_lines == [
        ['colored.time', 0.1],
        ['colored.quantity', 7],
        ['colored.time', 0.3],
        ['colored.quantity', 11],
        ['colorless.time', 0.5],
    ]
    counter = MetaCounter('mycnt')
    for name, value in data_lines: counter.calc(value)
    assert counter.counters() == {
        'avg': 3.78,
        'count': 5,
        'max': 11,
        'min': 0.1,
        'summ': 18.9
    }
    MetaCounter.instances = []

    ### def execute()

    # Prepare for test execute()
    make_tmpfile(test_filename)
    LogFollower.instances = []
    pi.settings = deepcopy(test_settings)
    pi.settings['file'] = test_file
    assert setup(pi) is True
    execute(pi)

    # Test execute()
    add_to_tmpfile(test_filename, '0.3s | take a 45 red baloons\n')
    add_to_tmpfile(test_filename, '0.5s | take a 2 blue baloons\n')
    add_to_tmpfile(test_filename, '0.5s | take a baloons\n')
    add_to_tmpfile(test_filename, '0.1s | no baloons\n')
    pi.sleep_time = 2
    pi.outbox = []
    execute(pi)
    assert len(pi.outbox) == 12

    add_to_tmpfile(test_filename, '0.6s | take a 10 blue baloons\n')
    add_to_tmpfile(test_filename, '0.2s | take a 11 blue baloons\n')
    add_to_tmpfile(test_filename, '0.1s | take a baloons\n')
    add_to_tmpfile(test_filename, '0.3s | take a baloons\n')
    pi.sleep_time = 2
    pi.outbox = []
    execute(pi)
    assert len(pi.outbox) == 9

    return True
