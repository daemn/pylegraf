#!/usr/bin/env python3
__doc__ = '''
Memory allocation info. Parse /proc/meminfo.

----- settings -----
plugins:
  memory_status:
    module: memory_status
    settings:
      mem_file: /proc/meminfo  # Data source.
      fields:            # Report this fields. Use any field in lowercase from
                         # "mem file". Default is ['swapused', 'used', 'cache'].
        - swapused       # Used swap memory (SwapTotal - SwapFree).
        - swapfree       # Unused swap memory.
        - memfree        # Unused memory.
        - buffers        # Memory used by kernel buffers.
        - cache          # The page cache and slabs (Cached and SReclaimable).
        - used           # Used memory (calculated as memtotal - memfree -
                         # buffers - cache).
        - memavailable   # Estimation of how much memory is available for
                         # starting new application, without swapping. If
                         # MemAvailable isn`t present in /proc/meminfo, then
                         # calculate as memfree + cache.
        - shared         # Memory used (mostly) by tmpfs (alias of Shmem).
      abs: false         # Output absolute values.
      prc: true          # Output values in percents.
      prec: 2            # Precision of values.
      multiplier: 'b'    # Out absolute values in [b]ytes, [k]ibibytes, ...,
                         # [p]ebibytes.
'''
import sys
import os
import pylegraf_lib as plib

DEFAULT_SETTINGS = {
    'mem_file':   '/proc/meminfo',
    'fields':     ['swapused', 'used', 'cache'],
    'abs':        False,
    'prc':        True,
    'prec':       2,
    'multiplier': 'b',
}
MULTIPLIERS = {b: 1024 ** a for a, b in enumerate('bkmgtp')}


def setup(pi):
    pi.apply_default_settings(DEFAULT_SETTINGS)
    s = pi.settings

    if not os.path.isfile(s['mem_file']):
        pi.log_critical('Wrong "mem_file".')
        return False

    if not s['abs'] and not s['prc']:
        pi.log_critical('Select "abs" or "prc" option.')
        return False

    if s['abs']:
        if not s['multiplier']: s['multiplier'] = '_'
        s['multiplier'] = s['multiplier'][0].lower()
        if s['multiplier'] not in MULTIPLIERS:
            pi.log_error('Wrong multiplier. Use default.')
            s['multiplier'] = DEFAULT_SETTINGS['multiplier']

    s['fields'] = set([str(a).lower().strip() for a in s['fields']]) - {''}
    if not s['fields']:
        pi.log_error('No fields has been selected. Use default.')
        s['fields'] = set(DEFAULT_SETTINGS['fields'])

    return True


def execute(pi):
    s = pi.settings

    # Get data from source file.
    meminfo = read_mem_file(s['mem_file'])
    if not meminfo:
        pi.log_critical('Wrong format of source file.')
        return False

    # Check the report fields list.
    wtf = s['fields'] - set(meminfo)
    if wtf:
        pi.log_error('Unknown fields "{}" will be ignored.'
                     .format(', '.join(wtf)))
        s['fields'] = s['fields'] - wtf
        if not s['fields']:
            pi.log_error('No fields has been selected. Use default.')
            s['fields'] = set(DEFAULT_SETTINGS['fields'])

    # Report absolute values.
    if s['abs']:
        for field in s['fields']:
            mname = '.'.join((plib.convert_to_metric(field), 'abs'))
            if field.startswith('hugepages_'):
                mval = meminfo[field]
            else:
                mval = round(meminfo[field] / MULTIPLIERS[s['multiplier']],
                             s['prec'])
                if s['prec'] < 1: mval = int(mval)
            pi.send_metric(mname=mname, mtype='gauge', mval=mval)

    # Report percent values.
    if s['prc']:
        for field in s['fields']:
            mname = '.'.join((plib.convert_to_metric(field), 'prc'))
            if field in ('swaptotal', 'memtotal'):
                continue
            elif field in ('swapused', 'swapfree'):
                mval = plib.in_percents(meminfo[field],
                                        meminfo['swaptotal'],
                                        s['prec'])
            else:
                mval = plib.in_percents(meminfo[field],
                                        meminfo['memtotal'],
                                        s['prec'])
            if s['prec'] < 1: mval = int(mval)
            pi.send_metric(mname=mname, mtype='gauge', mval=mval)


def read_mem_file(filename='/proc/meminfo'):
    with open(filename, 'r') as fd: mem_lines = fd.readlines()
    meminfo = {}
    for line in mem_lines:
        fields = line.strip().split()
        parameter = fields[0].rstrip(':').lower()
        value = int(fields[1])
        if len(fields) > 2: value *= MULTIPLIERS[fields[2][0].lower()]
        meminfo[parameter] = value

    if not meminfo: return None

    meminfo['shared']   = meminfo['shmem']
    meminfo['swapused'] = meminfo['swaptotal'] - meminfo['swapfree']
    meminfo['cache']    = meminfo['cached'] + meminfo['sreclaimable']
    meminfo['used']     = (meminfo['memtotal'] - meminfo['memfree'] -
                           meminfo['buffers'] - meminfo['cache'])
    if 'memavailable' not in meminfo:
        meminfo['memavailable'] = meminfo['memfree'] + meminfo['cache']

    return meminfo


def test(pi):
    meminfo = read_mem_file()
    assert isinstance(meminfo, dict)
    assert len(meminfo) > 0

    return True


# $ head /proc/meminfo
# MemTotal:       16339008 kB
# MemFree:         7949004 kB
# MemAvailable:   11207720 kB
# Buffers:          391944 kB
# Cached:          3076200 kB
# SwapCached:            0 kB
# Active:          5490932 kB
# Inactive:        2191752 kB
# Active(anon):    4197816 kB
# Inactive(anon):   188664 kB
