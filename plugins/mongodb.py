from pymongo import MongoClient

__doc__ = '''
Collect mongodb statistic.

----- settings -----
plugins:
  mongodb:
    module: mongodb
    settings:
      host:       "localhost" # \
      port:       27017       # | MongoDB connection parameters.
      user:       "guest"     # |
      password:   "guest"     # /
      send_zeros: false       # Send metrics with zero value.
'''

DEFAULT = {
    'host':     'localhost',
    'port':     27017,
    'user':     'guest',
    'password': 'guest',
    'send_zeros': False,
}
OLD_SERVER_STATUS = dict()


def increment(val_from, val_to):
    if val_to > val_from:
        return val_to - val_from
    else:
        return 0


def safe_divide(dividend, divisor):
    try:
        return dividend / divisor
    except ZeroDivisionError:
        return 0


def setup(pi):
    pi.apply_default_settings(DEFAULT)
    s = pi.settings

    if not s['host']:
        pi.log_critical('Empty "host".')
        return False

    if s['port'] < 0 or s['port'] > 65535:
        pi.log_critical('Wrong "port".')
        return False

    return True


def execute(pi):
    global OLD_SERVER_STATUS
    if pi.failures_in_row:
        OLD_SERVER_STATUS = dict()
    metrics = dict()

    # Подключиться к базе данных и получить показания сервера.
    mongo_client = MongoClient(
        host=[pi.settings['host']],
        port=pi.settings['port'],
        username=pi.settings['user'],
        password=pi.settings['password']
    )
    db = mongo_client['admin']
    server_status = db.command('serverStatus')
    mongo_client.close()

    # Собрать показания:
    # "connections": {
    #     "current": 5,
    # },
    metrics['connections.current'] = server_status['connections']['current']

    # Собрать показания:
    # "metrics": {
    #     "document": {
    #         "deleted": 0,
    #         "inserted": 0,
    #         "returned": 3,
    #         "updated": 2
    #     },
    # },
    try:
        for key, value in server_status['metrics']['document'].items():
            value_increment = increment(
                OLD_SERVER_STATUS['metrics']['document'][key], value
            )
            metrics['document.' + key] = safe_divide(value_increment, pi.sleep_time)
    except KeyError:
        pass
    OLD_SERVER_STATUS = server_status

    # Отправить метрики диспетчеру.
    for mname, mval in metrics.items():
        if mval == 0 and not pi.settings['send_zeros']:
            continue
        pi.send_metric(mtime=None, mname=mname, mtype='gauge', mval=mval)

    return True


def test(pi):
    from copy import deepcopy

    #########################
    # test function "setup()"

    # Wrong option 'host'
    pi.settings = deepcopy(DEFAULT)
    pi.settings['host'] = ''
    assert setup(pi) is False

    # Wrong option 'port'
    pi.settings = deepcopy(DEFAULT)
    pi.settings['port'] = 70000
    assert setup(pi) is False

    # Everything is ok.
    pi.settings = deepcopy(DEFAULT)
    pi.settings['user'] = 'guest'
    pi.settings['password'] = 'guest'
    assert setup(pi) is True

    #######################
    # test mongo connection

    mongo_client = MongoClient(
        host=[pi.settings['host']],
        port=pi.settings['port'],
        username=pi.settings['user'],
        password=pi.settings['password']
    )
    db = mongo_client['admin']
    _ = db.command('serverStatus')
    mongo_client.close()

    return True


# В выводе serverStatus есть ещё такие показания:
# "opcounters": {
#     "insert": 0,
#     "query": 9,
#     "update": 5,
#     "delete": 0,
#     "getmore": 0,
#     "command": 688
# },
# "opcountersRepl": {
#     "insert": 0,
#     "query": 0,
#     "update": 0,
#     "delete": 0,
#     "getmore": 0,
#     "command": 0
# },

# db.hostInfo()     db.command('hostInfo')

# db.stats()        db.command('dbstats')
