#!/usr/bin/env python3
__doc__ = '''
Mysql status. Now for mysql-8 only.
Requirements:
  - mysql-connector-python-8.0.23
  - protobuf-3.15.8

Reported metrics:

threads_cached
    The number of threads in the thread cache.

threads_connected
    The number of currently open connections.

threads_running
    The number of threads that are not sleeping.

slow_queries
    The number of queries that have taken more than long_query_time seconds.

table_locks_immediate
    The number of times that a request for a table lock could be granted
    immediately.

table_locks_waited
    The number of times that a request for a table lock could not be granted
    immediately and a wait was needed.

bytes_received
    The number of bytes per second received from all clients.

bytes_sent
    The number of bytes per second sent to all clients.

----- settings -----
plugins:
  mysql_status:
    module: mysql_status
    settings:
      host:       'localhost' # \
      port:       3306        # | MySQL connect data
      user:       'watch'     # |
      password:   ''          # /
      send_zeros: false       # Send metrics with zero value.
'''
import pylegraf_lib as plib
import mysql.connector

default_settings = {
    'host':       'localhost',
    'port':       3306,
    'user':       'watch',
    'password':   '',
    'send_zeros': False,
}
mysql_interface = None
server_state = None


class MysqlInterface():

    def __init__(self, host='localhost', port=3306, user='root', password=''):
        self.host       = host
        self.port       = port
        self.user       = user
        self.password   = password
        self.connection = None
        self.result     = None

    def execute(self, request):
        self.result = None
        if self.connection is None or not self.connection.is_connected():
            self.connection = mysql.connector.connect(
                host=self.host,
                port=self.port,
                user=self.user,
                password=self.password
            )
        with self.connection.cursor() as cursor:
            cursor.execute(request)
            self.result = cursor.fetchall()
        return True

    def close(self):
        try: self.connection.close()
        except Exception: pass
        return True


class Mysql8State():

    def __init__(self, mysql_interface):
        self.mysql_interface = mysql_interface
        self.old_state = {}
        self.new_state = {}
        self.elapsed_time = None
        self.fail_counter = 0

    def refresh(self):
        # Try to request new server state.
        if not self.mysql_interface.execute('SHOW GLOBAL STATUS'):
            self.fail_counter += 1
            if self.fail_counter > 3:
                # Reset all states if too much fails.
                self.new_state = {}
                self.old_state = {}
            return False
        self.fail_counter = 0

        # Read new server state from mysql answer.
        self.old_state = self.new_state
        self.new_state = {}
        self.elapsed_time = None
        for row in self.mysql_interface.result:
            try: self.new_state[row[0]] = int(row[1])
            except (TypeError, ValueError): continue

        try:
            elapsed_time = self.new_state['Uptime'] - self.old_state['Uptime']
            elapsed_time_since_flush = (
                self.new_state['Uptime_since_flush_status'] -
                self.old_state['Uptime_since_flush_status']
            )
        except KeyError: return True
        if elapsed_time < 1 or abs(elapsed_time - elapsed_time_since_flush) > 1:
            self.old_state = {}
            return True

        self.elapsed_time = min(elapsed_time, elapsed_time_since_flush)
        return True

    def value(self, name):
        try: return self.new_state[name]
        except KeyError: return None

    def delta(self, name):
        try: return self.new_state[name] - self.old_state[name]
        except KeyError: return None

    def per_second(self, name):
        try: return self.delta(name) / self.elapsed_time
        except (ValueError, TypeError, ZeroDivisionError): return None


def setup(pi):

    pi.apply_default_settings(default_settings)
    s = pi.settings

    if not s['host']:
        pi.log_error('Not specified host.')
        return False

    for setting in ('host', 'port', 'user'):
        if not s[setting]:
            pi.log_error(
                '{} not specified.'.format(setting[0].upper() + setting[1:])
            )
            return False

    if s['port'] < 0 or s['port'] > 65535:
        pi.log_error('Port out of range 0-65535.')
        return False

    global mysql_interface
    global server_state
    mysql_interface = MysqlInterface(
        host=s['host'],
        port=s['port'],
        user=s['user'],
        password=s['password']
    )
    server_state = Mysql8State(mysql_interface)

    return True


def execute(pi):
    if not server_state.refresh(): return False

    for_sent = []
    for name in (
        'Bytes_received',
        'Bytes_sent',
        'wsrep_replicated_bytes',
        'wsrep_received_bytes',
    ):
        mval = server_state.per_second(name)
        if mval is None: continue
        mname = plib.convert_to_metric(name.lower())
        for_sent.append((mname, mval))

    for name in (
        'Slow_queries',
        'Table_locks_immediate',
        'Table_locks_waited',
        'wsrep_flow_control_sent',
        'wsrep_flow_control_recv',
        'wsrep_replicated',
        'wsrep_received',
    ):
        mval = server_state.delta(name)
        if mval is None: continue
        mname = plib.convert_to_metric(name.lower())
        for_sent.append((mname, mval))

    for name in (
        'Threads_cached',
        'Threads_connected',
        'Threads_running',
        'wsrep_local_cert_failures',
        'wsrep_local_bf_aborts',
        'wsrep_local_recv_queue',
        'wsrep_local_send_queue',
    ):
        mval = server_state.value(name)
        if mval is None: continue
        mname = plib.convert_to_metric(name.lower())
        for_sent.append((mname, mval))

    for mname, mval in for_sent:
        if mval == 0 and not pi.settings['send_zeros']: continue
        pi.send_metric(
            mtime=None,
            mname=mname,
            mtype='gauge',
            mval=mval,
        )

    return True


def destroy(pi):
    pi.log_debug('Close mysql connection.')
    mysql_interface.close()
    return True


def test(pi):
    from copy import deepcopy

    ### def setup()

    # Wrong option 'host'
    pi.settings = deepcopy(default_settings)
    pi.settings['host'] = ''
    assert setup(pi) is False

    # Wrong option 'port'
    pi.settings = deepcopy(default_settings)
    pi.settings['port'] = 70000
    assert setup(pi) is False
    pi.settings['port'] = -1
    assert setup(pi) is False

    # Everything is ok.
    pi.settings = deepcopy(default_settings)
    pi.settings['user']     = 'guest'
    pi.settings['password'] = 'guest'
    assert setup(pi) is True

    return True
