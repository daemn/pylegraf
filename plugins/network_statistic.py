#!/usr/bin/env python3
__doc__ = '''
Network statistic. Parse /proc/net/dev.

----- settings -----
plugins:
  network_statistic:
    module: network_statistic
    settings:
      src_file: '/proc/net/dev'  # Data source. Default is '/proc/net/dev'.
      bps: false        # Out bits per second.
      ops: true         # Out octets per second.
      pps: true         # Out packets per second.
      eps: true         # Out errors per second.
      include: []       # Include this interfaces.  ###TODO
      exclude: ['lo']   # Exclude this interfaces. Default is 'lo'.
      send_zeros: false # Report values equal zeros.
'''
import sys
import os
import pylegraf_lib as plib


DEFAULT_SETTINGS = {
    'src_file':   '/proc/net/dev',
    'bps':        False,
    'ops':        True,
    'pps':        True,
    'eps':        True,
    # 'include':    [],
    'exclude':    ['lo'],
    'send_zeros': False,
}
PREVIOUS_STATISTIC = {}
FIELDS = ('rc_bytes', 'rc_packets', 'rc_errs', 'rc_drop',
          'rc_fifo', 'rc_frame', 'rc_compressed', 'rc_multicast',
          'tr_bytes', 'tr_packets', 'tr_errs', 'tr_drop',
          'tr_fifo', 'tr_colls', 'tr_carrier', 'tr_compressed')


def setup(pi):
    pi.apply_default_settings(DEFAULT_SETTINGS)
    s = pi.settings

    if not os.path.isfile(s['src_file']):
        pi.log_critical('Wrong "src_file".')
        return False

    if not any([s['bps'], s['ops'], s['pps']]):
        pi.log_critical('Select something to send.')
        return False

    s['exclude'] = set([str(a).lower().strip() for a in s['exclude']]) - {''}

    return True


def execute(pi):
    s = pi.settings
    global PREVIOUS_STATISTIC

    # Read statistic data from source file.
    statistic = read_net_statistic(s['src_file'])
    if not statistic: return
    for if_name in list(statistic):
        if if_name in s['exclude']: del(statistic[if_name])

    # If this is a first successfull execution, just save statistic.
    if pi.successes_in_row == 0 or pi.sleep_time <= 0:
        PREVIOUS_STATISTIC = statistic
        return True

    # Statistic growing calculate and send.
    for if_name, if_stat in statistic.items():
        if if_name not in PREVIOUS_STATISTIC: continue
        for metric_name in 'bps', 'ops', 'pps', 'eps':
            if s[metric_name]:
                for direction in 'rc', 'tr':
                    mname = plib.join_metric((plib.convert_to_metric(if_name),
                                              metric_name,
                                              direction))
                    delta = (
                        if_stat[metric_name][direction]
                        - PREVIOUS_STATISTIC[if_name][metric_name][direction])
                    if delta < 0: continue
                    mval = round(delta / pi.sleep_time, 1)
                    if s['send_zeros'] or mval > 0:
                        pi.send_metric(mname=mname, mval=mval)

    PREVIOUS_STATISTIC = statistic
    return True


def read_net_statistic(filename='/proc/net/dev'):
    with open(filename, 'r') as fd: raw_lines = fd.readlines()
    if len(raw_lines) < 3: return
    statistic = {}
    for line in raw_lines[2:]:
        fields = line.split()
        if_name = fields[0][:-1]
        fields = dict(zip(FIELDS, fields[1:]))
        statistic[if_name] = {'bps': {'rc': int(fields['rc_bytes']) * 8,
                                      'tr': int(fields['tr_bytes']) * 8},
                              'ops': {'rc': int(fields['rc_bytes']),
                                      'tr': int(fields['tr_bytes'])},
                              'pps': {'rc': int(fields['rc_packets']),
                                      'tr': int(fields['tr_packets'])},
                              'eps': {'rc': int(fields['rc_errs']),
                                      'tr': int(fields['tr_errs'])}}

    return statistic


def test(pi):
    statistic = read_net_statistic()
    assert len(statistic) > 0

    return True


### cat /proc/net/dev
# Inter-|   Receive                                                |  Transmit
#  face |bytes    packets errs drop fifo frame compressed multicast|bytes    packets errs drop fifo colls carrier compressed
#     lo:   16413     210    0    0    0     0          0         0    16413     210    0    0    0     0       0          0
# docker0:       0       0    0    0    0     0          0         0        0       0    0    0    0     0       0          0
# veth8e8615f:  742160    7882    0    0    0     0          0         0  3086366   10917    0    0    0     0       0          0
# veth304d8f8:    1378      12    0    0    0     0          0         0   861693    4240    0    0    0     0       0          0
# enp3s0: 30152608618 21177696    0    0    0     0          0      3749 1248165757 14103515    5    0    0     0       0          0
# br-732dcb4b32f9:  633270    7900    0    0    0     0          0         0  3072350   10793    0    0    0     0       0          0
# veth05de535:     332       6    0    0    0     0          0         0   860734    4234    0    0    0     0       0          0
# vetha1939fa:       0       0    0    0    0     0          0         0   859796    4227    0    0    0     0       0          0
#   tun0:  964600    1702    0    0    0     0          0         0   213522    2178    0    0    0     0       0          0
