#!/usr/bin/env python3
__doc__ = '''
Parse /proc/sys/fs/file-nr and report the number of open files.

----- settings -----
plugins:
  open_files_status:
    module: open_files_status
    settings:
      status_file: /proc/sys/fs/file-nr        # Data source.
      openvz_bc_file: /proc/user_beancounters  # Data source in openvz containers.
      abs: false                       # Output absolute value.
      prc: true                        # Output percent value.
      prec: 2                          # Precision of percents.
'''
import sys
import os
import pylegraf_lib as plib


DEFAULT_SETTINGS = {
    'status_file':    '/proc/sys/fs/file-nr',
    'openvz_bc_file': '/proc/user_beancounters',
    'abs':  False,
    'prc':  True,
    'prec': 2,
}
HOST_TYPE = 'default'


def setup(pi):
    global HOST_TYPE
    pi.apply_default_settings(DEFAULT_SETTINGS)
    s = pi.settings

    if os.path.isfile(s['openvz_bc_file']):
        pi.log_info('Host is openvz container.')
        HOST_TYPE = 'openvz_ct'
    elif not os.path.isfile(s['status_file']):
        pi.log_critical('Wrong source file.')
        return False

    if not s['abs'] and not s['prc']:
        pi.log_critical('Select "abs" or "prc" option.')
        return False

    return True


def execute(pi):
    s = pi.settings

    # Get data from source file.
    if HOST_TYPE == 'openvz_ct':
        statistic = read_openvz_bc_file(s['openvz_bc_file'])
    else:
        statistic = read_status_file(s['status_file'])

    # Report absolute value.
    if s['abs']:
        pi.send_metric(mname='abs', mtype='gauge', mval=statistic['opened'])

    # Report percent value.
    if s['prc']:
        mval = plib.in_percents(statistic['opened'],
                                statistic['limit'],
                                s['prec'])
        if s['prec'] < 1: mval = int(mval)
        pi.send_metric(mname='prc', mtype='gauge', mval=mval)

    return True


def read_status_file(filename='/proc/sys/fs/file-nr'):
    with open(filename, 'r') as fd: line = fd.readline().strip()
    fields = line.split()
    return {'opened': int(fields[0]), 'limit': int(fields[2])}


def read_openvz_bc_file(filename='/proc/user_beancounters'):
    with open(filename, 'r') as fd: raw_lines = fd.readlines()
    statistic = {}
    for line in raw_lines:
        fields = line.strip().split()
        if fields[0] == 'numfile':
            statistic['opened'] = int(fields[1])
            statistic['limit'] = int(fields[3])
            break

    return statistic


def test(pi):
    if os.path.isfile('/proc/user_beancounters'):
        statistic = read_openvz_bc_file()
    else:
        statistic = read_status_file()
    assert statistic['opened'] > 0

    return True


### cat /proc/sys/fs/file-nr
# 17730 0   9223372036854775807

### cat /proc/user_beancounters | egrep fil
# numfile  1751  5500  9223372036854775807  9223372036854775807  0
