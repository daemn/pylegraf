#!/usr/bin/env python3
__doc__ = '''
Report openvz resources. Parse file /proc/bc/CTID/resources.

----- settings -----
plugins:
  openvz_resources:
    module: openvz_resources
    settings:
      bc_file: /proc/bc/resources  # Beancounters file.
      conf_dir: /etc/vz/conf       # Directory with openvz config files.
      ctname: shortid     # Report 'id', 'shortid', 'name' or 'hostname' as name.
      held: false         # Report current values ('held' in resources-file).
      held_prc: true      # Report current values in percents ('barrier' is 100%).
      fails: false        # Report fails by rows (added number).
      total_fails: true   # Report total fails (added number).
      send_zeros: false   # Send metrics with zero value.
      prec: 1             # Precision of percents.
      ignore_ct0: true    # Ignore ctid 0.
      rows:               # Rows for report.
        # - kmemsize
        # - lockedpages
        # - privvmpages
        # - shmpages
        - numproc
        - physpages
        # - vmguarpages
        # - oomguarpages
        - numtcpsock
        # - numflock
        # - numpty
        # - numsiginfo
        # - tcpsndbuf
        # - tcprcvbuf
        # - othersockbuf
        # - dgramrcvbuf
        # - numothersock
        # - dcachesize
        - numfile
        # - numiptent
        # - swappages
'''
import os
# from copy import deepcopy
import pylegraf_lib as plib
DEFAULT_SETTINGS = {
    'bc_file':     '/proc/bc/resources',
    'conf_dir':    '/etc/vz/conf',
    'ctname':      'shortid',
    'held':        False,
    'held_prc':    True,
    'fails':       False,
    'total_fails': True,
    'send_zeros':  False,
    'prec':        1,
    'exclude_ct0': True,
    'rows':        ['numproc', 'physpages', 'numtcpsock', 'numfile'],
}
# CJOIN = plib.CacheForFunction(plib.join_metric)


class Container():
    instances = {}
    ctname    = 'id'
    conf_dir  = '/etc/vz/conf'

    def __init__(self, ctid):
        cls = self.__class__
        self.ctid        = ctid
        self._make_shortid()
        self.report_name = None
        self.resources   = None
        self.fails       = None
        self.total_fails = None
        self.failcnt     = {}
        cls.instances[ctid] = self

    def _make_shortid(self):
        used_short_ids = [a.shortid for a in self.__class__.instances.values()]
        shortid = self.ctid
        for cut_position in range(4, len(self.ctid)):
            if not self.ctid[cut_position - 1].isalnum(): continue
            shortid = self.ctid[:cut_position]
            if shortid not in used_short_ids: break
        self.shortid = shortid

    def get_report_name(self):
        cls = self.__class__

        if cls.ctname in ('id', 'shortid', 'name') and self.report_name:
            return self.report_name

        self.report_name = None
        if cls.ctname == 'id':
            name = self.ctid
        elif cls.ctname == 'shortid':
            name = self.shortid
        else:
            name = self._get_ct_parameter(cls.ctname)
            if not name:
                name = self.shortid
        name = plib.convert_to_metric(name)
        used_names = [a.report_name for a in cls.instances.values()]
        while name in used_names:
            name = name + '-' + plib.convert_to_metric(self.shortid)
        self.report_name = name
        return name

    def _get_ct_parameter(self, parameter):
        filename = self.__class__.conf_dir + '/' + self.ctid + '.conf'
        try:
            with open(filename, 'r') as fd: filedata = fd.readlines()
        except OSError:
            filedata = []
        for line in filedata:
            line = line.lower().strip()
            if line.startswith(parameter + '='):
                return line[len(parameter) + 1:].strip(' \t\n\'"')

    def calculate_resources(self, new_resources):
        if self.resources is None:
            self.resources = new_resources
            self.fails = {a: 0 for a in new_resources.keys()}
            self.total_fails = 0
            return

        for resource in new_resources.keys():
            old_failcnt = self.resources[resource]['failcnt']
            new_failcnt = new_resources[resource]['failcnt']
            self.fails[resource] = max(new_failcnt - old_failcnt, 0)

        self.total_fails = sum([a for a in self.fails.values()])
        self.resources = new_resources


def setup(pi):
    pi.apply_default_settings(DEFAULT_SETTINGS)
    s = pi.settings

    if not os.path.isfile(s['bc_file']):
        pi.log_critical('Wrong beancounters file (bc_file).')
        return False

    if not os.path.isdir(s['conf_dir']):
        pi.log_critical('Wrong openvz conf directory (conf_dir).')
        return False
    Container.conf_dir = s['conf_dir']

    s['ctname'] = s['ctname'].lower().strip()
    if s['ctname'] not in ('id', 'shortid', 'name', 'hostname'):
        pi.log_error('Wrong "ctname". Use default "{}".'
                     .format(DEFAULT_SETTINGS['ctname']))
        s['ctname'] = DEFAULT_SETTINGS['ctname']
    Container.ctname = s['ctname']

    if not any([s['held'], s['held_prc'],
                s['fails'], s['total_fails']]):
        pi.log_critical('Nothing to report.')
        return False

    s['rows'] = set([str(a).strip().lower() for a in s['rows']]) - {''}
    if not s['rows']:
        pi.log_error('No rows has been selected. Use default.')
        s['rows'] = DEFAULT_SETTINGS['rows']

    return True


def execute(pi):
    s = pi.settings
    # Get data from beancounters file and interpret resource counters.
    all_resources = read_bc_file(s['bc_file'])
    if s['exclude_ct0'] and '0' in all_resources: del(all_resources['0'])
    if not all_resources: return

    for ctid in (set(Container.instances) - set(all_resources)):
        del(Container.instances[ctid])

    for ctid, resources in all_resources.items():
        if ctid in Container.instances:
            container = Container.instances[ctid]
        else:
            container = Container(ctid)
        container.calculate_resources(resources)

    # Report requested helds.
    for container in Container.instances.values():
        container_name = container.get_report_name()
        for row in s['rows']:
            row = plib.convert_to_metric(row)
            try: resource = container.resources[row]
            except KeyError: continue

            if s['held']:
                mname = '.'.join(('held', container_name, row))
                mval = resource['held']
                if not s['send_zeros'] and not mval: continue
                pi.send_metric(mname=mname, mval=mval)

            if s['held_prc']:
                mname = '.'.join(('held_prc', container_name, row))
                mval = plib.in_percents(resource['held'],
                                        resource['barrier'],
                                        s['prec'])
                if s['prec'] < 1: mval = int(mval)
                if not s['send_zeros'] and not mval: continue
                pi.send_metric(mname=mname, mval=mval)

    # Report requested fails.
    for container in Container.instances.values():
        container_name = container.get_report_name()

        if s['fails']:
            for parameter in container.failcnt.keys():
                parameter = plib.convert_to_metric(parameter)
                mname = '.'.join(('fails', container_name, parameter, 'count'))
                mval = container.failcnt[parameter]
                if not s['send_zeros'] and not mval: continue
                pi.send_metric(mname=mname, mval=mval, mtype='count')

        if s['total_fails']:
            mname = '.'.join(('total_fails', container_name, 'count'))
            mval = container.total_fails
            if not s['send_zeros'] and not mval: continue
            pi.send_metric(mname=mname, mval=mval, mtype='count')


def read_bc_file(filename='/proc/bc/resources'):
    with open(filename, 'r') as fd: filedata = fd.readlines()
    all_resources = {}
    parameters = ()
    for line in filedata:
        fields = line.split()
        if len(fields) < 6: continue
        if fields[0] == 'uid':
            parameters = fields[2:]
            continue
        if fields[0].endswith(':'):
            ctid = fields[0][:-1]
            all_resources[ctid] = {}
            fields = fields[1:]
        resource = fields[0]
        values = tuple(map(int, fields[1:]))
        all_resources[ctid][resource] = dict(zip(parameters, values))

    return all_resources


def test(pi):
    # all_resources = read_bc_file(DEFAULT_SETTINGS['bc_file'])
    if not os.path.isfile('/proc/bc/resources'):
        pi.log_warning('Resource file not found.')
        return True
    all_resources = read_bc_file()

    return True


# $ sudo cat /proc/bc/resources
# Version: 2.5
#        uid  resource                     held              maxheld              barrier                limit              failcnt
# cd0aa2e...: kmemsize                 30949376            161091584  9223372036854775807  9223372036854775807                    0
#             lockedpages                     0                    0  9223372036854775807  9223372036854775807                    0
#             privvmpages                247389               482464  9223372036854775807  9223372036854775807                    0
#             shmpages                       64                 2057  9223372036854775807  9223372036854775807                    0
#             numproc                        75                   75               131072               131072                    0
#             physpages                  144866               524289               524288               524288                    0
#             vmguarpages                     0                    0  9223372036854775807  9223372036854775807                    0
#             oomguarpages               144866               524289                    0                    0                    0
#             numtcpsock                      0                    0  9223372036854775807  9223372036854775807                    0
#             numflock                       34                   40  9223372036854775807  9223372036854775807                    0
#             numpty                          0                    8  9223372036854775807  9223372036854775807                    0
#             numsiginfo                      0                   81  9223372036854775807  9223372036854775807                    0
#             tcpsndbuf                       0                    0  9223372036854775807  9223372036854775807                    0
#             tcprcvbuf                       0                    0  9223372036854775807  9223372036854775807                    0
#             othersockbuf                    0                    0  9223372036854775807  9223372036854775807                    0
#             dgramrcvbuf                     0                    0  9223372036854775807  9223372036854775807                    0
#             numothersock                    0                    0  9223372036854775807  9223372036854775807                    0
#             dcachesize               11874304            135892992  9223372036854775807  9223372036854775807                    0
#             numfile                       680                 1730  9223372036854775807  9223372036854775807                    0
#             numiptent                      61                   61                 2000                 2000                    0
#             swappages                       0                    0                    0                    0                    0
#          0: kmemsize               2726948864           4735410176  9223372036854775807  9223372036854775807                    0
#             lockedpages                 32010                38800  9223372036854775807  9223372036854775807                    0
#             privvmpages                538407              2577343  9223372036854775807  9223372036854775807                    0
#             shmpages                    78397                88605  9223372036854775807  9223372036854775807                    0
#             numproc                      3413                 3413  9223372036854775807  9223372036854775807                    0
#             physpages                 7580713              7694869  9223372036854775807  9223372036854775807                    0
#             vmguarpages                     0                    0                    0  9223372036854775807                    0
#             oomguarpages              7580713              7952852                    0                    0                    0
#             numtcpsock                      0                    0  9223372036854775807  9223372036854775807                    0
#             numflock                       10                   47  9223372036854775807  9223372036854775807                    0
#             numpty                          1                    4  9223372036854775807  9223372036854775807                    0
#             numsiginfo                      0                  111  9223372036854775807  9223372036854775807                    0
#             tcpsndbuf                       0                    0  9223372036854775807  9223372036854775807                    0
#             tcprcvbuf                       0                    0  9223372036854775807  9223372036854775807                    0
#             othersockbuf                    0                    0  9223372036854775807  9223372036854775807                    0
#             dgramrcvbuf                     0                    0  9223372036854775807  9223372036854775807                    0
#             numothersock                    0                    0  9223372036854775807  9223372036854775807                    0
#             dcachesize             1943883776           4008382464  9223372036854775807  9223372036854775807                    0
#             numfile                      1998                 2865  9223372036854775807  9223372036854775807                    0
#             numiptent                      69                  567  9223372036854775807  9223372036854775807                    0
#             swappages                       0                    0  9223372036854775807  9223372036854775807                    0

#        uid  resource        PARAMETERS-> held              maxheld
#             RESOURCES                  VALUES
#                 |                         |
#                 V                         V
# CTID-> 201: kmemsize               2726948864           4735410176
#             lockedpages                 32010                38800
#             privvmpages                538407              2577343

