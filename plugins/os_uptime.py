#!/usr/bin/env python3
__doc__ = '''
System uptime (parse /proc/uptime).

----- settings -----
plugins:
  os_uptime:
    module: os_uptime
    settings:
      src_file: /proc/uptime  #  Data source.
      up_seconds: true        #  Report uptime in seconds.
      up_minutes: false       #  Report uptime in minutes.
      up_hours: false         #  Report uptime in hours.
      up_days: false          #  Report uptime in days.
      work_part: false        #  How much the processor has working.
      idle_part: false        #  How much the processor has spent idle.
'''
import os
import sys
import pylegraf_lib as plib

DEFAULT_SETTINGS = {
    'src_file':   '/proc/uptime',
    'up_seconds': True,
    'up_minutes': False,
    'up_hours':   False,
    'up_days':    False,
    'work_part':  False,
    'idle_part':  False,
}


def how_much_cpus():
    '''Report number of processors in system.
    '''
    # Try to get data from /proc/stat.
    with open('/proc/stat', 'r') as fd: source_lines = fd.readlines()
    cpu_quantity = 0
    for line in source_lines:
        if not line.startswith('cpu'): continue
        if line.startswith('cpu '): continue
        cpu_name = line[:line.find(' ')]
        cpu_number = cpu_name[3:]
        if cpu_number.isdigit(): cpu_quantity += 1
    if cpu_quantity: return cpu_quantity

    # Get data from /proc/cpuinfo
    with open('/proc/cpuinfo', 'r') as fd: source_lines = fd.readlines()
    cpu_quantity = 0
    for line in source_lines:
        if not line.startswith('processor '): continue
        field, cpu_number = line.split(':')
        field = field.strip()
        cpu_number = cpu_number.strip()
        if field == 'processor' and cpu_number.isdigit(): cpu_quantity += 1
    return cpu_quantity


CPU_QUANTITY = how_much_cpus()
PREVIOUS_UPTIME = {}


def setup(pi):
    pi.apply_default_settings(DEFAULT_SETTINGS)
    s = pi.settings
    if not os.path.isfile(s['src_file']):
        pi.log_critical('Wrong "src_file".')
        return False
    if not any([s['up_seconds'],
                s['up_minutes'],
                s['up_hours'],
                s['up_days'],
                s['work_part'],
                s['idle_part']]):
        pi.log_critical('Select at least one option.')
        return False
    if s['work_part'] or s['idle_part']:
        if not CPU_QUANTITY:
            pi.log_error('Can`t calculate cpu quantity.'
                         ' Ignore "work_part" and "idle_part" options.')
            s['work_part'], s['idle_part'] = False, False
    return True


def execute(pi):
    s = pi.settings
    global PREVIOUS_UPTIME

    uptime = get_system_uptime(s['src_file'])
    if not uptime:
        pi.log_critical('Wrong format of source file.')
        return False

    # Report uptime.
    if s['up_seconds']:
        pi.send_metric(mname='up_seconds', mval=uptime['up'])
    if s['up_minutes']:
        pi.send_metric(mname='up_minutes', mval=round(uptime['up'] / 60, 2))
    if s['up_hours']:
        pi.send_metric(mname='up_hours', mval=round(uptime['up'] / 3600, 2))
    if s['up_days']:
        pi.send_metric(mname='up_days', mval=round(uptime['up'] / 86400, 2))
    if not s['work_part'] and not s['idle_part']: return True

    # Calculate work time for all cpus, like idle time.
    uptime['work'] = uptime['up'] * CPU_QUANTITY - uptime['idle']

    # If this is a first successfull execution, just save data.
    if pi.successes_in_row == 0 or pi.sleep_time <= 0:
        PREVIOUS_UPTIME = uptime
        return True

    # Report work and idle parts.
    workpart = uptime['work'] - PREVIOUS_UPTIME['work']
    idlepart = uptime['idle'] - PREVIOUS_UPTIME['idle']
    wholetime = workpart + idlepart
    if s['work_part']:
        mval = plib.in_percents(workpart, wholetime, 2)
        pi.send_metric(mname='work_part', mval=mval)
    if s['idle_part']:
        mval = plib.in_percents(idlepart, wholetime, 2)
        pi.send_metric(mname='idle_part', mval=mval)

    PREVIOUS_UPTIME = uptime
    return True


def get_system_uptime(filename='/proc/uptime'):
    with open(filename, 'r') as fd: data = fd.read()
    data = data.strip().split()
    uptime = {}
    if len(data) > 1 and data[0] and data[1]:
        uptime['up'] = int(data[0][:data[0].find('.')])
        uptime['idle'] = int(data[1][:data[1].find('.')])

    return uptime


def test(pi):
    from time import sleep
    cpu_number = how_much_cpus()
    assert cpu_number > 0
    uptime1 = get_system_uptime()
    assert uptime1['up'] > 0
    sleep(1.5)
    uptime2 = get_system_uptime()
    assert 1 <= (uptime2['up'] - uptime1['up']) <= 2

    return True


# $ cat /proc/uptime
# 7149.89 19213.10
