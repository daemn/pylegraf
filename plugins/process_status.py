#!/usr/bin/env python3
__doc__ = '''
Report a status of the process.
The process must be marked with environ variable PYLEGRAF_WATCHID.

----- settings -----
plugins:
  process_status:
    module: process_status
    settings:
      proc_dir:   /proc    # The "proc" directory.
      watchid:    ''       # ID of the watch process. PYLEGRAF_WATCHID=<id>
      threads:    false    # Report a number of threads.
      open_files: true     # Report a number of open files.
'''
import sys
import os
import pylegraf_lib as plib

DEFAULT_SETTINGS = {
    'proc_dir':   '/proc',
    'watchid':    '',
    'threads':    False,
    'open_files': True,
}
SEARCH_VAR = None
PID        = None


def setup(pi):
    global SEARCH_VAR
    pi.apply_default_settings(DEFAULT_SETTINGS)
    s = pi.settings

    s['proc_dir'] = s['proc_dir'].rstrip('/')
    if not os.path.isdir(s['proc_dir']):
        pi.log_critical('Wrong "proc_dir".')
        return False

    if not s['watchid']:
        pi.log_critical('Set process watch id.')
        return False

    if not s['threads'] and not s['open_files']:
        pi.log_critical('Select something for report.')
        return False

    SEARCH_VAR = 'PYLEGRAF_WATCHID=' + s['watchid']


def check_pdir(process_dir):
    r'''Is the process_dir are directory for the marked process.
    '''
    try:
        with open(process_dir + '/environ', 'rb') as fd: environ = fd.read()
    except OSError:
        return False
    environ_vars = [a.decode('utf-8') for a in environ.split(b'\x00')]
    if SEARCH_VAR in environ_vars: return True
    return False


def search_pid(proc_dir='/proc'):
    r'''Search directory of the desired process.
    '''
    all_pids = [a for a in os.listdir(proc_dir) if a.isnumeric()]
    for pid in all_pids:
        if check_pdir(proc_dir + '/' + pid): return pid


def get_threads_number(process_dir):
    with open(process_dir + '/status', 'r') as fd: data = fd.readlines()
    for line in data:
        if line.startswith('Threads:'): return int(line.split()[1])
    return None


def get_open_files_limit(process_dir):
    with open(process_dir + '/limits', 'r') as fd: data = fd.readlines()
    for line in data:
        if line.startswith('Max open files '):
            limit = line.split()[3]
            if limit == 'unlimited': return 0
            else: return int(limit)
    return None


def calc_open_files(process_dir):
    return len(os.listdir(process_dir + '/fd'))


def execute(pi):
    global PID
    s = pi.settings

    if not PID or not check_pdir(s['proc_dir'] + '/' + PID):
        PID = search_pid(s['proc_dir'])
    if not PID: return True
    process_dir = s['proc_dir'] + '/' + PID

    # Report threads
    if s['threads']:
        threads_number = get_threads_number(process_dir)
        if threads_number is not None:
            pi.send_metric(mname='threads.abs',
                           mtype='gauge',
                           mval=threads_number)

    # Report open files.
    if s['open_files']:
        open_files = calc_open_files(process_dir)
        if open_files is not None:
            pi.send_metric(mname='open_files.abs',
                           mtype='gauge',
                           mval=open_files)
        open_files_limit = get_open_files_limit(process_dir)
        if open_files is not None and open_files_limit is not None:
            mval = int(plib.in_percents(open_files, open_files_limit, 0))
            pi.send_metric(mname='open_files.prc',
                           mtype='gauge',
                           mval=mval)

    return True


def test(pi):
    if not os.path.isdir('/proc'):
        print('WARNING. Don`t see the "/proc" dir.')
        return False
    assert check_pdir('/proc/0') is False
    pid = search_pid()
    if pid is None: pid = '1'
    process_dir = '/proc/' + pid
    assert get_threads_number(process_dir) > 0
    assert get_open_files_limit(process_dir) > 0
    assert calc_open_files(process_dir) >= 0

    return True

### /proc/19417/status
# Threads:    1
# ...

### /proc/19417/limits
# Limit                     Soft Limit           Hard Limit           Units
# Max open files            1024                 1048576              files
# ...

### /proc/19417/fd/
# 0
# 1
# 2
# 255
