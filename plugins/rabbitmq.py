#!/usr/bin/env python3
__doc__ = '''
Collect rabbitmq statistic.
Information about metrics: https://www.rabbitmq.com/monitoring.html

You can specify any web-api metric as target.
Targets may have a default numeric value like this: 'stat.total = 0'.

----- settings -----
plugins:
  rabbitmq:
    module: rabbitmq
    settings:
      host:       "localhost" # \
      port:       15672       # | RabbitMQ WEB-API connection parameters.
      user:       "guest"     # | Now the plugin only supports http requests.
      password:   "guest"     # /
      send_zeros: false       # Send metrics with zero value.
      report_overview:        # Total statistic.
        - object_totals.channels
        - object_totals.connections
        - object_totals.consumers
        - object_totals.exchanges
        - object_totals.queues
      report_queues:          # Statistic for every queue.
        - consumers
        - messages
        - memory
      report_nodes:       []  # Stats for all cluster members
      report_vhosts:      []
      report_channels:    []
      report_connections: []
      report_exchanges:   []
'''
import pylegraf_lib as plib
import base64
import requests

DEFAULT = {
    'host':     'localhost',
    'port':     15672,
    'user':     'guest',
    'password': 'guest',
    'send_zeros':     False,
    'report_overview': ['object_totals.channels',
                        'object_totals.connections',
                        'object_totals.consumers',
                        'object_totals.exchanges',
                        'object_totals.queues'],
    'report_queues':   ['consumers',
                        'messages',
                        'memory'],
    'report_nodes':       [],
    'report_vhosts':      [],
    'report_channels':    [],
    'report_connections': [],
    'report_exchanges':   [],
}
URL     = None
HEADERS = None
REPORTS_LIST = ('overview', 'queues', 'nodes', 'vhosts',
                'channels', 'connections', 'exchanges')


def setup(pi):
    pi.apply_default_settings(DEFAULT)
    s = pi.settings

    if not s['host']:
        pi.log_critical('Empty "host".')
        return False

    if s['port'] < 0 or s['port'] > 65535:
        pi.log_critical('Wrong "port".')
        return False

    for report in REPORTS_LIST:
        option_name = 'report_' + report
        s[option_name] = set([str(a).strip() for a in s[option_name]]) - {''}
    if not any([len(s['report_' + a]) > 0 for a in REPORTS_LIST]):
        pi.log_critical('No options for send.')
        return False

    global URL, HEADERS
    auth_string = '{}:{}'.format(s['user'], s['password'])
    auth_string = base64.b64encode(auth_string.encode('utf-8')).decode('utf-8')
    HEADERS = {'Authorization': 'Basic ' + auth_string}
    URL = 'http://{}:{}/api/'.format(s['host'], s['port'])

    return True


def execute(pi):
    s = pi.settings
    metrics_for_send = []

    for report in REPORTS_LIST:
        option_name = 'report_' + report
        if not s[option_name]: continue
        api_url = URL + report
        raw_data = request_data(api_url)
        if raw_data is None:
            pi.log_error('Get ' + report + ' error.')
            continue
        if report == 'overview':
            raw_metrics = get_metrics(raw_data, s[option_name])
        else:
            raw_metrics = get_metrics_for_all_units(raw_data, s[option_name])
        for name, value in raw_metrics:
            metrics_for_send.append((report + '.' + name, value))

    for metric_name, metric_value in metrics_for_send:
        if metric_value or s['send_zeros']:
            pi.send_metric(mname=metric_name, mtype='gauge', mval=metric_value)

    return True


def request_data(url):
    try: page = requests.get(url, headers=HEADERS)
    except Exception: return None
    if page.status_code != 200: return None
    return plib.read_json(page.text)


def get_target(data, target):
    ### data example:
    # {"message_stats": {
    #     "ack": 24925828273,
    #     "ack_details": {"rate": 160.6 },
    #  },
    #  "object_totals": {
    #     "channels": 621,
    #     "connections": 331,
    #  },
    #  "queue_totals": {
    #     "messages": 192578,
    #     "messages_details": {"rate": -23.8 }}}
    #
    ### target example:
    # "message_stats.ack_details.rate"
    for chain in target.split('.'):
        data = data[chain]
    if not isinstance(data, int) and not isinstance(data, float):
        raise TypeError('Target is not number.')
    return data


def get_metrics(data, targets):
    ### data example:
    # {"message_stats": {
    #     "ack": 24925828273,
    #     "ack_details": {"rate": 160.6 },
    #  },
    #  "object_totals": {
    #     "channels": 621,
    #     "connections": 331,
    #  },
    #  "queue_totals": {
    #     "messages": 192578,
    #     "messages_details": {"rate": -23.8 }}}
    #
    ### targets example:
    # ["message_stats",
    #  "ack",
    #  "deliver_details.rate",
    #  "object_totals",
    #  "channels",
    #  "queue_totals",
    #  "messages_details.rate"]
    metrics = []
    for target in targets:
        if target.find('=') > -1:
            target, default_value = target.split('=')
            target = target.strip()
            default_value = default_value.replace(',', '.')
            if default_value.find('.') > -1:
                default_value = float(default_value)
            else:
                default_value = int(default_value)
        else:
            default_value = None
        try:
            metrics.append((target, get_target(data, target)))
        except (KeyError, TypeError):
            if default_value is not None:
                metrics.append((target, default_value))

    return metrics


def get_metrics_for_all_units(data, targets):
    ### data example:
    # [{"messages":0,
    #   "name":"ddexDo-Album",
    #   "message_bytes":0,
    #   "consumers":0},
    #  {"messages":0,
    #   "message_stats":{"deliver_get_details":{"rate":0.0},
    #                    "publish_details":{"rate":0.0}},
    #   "name":"ddexDo-PostProcessTrack",
    #   "message_bytes":0,
    #   "consumers":1}]
    #
    ### targets example:
    # ['backing_queue_status.avg_ack_egress_rate',
    #  'consumers',
    #  'memory',
    #  'message_bytes',
    #  'message_stats.deliver_get_details.rate']
    metrics = []
    for item_data in data:
        try: item_name = plib.convert_to_metric(item_data['name'])
        except KeyError: continue
        item_metrics = get_metrics(item_data, targets)
        for item_metric_name, item_metric_value in item_metrics:
            metrics.append((item_name + '.' + item_metric_name, item_metric_value))

    return metrics


def test(pi):
    from copy import deepcopy

    ### def setup()

    # Wrong option 'host'
    pi.settings = deepcopy(DEFAULT)
    pi.settings['host'] = ''
    assert setup(pi) is False

    # Wrong option 'port'
    pi.settings = deepcopy(DEFAULT)
    pi.settings['port'] = 70000
    assert setup(pi) is False

    # Everything is ok.
    pi.settings = deepcopy(DEFAULT)
    pi.settings['user']     = 'guest'
    pi.settings['password'] = 'guest'
    assert setup(pi) is True
    assert HEADERS['Authorization'] == 'Basic Z3Vlc3Q6Z3Vlc3Q='

    ### def get_target()

    F = get_target
    data = {
        "node": "rabbit@zina",
        "message_stats": {
            "ack": 24925828273,
            "ack_details": {"rate": 160.6},
            "confirm": 129979471,
            "confirm_details": {"rate": 0.0},
            "deliver": 26143219554,
            "deliver_details": {"rate": 176.8},
        },
    }
    assert F(data, 'message_stats.ack') == 24925828273
    assert F(data, 'message_stats.confirm_details.rate') == 0.0
    try:
        F(data, 'node')
        raise AssertionError('No error in case of wrong type.')
    except TypeError: pass
    try:
        F(data, 'unknown.target')
        raise AssertionError('No error in case of wrong target name.')
    except KeyError: pass

    ### def get_metrics()

    data = {
        "management_version": "3.6.14",
        "message_stats": {
            "ack": 24925828273,
            "ack_details": {"rate": 160.6},
            "confirm": 129979471,
            "confirm_details": {"rate": 0.0},
            "deliver": 26143219554,
            "deliver_details": {"rate": 176.8},
        },
        "node": "rabbit@zina",
        "object_totals": {
            "channels": 621,
            "connections": 331,
            "consumers": 610,
            "exchanges": 21,
            "queues": 110
        },
        "queue_totals": {
            "messages": 192578,
            "messages_details": {"rate": -23.8},
        },
    }
    targets = ['message_stats.ack_details.rate = 0',
               'message_stats.ack_details',
               'message_stats.may_exists = 0',
               'message_stats.may_exists_too = -5.3',
               'queue_totals.messages']
    assert get_metrics(data, targets) == [
        ('message_stats.ack_details.rate', 160.6),
        ('message_stats.may_exists', 0),
        ('message_stats.may_exists_too', -5.3),
        ('queue_totals.messages', 192578)
    ]

    ### def get_metrics_for_all_units()

    data = [{
        'consumers': 0,
        'message_bytes': 0,
        'messages': 0,
        'name': 'ddexDo-Album'
    }, {
        'consumers': 1,
        'message_bytes': 0,
        'message_stats': {
            'deliver_get_details': {'rate': 0.0},
            'publish_details': {'rate': 0.0}},
        'messages': 0,
        'name': 'ddexDo-PostProcessTrack'
    }]
    targets = ['consumers',
               'message_bytes',
               'message_stats.deliver_get_details.rate',
               'message_stats.publish_get_details.rate']
    assert get_metrics_for_all_units(data, targets) == [
        ('ddexDo-Album.consumers', 0),
        ('ddexDo-Album.message_bytes', 0),
        ('ddexDo-PostProcessTrack.consumers', 1),
        ('ddexDo-PostProcessTrack.message_bytes', 0),
        ('ddexDo-PostProcessTrack.message_stats.deliver_get_details.rate', 0.0)
    ]

    return True
