#!/usr/bin/env python3
__doc__ = '''
Plugin for tests. Generate random numbers.

----- settings -----
plugins:
  random_numbers:
    module: random_numbers
    settings:
      amplitude: 50  # Amplitude of graph (>0).
      jitter: 10     # Jitter of graph (>=0).
'''
from random import randint
from math import sin, radians
from time import localtime

# We will use this for automatically typecast user settings.
DEFAULT_SETTINGS = {
    'amplitude': 50,
    'jitter':    10,
}


# Function "setup" used for check settings and setup plugin environment.
# Link to PluginInterface will where given as argument.
def setup(pi):

    # This method merge given and user settings, and put result to pi.settings.
    pi.apply_default_settings(DEFAULT_SETTINGS)
    s = pi.settings

    # Check user settings for limits and errors.
    if s['amplitude'] < 1: s['amplitude'] = 1
    if s['jitter'] < 0: s['jitter'] = 0


# Function "execute" is requred. It used for collecting or processing
# (sending) metrics.
# Link to PluginInterface will where given as argument.
def execute(pi):

    s = pi.settings
    # Calculate metric value.
    perlin = sin(radians(localtime().tm_min * 6))
    value = perlin * s['amplitude']
    value = int(value + (randint(-s['jitter'], s['jitter'])))

    # Send metric to plugins whom registered as recievers.
    pi.send_metric(mtime=None,
                   mname='',
                   mtype='gauge',
                   mval=value)


# This function is used to clean up after plugin.
# Link to PluginInterface will where given as argument.
def destroy(pi):
    pass
