#!/usr/bin/env python3
__doc__ = '''
Parse logs and count rows.

----- example -----
foo.log:

0.3s | ok  | take a 45 red baloons
0.5s | err | take a 2 blue baloons
0.5s | ok  | take a baloons

settings:

file:  foo.log
count: true
cps:   false
lines:
  colored:   " (red|blue) "
  colorless: "a baloons"
  ok:        "| ok  |"

result:

host.row_counter.colored.count,   count, 2
host.row_counter.colorless.count, count, 1
host.row_counter.ok.count,        count, 2

----- settings -----
plugins:
  row_counter:
    module: row_counter
    settings:
      file:  []          # Read data from these files.
      send_zeros: false  # Send metrics with zero value.
      count: false       # Report the number of occurences.
      cps:   true        # Same, but per second.
      lines: {}          # Lines for search.
'''
import os
import re
re_lines = {}


def setup(pi):
    global re_lines
    default_settings = {
        'file':  [],
        'send_zeros': False,
        'count': False,
        'cps':   True,
        'lines': {},
    }
    if not pi.apply_default_settings(default_settings): return False
    s = pi.settings

    # Check settings
    if not any((s['count'], s['cps'])):
        pi.log_critical('No parameters for send.')
        return False

    s['file'] = [a for a in map(str, s['file']) if a]
    if not s['file']:
        pi.log_critical('No files for watch.')
        return False

    # Prepare search lines
    for name, expr in s['lines'].items():
        expr = str(expr)
        if not expr: continue
        try:
            re_lines[name] = re.compile(expr)
        except re.error:
            pi.log_error('Wrong search expression: "{}"'.format(expr))
            return False

    if not re_lines:
        pi.log_critical('No search lines.')
        return False

    # Prepare LogFollower
    for file in s['file']:
        if not os.path.isfile(file):
            pi.log_warning('Start with no source file "{}".'.format(file))
        LogFollower(file)

    return True


def execute(pi):
    s = pi.settings

    # Get all new lines
    new_lines = []
    for log in LogFollower.instances: new_lines += log.read_lines()
    if not new_lines: return

    # Count lines
    counters = {a: 0 for a in s['lines'].keys()}
    for line in new_lines:
        for name, expr in re_lines.items():
            if expr.search(line): counters[name] += 1

    # Out result
    for name, value in counters.items():
        if value == 0 and not s['send_zeros']: continue
        if s['count']:
            pi.send_metric(
                mname=name + '.count',
                mval=value,
                mtype='count'
            )
        if s['cps']:
            try: mval = value / pi.sleep_time
            except ZeroDivisionError: mval = value
            pi.send_metric(
                mname=name + '.cps',
                mval=mval
            )

    return True


class LogFollower():
    instances = []

    def __init__(self, filename):
        self.filename = filename
        self.position = None
        self.size     = None
        if self._check_file_size(): self.position = self.size
        self.__class__.instances.append(self)

    def _check_file_size(self):
        try: self.size = os.path.getsize(self.filename)
        except OSError: return False
        return True

    def read_lines(self):
        lines = []
        if not self._check_file_size():
            self.position = None
            return lines
        if self.position is None:
            self.position = self.size
            return lines
        if self.position > self.size: self.position = 0
        try:
            with open(self.filename, 'r') as fd:
                if self.position > 0: fd.seek(self.position)
                while self.position < self.size:
                    lines.append(fd.readline().strip())
                    self.position = fd.tell()
        except OSError: pass
        return lines


def test(pi):
    from test_lib import tmpdir, make_tmpfile, add_to_tmpfile, clear_tmpdir
    from copy import deepcopy
    test_filename = 'test_log'
    test_file = tmpdir + '/' + test_filename
    test_settings = {
        'file':  test_file,
        'send_zeros': False,
        'count': True,
        'cps':   True,
        'lines': {
            'colored':   " (red|blue) ",
            'colorless': "a baloons",
        },
    }
    clear_tmpdir()

    ### def setup()

    # Empty file name
    pi.settings = deepcopy(test_settings)
    pi.settings['file'] = ''
    assert setup(pi) is False

    # Many file names
    pi.settings = deepcopy(test_settings)
    pi.settings['file'] = ['a', 'b', 'c']
    assert setup(pi) is True

    # Nothing to report
    pi.settings = deepcopy(test_settings)
    pi.settings['count'] = False
    pi.settings['cps'] = False
    assert setup(pi) is False

    # Incorrect search line
    pi.settings = deepcopy(test_settings)
    pi.settings['lines'] = ['something']
    assert setup(pi) is False

    ### class LogFollower()

    # No log file
    log = LogFollower(test_file)
    assert log.read_lines() == []

    # Empty log file
    make_tmpfile(test_filename)
    assert log.read_lines() == []

    # Add one line
    add_to_tmpfile(test_filename, '0.3s | take a 45 red baloons\n')
    assert log.read_lines() == ['0.3s | take a 45 red baloons']

    # Add two new lines
    add_to_tmpfile(test_filename, '0.5s | take a 2 blue baloons\n')
    add_to_tmpfile(test_filename, '0.5s | take a baloons\n')
    assert log.read_lines() == ['0.5s | take a 2 blue baloons',
                                '0.5s | take a baloons']

    # Renew file with one line
    make_tmpfile(test_filename, '0.3s | take a 45 red baloons\n')
    assert log.read_lines() == ['0.3s | take a 45 red baloons']

    # Remove log file
    clear_tmpdir()
    assert log.read_lines() == []

    # First correct read file. Line is ignored
    make_tmpfile(test_filename, '0.3s | take a 45 red baloons\n')
    assert log.read_lines() == []

    # Add line
    add_to_tmpfile(test_filename, '0.5s | take a 2 blue baloons\n')
    assert log.read_lines() == ['0.5s | take a 2 blue baloons']

    LogFollower.instances = []

    ### def execute()

    # Prepare for test execute()
    make_tmpfile(test_filename)
    LogFollower.instances = []
    pi.settings = deepcopy(test_settings)
    pi.settings['file'] = test_file
    assert setup(pi) is True
    execute(pi)

    # Test execute()
    add_to_tmpfile(test_filename, '0.3s | take a 45 red baloons\n')
    add_to_tmpfile(test_filename, '0.5s | take a 2 blue baloons\n')
    add_to_tmpfile(test_filename, '0.5s | take a baloons\n')
    add_to_tmpfile(test_filename, '0.1s | no baloons\n')
    pi.sleep_time = 2
    pi.outbox = []
    execute(pi)
    assert len(pi.outbox) == 4

    return True
