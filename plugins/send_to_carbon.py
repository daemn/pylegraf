#!/usr/bin/env python3
__doc__ = '''
Send a data to the Carbon (backend of Graphite).

----- settings -----
plugins:
  send_to_carbon:
    module: send_to_carbon
    settings:
      host_premetric: ''       # \
      host_metric: <hostname>  # | The host part of the metric name.
      host_postmetric: ''      # / "host_metric" is hostname as default.
      format: 'pickle'         # 'pickle' or 'plaintext'.
      host: 'localhost'        # The Carbon host.
      port: 2004               # The Carbon port.
      send_timeout: 60         # Try to send a messages during this time.
'''
import time
import pickle
import struct
import pylegraf_lib as plib
import socket

HOSTNAME = socket.getfqdn()
if HOSTNAME == 'localhost' or HOSTNAME.startswith('localhost.'):
    HOSTNAME = socket.gethostname()

DEFAULT_SETTINGS = {
    'host_premetric':  '',
    'host_metric':     plib.convert_to_metric(HOSTNAME),
    'host_postmetric': '',
    'format':          'pickle',
    'host':            'localhost',
    'port':            2004,
    'send_timeout':    60,
}
METRIC = ''
SEND_QUEUE = {}
# CJOIN = plib.CacheForFunction(plib.join_metric)


def setup(pi):
    global METRIC
    pi.i_need_metrics()
    pi.apply_default_settings(DEFAULT_SETTINGS)
    s = pi.settings
    if s['format'] not in ('pickle', 'plaintext'):
        pi.log_critical('Unknown format of messages.')
        return False
    if not (0 <= s['port'] <= 65535):
        pi.log_critical('The Carbon port is out of range.')
        return False
    if s['format'] == 'pickle' and s['port'] != 2004:
        pi.log_warning('Non-standard port for pickle format. Standard is 2004.')
    if s['format'] == 'plaintext' and s['port'] != 2003:
        pi.log_warning('Non-standard port for plaintext format. '
                       'Standard is 2003.')
    for key in ('host_premetric', 'host_metric', 'host_postmetric'):
        s[key] = plib.convert_to_metric(s[key], preserve_dot=True)
    METRIC = '.'.join((s['host_premetric'],
                       s['host_metric'],
                       s['host_postmetric']))
    return True


def execute(pi):
    global SEND_QUEUE
    # Format new messages and add to queue.
    messages = list(pi.recieve_metric())
    if messages:
        if pi.settings['format'] == 'pickle':
            SEND_QUEUE[time.time()] = format_pickle(messages)
        else:
            SEND_QUEUE[time.time()] = format_plaintext(messages)

    # Delete deprecated messages.
    dead_time = time.time() - pi.settings['send_timeout']
    for msg_index in list(SEND_QUEUE.keys()):
        if msg_index < dead_time:
            del(SEND_QUEUE[msg_index])
            pi.log_error('Drop the old message {}.'.format(msg_index))

    # Try to send messages.
    dst_host = pi.settings['host']
    dst_port = pi.settings['port']
    for msg_index in sorted(SEND_QUEUE.keys()):
        pi.log_debug('Packet size for sending is {}.'
                     .format(len(SEND_QUEUE[msg_index])))
        if plib.SendTcp(dst_host, dst_port, SEND_QUEUE[msg_index]):
            del(SEND_QUEUE[msg_index])
        else:
            pi.log_error('Error of sending message {}.'.format(msg_index))
    return True


def format_pickle(messages):
    # Example of Carbon allowed pickle format:
    # [('testhost.random.gauge', (1580668952, 40)),
    #  ('testhost.random.gauge', (1580668953, 20))]
    fdata = []
    for note in messages:
        note['mname'] = plib.convert_to_metric(note['mname'], preserve_dot=True)
        mname = '.'.join((METRIC, note['mname']))
        fdata.append((mname, (note['mtime'], note['mval'])))
    body = pickle.dumps(fdata, protocol=2)
    header = struct.pack('!L', len(body))
    bmsg = header + body
    return bmsg


def format_plaintext(messages):
    # Example of Carbon allowed plaintext format:
    # 'testhost.random.gauge 40 1580668952\n
    #  testhost.random.gauge 20 1580668953\n'
    fdata = ''
    for note in messages:
        note['mname'] = plib.convert_to_metric(note['mname'], preserve_dot=True)
        mname = '.'.join((METRIC, note['mname']))
        fdata += '{} {} {}\n'.format(mname, note['mval'], note['mtime'])
    bmsg = fdata.encode('utf8')
    return bmsg
