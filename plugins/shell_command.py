#!/usr/bin/env python3
__doc__ = '''
Execute shell cmd or script and interpret out as metrics.

----- settings -----
plugins:
  shell_command:
    module: shell_command
    settings:
      script: ''  # Something for execute.

Valid script output rows format:

METRIC VALUE           | Two fields is METRIC and VALUE always.
METRIC VALUE TYPE      | If third field is string, then this is TYPE.
TIME METRIC VALUE      | If third field is number, then first field is TIME.
TIME METRIC VALUE TYPE | Four fields is TIME, METRIC, VALUE and TYPE always.
# anything             | If first char is '#', then this is comment.

TIME is seconds since start of Unix epoch (01.01.1970).
METRIC is string or number. Alphabeticals, numbers, dot and underline is
allowed.
VALUE is integer or float number.
TYPE is 'c' (count) or 'g' (gauge). Default TYPE is gauge.
Comments and incomprehensible rows will be ignored.
You can use single or double quotes for fields. Eg in case of empty METRIC.
'''
import os
import sys
import pylegraf_lib as plib
DEFAULT = {'script': ''}


def setup(pi):
    pi.apply_default_settings(DEFAULT)
    if not pi.settings['script']:
        pi.log_critical('Script is not defined.')
        return False
    return True


def execute(pi):
    try:
        outlines = os.popen(pi.settings['script']).readlines()
    except OSError as err:
        pi.log_error('Script error.')
        pi.log_debug(str(err))
        return False
    if not outlines: return True
    for note in find_metrics(outlines):
        pi.send_metric(mtime=note[0],
                       mname=note[1],
                       mtype=note[2],
                       mval=note[3])
    return True


def find_metrics(data):
    for line in data:
        line = line.strip()
        if line.startswith('#'): continue
        fields = [a.strip('\'"').lower() for a in line.split()]
        mtime = None
        mtype = 'g'
        try:
            if len(fields) == 2:
                mname, mval = fields[:2]
            elif len(fields) == 3:
                if fields[2][0] in 'cg': mname, mval, mtype = fields[:3]
                else: mtime, mname, mval = fields[:3]
            elif len(fields) == 4:
                mtime, mname, mval, mtype = fields[:4]
            else:
                raise ValueError
            if mtime is not None: mtime = int(mtime)
            mval = plib.mask_chars(mval, ',', '.')
            if mval.find('.') > -1: mval = float(mval)
            else: mval = int(mval)
            if mtype[0] == 'c':
                mtype = 'count'
            elif mtype[0] == 'g':
                mtype = 'gauge'
            else:
                raise ValueError
        except(AttributeError, TypeError, IndexError, ValueError):
            continue
        yield (mtime, mname, mtype, mval)


def test(pi):
    for a, b in (
        (['foo 11'],      [(None, 'foo', 'gauge', 11)]),
        (['bar 11.22'],   [(None, 'bar', 'gauge', 11.22)]),
        (['123 foo 11'],  [(123,  'foo', 'gauge', 11)]),
        (['123 "" 3'],    [(123,  '',    'gauge', 3)]),
        (['bar 11.22 c'], [(None, 'bar', 'count', 11.22)]),
        (['bar 11.22 c',
          '123 "" 3'],
         [(None, 'bar', 'count', 11.22),
          (123,  '',    'gauge', 3)]),
        (['no metrics'],  []),
        (['# comment'],   []),
        ([''],            []),
        (['3'],           []),
        (['"no" ""'],     []),
        (['a a a'],       []),
        ([''],            []),
        (['3 4 5 6'],     []),
    ):
        # print(a, b, list(find_metrics(a)))
        assert list(find_metrics(a)) == b

    return True
