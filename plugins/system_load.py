#!/usr/bin/env python3
__doc__ = '''
Average system load (parse /proc/loadavg).

----- settings -----
plugins:
  system_load:
    module: system_load
    settings:
      src_file: /proc/loadavg  # Data source.
      1m: true                 #
      5m: true                 #
      15m: true                #
      trend: false             # Difference bitween 1m and 15m.
'''
import os
DEFAULT = {
    'src_file': '/proc/loadavg',
    '1m':    True,
    '5m':    True,
    '15m':   True,
    'trend': False
}


def setup(pi):
    pi.apply_default_settings(DEFAULT)
    s = pi.settings
    if not os.path.isfile(s['src_file']):
        pi.log_critical('Wrong "src_file".')
        return False
    if not any([s['1m'], s['5m'], s['15m'], s['trend']]):
        pi.log_critical('No metrics has been selected for watch.')
        return False
    return True


def execute(pi):
    # Read data from source file.
    with open(pi.settings['src_file'], 'r') as fd: data = fd.read()
    fields = data.split()
    sysload = tuple(float(a) for a in fields[:3])

    # Send metrics.
    if pi.settings['1m']:    pi.send_metric(mname='1m',  mval=sysload[0])
    if pi.settings['5m']:    pi.send_metric(mname='5m',  mval=sysload[1])
    if pi.settings['15m']:   pi.send_metric(mname='15m', mval=sysload[2])
    if pi.settings['trend']:
        pi.send_metric(mname='trend', mval=round(sysload[0] - sysload[2], 2))
    return True
