#!/usr/bin/env python3
__doc__ = '''
Report the number of tcp connections by state.
Parse "/proc/net/tcp" and "/proc/net/tcp6".

----- settings -----
plugins:
  tcp_connections:
    module: tcp_connections
    settings:
      src_file: ''       # File with source data.  ###TODO
      state: ''          # State for report.  ###TODO
      port: ''           # Number of port for report.  ###TODO
      user: ''           # User name or ID for report.  ###TODO
      process: ''        # Process name or ID for report.  ###TODO
      send_zeros: false  # Report zero values.
'''
import os
DEFAULT = {
    'send_zeros': False,
}
STATENAMES = [
    'ESTABLISHED',
    'SYN_SENT',
    'SYN_RECV',
    'FIN_WAIT1',
    'FIN_WAIT2',
    'TIME_WAIT',
    'CLOSE',
    'CLOSE_WAIT',
    'LAST_ACK',
    'LISTEN',
    'CLOSING',
    'NEW_SYN_RECV',
]
SRCFILES = ['/proc/net/tcp', '/proc/net/tcp6']


def setup(pi):
    pi.apply_default_settings(DEFAULT)
    for file in SRCFILES:
        if not os.path.isfile(file):
            pi.log_critical('Wrong source file "{}".'.format(file))
            return False
    return True


def execute(pi):
    data = []
    for file in SRCFILES:
        with open(file, 'r') as fd: rawdata = fd.readlines()
        if len(rawdata) > 1: data += rawdata[1:]

    connections = []
    for line in data:
        fields = line.split()[:10]
        connect = {}
        # sl laddr raddr st txqueue rxqueue tr tm retrnsmt uid timeout inode
        # connect['lport'] = int(fields[1].split(':')[1], 16)
        # connect['rport'] = int(fields[2].split(':')[1], 16)
        connect['state'] = int(fields[3], 16)
        # connect['uid']   = int(fields[7])
        # connect['inode'] = int(fields[9])
        connections.append(connect)
    state_list = tuple(a['state'] for a in connections)
    send_zeros = pi.settings['send_zeros']
    for state, name in enumerate(STATENAMES, 1):
        mval = state_list.count(state)
        if not send_zeros and not mval: continue
        pi.send_metric(mname=name, mtype='gauge', mval=mval)
    return True


def test(pi):
    for file in SRCFILES:
        if not os.path.isfile(file): return False
    return True


### /proc/net/tcp
#
# sl  local_address rem_address   st tx_queue rx_queue tr tm->when retrnsmt  uid timeout inode
#  0: 0100007F:1734 00000000:0000 0A 00000000:00000000 00:00000000 00000000    0       0 21237 1 00000000e74aaa02 100 0 0 10 0
#  1: 0100007F:193F 00000000:0000 0A 00000000:00000000 00:00000000 00000000 1000       0 35470 1 00000000d32ae409 100 0 0 10 0
#  2: 660A0A0A:AD1E C4DEC2AD:01BB 08 00000000:00000001 00:00000000 00000000 1000       0 29030 1 000000003261eb47 24 4 0 10 -1
#  3: 660A0A0A:E484 5EA5E940:01BB 08 00000000:00000001 00:00000000 00000000 1000       0 30018 1 00000000b5bdf2a0 24 4 22 10 -1
#  4: 660A0A0A:EB18 55F2210D:01BB 08 00000000:00000001 00:00000000 00000000 1000       0 43428 1 00000000c782d909 24 4 24 10 -1
#  5: 660A0A0A:91DC 5F49C2AD:01BB 08 00000000:00000001 00:00000000 00000000 1000       0 39869 1 00000000f15257b3 24 4 24 10 -1
#  6: 660A0A0A:8078 5EDEC2AD:01BB 01 00000000:00000000 00:00000000 00000000 1000       0 756982 1 000000002342e65f 24 4 30 7 -1
#  7: 660A0A0A:C3C6 1249C2AD:01BB 01 00000000:00000000 00:00000000 00000000 1000       0 1411189 1 000000009fb4531b 24 4 30 10 -1

### /proc/net/tcp6
#
# sl  local_address                         remote_address                        st tx_queue rx_queue tr tm->when retrnsmt uid timeout inode
#  0: 00000000000000000000000000000000:07D4 00000000000000000000000000000000:0000 0A 00000000:00000000 00:00000000 00000000   0       0 25706 1 0000000054192383 100 0 0 10 0
#  1: 00000000000000000000000000000000:0BB8 00000000000000000000000000000000:0000 0A 00000000:00000000 00:00000000 00000000   0       0 25124 1 0000000071b16794 100 0 0 10 0
#  2: 00000000000000000000000000000000:1FBD 00000000000000000000000000000000:0000 0A 00000000:00000000 00:00000000 00000000   0       0 25637 1 000000000df885b8 100 0 0 10 0
#  3: 00000000000000000000000000000000:1FBE 00000000000000000000000000000000:0000 0A 00000000:00000000 00:00000000 00000000   0       0 24971 1 000000005140ae39 100 0 0 10 0

### tcp states:
#
#  1 TCP_ESTABLISHED
#  2 TCP_SYN_SENT
#  3 TCP_SYN_RECV
#  4 TCP_FIN_WAIT1
#  5 TCP_FIN_WAIT2
#  6 TCP_TIME_WAIT
#  7 TCP_CLOSE
#  8 TCP_CLOSE_WAIT
#  9 TCP_LAST_ACK
# 10 TCP_LISTEN
# 11 TCP_CLOSING
# 12 TCP_NEW_SYN_RECV

### https://www.kernel.org/doc/Documentation/networking/proc_net_tcp.txt
#
# This document describes the interfaces /proc/net/tcp and /proc/net/tcp6.
# Note that these interfaces are deprecated in favor of tcp_diag.
#
# These /proc interfaces provide information about currently active TCP
# connections, and are implemented by tcp4_seq_show() in net/ipv4/tcp_ipv4.c
# and tcp6_seq_show() in net/ipv6/tcp_ipv6.c, respectively.
#
# It will first list all listening TCP sockets, and next list all established
# TCP connections. A typical entry of /proc/net/tcp would look like this (split
# up into 3 parts because of the length of the line):
#
#    46: 010310AC:9C4C 030310AC:1770 01
#    |      |      |      |      |   |--> connection state
#    |      |      |      |      |------> remote TCP port number
#    |      |      |      |-------------> remote IPv4 address
#    |      |      |--------------------> local TCP port number
#    |      |---------------------------> local IPv4 address
#    |----------------------------------> number of entry
#
#    00000150:00000000 01:00000019 00000000
#       |        |     |     |       |--> number of unrecovered RTO timeouts
#       |        |     |     |----------> number of jiffies until timer expires
#       |        |     |----------------> timer_active (see below)
#       |        |----------------------> receive-queue
#       |-------------------------------> transmit-queue
#
#    1000        0 54165785 4 cd1e6040 25 4 27 3 -1
#     |          |    |     |    |     |  | |  | |--> slow start size threshold,
#     |          |    |     |    |     |  | |  |      or -1 if the threshold
#     |          |    |     |    |     |  | |  |      is >= 0xFFFF
#     |          |    |     |    |     |  | |  |----> sending congestion window
#     |          |    |     |    |     |  | |-------> (ack.quick<<1)|ack.pingpong
#     |          |    |     |    |     |  |---------> Predicted tick of soft clock
#     |          |    |     |    |     |              (delayed ACK control data)
#     |          |    |     |    |     |------------> retransmit timeout
#     |          |    |     |    |------------------> location of socket in memory
#     |          |    |     |-----------------------> socket reference count
#     |          |    |-----------------------------> inode
#     |          |----------------------------------> unanswered 0-window probes
#     |---------------------------------------------> uid
#
# timer_active:
#   0  no timer is pending
#   1  retransmit-timer is pending
#   2  another timer (e.g. delayed ack or keepalive) is pending
#   3  this is a socket in TIME_WAIT state. Not all fields will contain
#      data (or even exist)
#   4  zero window probe timer is pending
