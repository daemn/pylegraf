#!/usr/bin/env python3
__doc__ = '''
Report the number of udp connections by state.
Parse "/proc/net/udp" and "/proc/net/udp6".

----- settings -----
plugins:
  udp_connections:
    module: udp_connections
    settings:
      src_file: ''       # File with source data.  ###TODO
      state: ''          # State for report.  ###TODO
      port: ''           # Number of port for report.  ###TODO
      user: ''           # User name or ID for report.  ###TODO
      process: ''        # Process name or ID for report.  ###TODO
      send_zeros: false  # Report zero values.
'''
import os
DEFAULT = {
    'send_zeros': False,
}
STATENAMES = {'ESTABLISHED': 1, 'NO_STATE': 7}
SRCFILES = ['/proc/net/udp', '/proc/net/udp6']


def setup(pi):
    pi.apply_default_settings(DEFAULT)
    for file in SRCFILES:
        if not os.path.isfile(file):
            pi.log_critical('Wrong source file "{}".'.format(file))
            return False
    return True


def execute(pi):
    data = []
    for file in SRCFILES:
        with open(file, 'r') as fd: rawdata = fd.readlines()
        if len(rawdata) > 1: data += rawdata[1:]

    connections = []
    for line in data:
        fields = line.split()[:10]
        connect = {}
        # sl laddr raddr st txqueue rxqueue tr tm retrnsmt uid timeout inode
        # connect['lport'] = int(fields[1].split(':')[1], 16)
        # connect['rport'] = int(fields[2].split(':')[1], 16)
        connect['state'] = int(fields[3], 16)
        # connect['uid']   = int(fields[7])
        # connect['inode'] = int(fields[9])
        connections.append(connect)
    state_list = tuple(a['state'] for a in connections)
    send_zeros = pi.settings['send_zeros']
    for name, state in STATENAMES.items():
        mval = state_list.count(state)
        if not send_zeros and not mval: continue
        pi.send_metric(mname=name, mtype='gauge', mval=mval)
    return True


def test(pi):
    for file in SRCFILES:
        if not os.path.isfile(file): return False
    return True


### /proc/net/udp
#
#   sl  local_address rem_address   st tx_queue rx_queue tr tm->when retrnsmt uid timeout inode ref pointer drops
# 3733: 00000000:14E9 00000000:0000 07 00000000:00000000 00:00000000 00000000 111       0 18158 2 00000000b4dc13ea 0
# 3921: 00000000:D5A5 00000000:0000 07 00000000:00000000 00:00000000 00000000 111       0 18160 2 000000002a82bfa9 0
# 4607: 00000000:9853 00000000:0000 07 00000000:00000000 00:00000000 00000000   0       0 98006 2 000000008daea4f9 0
# 6640: 660A0A0A:0044 010A0A0A:0043 01 00000000:00000000 00:00000000 00000000   0       0 20883 2 000000006fcb0b70 0
# 6695: 2601630A:007B 00000000:0000 07 00000000:00000000 00:00000000 00000000 117       0 99791 2 0000000088659fbd 0

### /proc/net/udp6
#
#   sl  local_address                         remote_address                        st tx_queue rx_queue tr tm->when retrnsmt uid timeout inode ref pointer drops
# 2000: 00000000000000000000000000000000:EE24 00000000000000000000000000000000:0000 07 00000000:00000000 00:00000000 00000000 111       0 18161 2 000000000b50ed11 0
# 3733: 00000000000000000000000000000000:14E9 00000000000000000000000000000000:0000 07 00000000:00000000 00:00000000 00000000 111       0 18159 2 00000000b2388adb 0
# 6505: 00000000000000000000000000000000:1FBD 00000000000000000000000000000000:0000 07 00000000:00000000 00:00000000 00000000   0       0 23863 2 00000000cfcbb068 0
# 6695: 000080FE000000003C5CB9DDE0C5F38C:007B 00000000000000000000000000000000:0000 07 00000000:00000000 00:00000000 00000000 117       0 99798 2 00000000646d8266 0
# 6695: 000080FE00000000FF14D7B814C81FFE:007B 00000000000000000000000000000000:0000 07 00000000:00000000 00:00000000 00000000 117       0 28962 2 00000000586287da 0

### tcp states:
#
#  1 ESTABLISHED
#  7 NO_STATE
