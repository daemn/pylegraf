#!/usr/bin/env python3
__version__ = 3
__doc__ = 'A program for collecting metrics.'

import os
import sys
import yaml
import json
from argparse import ArgumentParser
import logging, logging.handlers
LOG_FORMAT = '%(asctime)s %(levelname)s %(message)s'
LOG_HANDLER = logging.StreamHandler()
LOG_HANDLER.setFormatter(logging.Formatter(LOG_FORMAT))
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)
LOG.addHandler(LOG_HANDLER)
LOG_HANDLER.setLevel(logging.WARNING)
import dispatcher
dispatcher.LOG.addHandler(LOG_HANDLER)
import pylegraf_lib as plib


def main():
    r'''The program entry point.
    '''
    execargs = get_execution_arguments()

    # --verbose
    if execargs['verbose']: LOG_HANDLER.setLevel(logging.DEBUG)

    # --test
    if execargs['test']:
        if not os.path.isfile(execargs['test']):
            LOG.critical('File "{}" not found.'.format(execargs['test']))
        plugin_dir = os.path.dirname(execargs['test'])
        module_name = os.path.basename(execargs['test'])[:-3]
        plugin_conf = make_plugin_configuration(module_name).get_all()
        plugin_conf['name'] = module_name
        plugin_conf['module'] = module_name
        plugin_conf['plugin_metric'] = module_name
        del(plugin_conf['premetric'],
            plugin_conf['metric'],
            plugin_conf['postmetric'])
        dispatcher.use_plugin_dirs(plugin_dir)
        if not dispatcher.test_plugin(plugin_conf): return 2
        return 0

    conf = read_configuration_file(execargs['conf_file'])
    if conf is None: return 1

    # --verbose
    if execargs['verbose']: conf['log_level'] = 'debug'
    LOG_HANDLER.setLevel({'debug':    logging.DEBUG,
                          'info':     logging.INFO,
                          'warning':  logging.WARNING,
                          'error':    logging.ERROR,
                          'critical': logging.CRITICAL}[conf['log_level']])

    # --show_conf_*
    if execargs['show_conf_yaml']:
        print('# Configuration based on file "{}".'.format(execargs['conf_file']))
        print(yaml.dump(conf))
        return 0

    # Concatenate add_plugin_dirs and plugin_dirs.
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    plugin_dirs = conf['plugin_dirs'] + conf['add_plugin_dirs']
    del(conf['add_plugin_dirs'])
    conf['plugin_dirs'] = []
    for plugin_dir in plugin_dirs:
        plugin_dir = plib.absolute_path(plugin_dir)
        if not os.path.isdir(plugin_dir): continue
        conf['plugin_dirs'].append(plugin_dir)
    conf['plugin_dirs'] = set(conf['plugin_dirs'])

    # Prepare some plugin parameters.
    for plugin_name, plugin_conf in conf['plugins'].items():
        plugin_conf['name'] = plugin_name
        plugin_conf['plugin_metric'] = plib.join_metric((
            plugin_conf['premetric'],
            plugin_conf['metric'],
            plugin_conf['postmetric']))
        del(plugin_conf['premetric'],
            plugin_conf['metric'],
            plugin_conf['postmetric'])

    # Check configuration.
    if not is_configuration_valid(conf): return 1

    # Configure dispatcher.
    dispatcher.use_plugin_dirs(conf['plugin_dirs'])
    dispatcher.set_exec_mode(conf['exec_mode'])
    for plugin_name, plugin_conf in conf['plugins'].items():
        if not dispatcher.add_plugin(plugin_conf):
            LOG.error('Add plugin "{}" failed.'.format(plugin_name))

    # Run dispatcher
    with PidFile(conf['pid_file']): result = dispatcher.run()
    if result: return 0
    else: return 2


def read_configuration_file(filename='/etc/pylegraf/conf.yml'):
    r'''Read and interpret configuration file.
    '''
    file_listener = FileListener()
    file_listener.search_files(filename)
    already_read_files = []
    main_conf = make_main_configuration()
    plugins_conf = {}
    while True:
        filename = file_listener.get_next_file()
        if filename is None: break
        if filename in already_read_files: continue
        already_read_files.append(filename)

        # Try to read file.
        try: file_data = interpret_config_file(filename)
        except (TypeError, OSError) as err:
            print('Could not read file "{}". {}'.format(filename, err))
            return None

        # Parse main configuration.
        try:
            for key, value in file_data.items(): main_conf[key] = value
        except (KeyError, TypeError, ValueError) as err:
            print('Wrong option "{}" in file "{}". {}'
                  .format(key, filename, err))
            return None

        # Additional conf files.
        for add_conf_file in main_conf['add_conf']:
            file_listener.search_files(add_conf_file)
        main_conf['add_conf'] = None

        # Parse plugin configuration.
        for plugin_name, plugin_data in main_conf['plugins'].items():
            if plugin_name in plugins_conf:
                plugin_conf = plugins_conf[plugin_name]
            else:
                plugin_conf = make_plugin_configuration(
                    plib.convert_to_metric(plugin_name))

            try:
                for key, value in plugin_data.items(): plugin_conf[key] = value
            except (KeyError, TypeError, ValueError) as err:
                print('Wrong option "{}" for plugin "{}". {}'
                      .format(key, plugin_name, err))
                return None

            for key in 'premetric', 'metric', 'postmetric':
                plugin_conf[key] = plib.convert_to_metric(
                    plugin_conf[key], preserve_dot=True)

            plugins_conf[plugin_name] = plugin_conf

        main_conf['plugins'] = None

    del(main_conf['add_conf'])
    del(main_conf['plugins'])

    conf = main_conf.get_all()
    conf['plugins'] = {}
    for plugin_name, plugin_conf in plugins_conf.items():
        conf['plugins'][plugin_name] = plugin_conf.get_all()

    return(conf)


class FileListener():
    r'''Search files by mask and return filenames one by one.

    ----- examples -----
    A = FileListener()
    A.search_files('/tmp/')
    A.get_next_file()               # '/tmp/file1'
    A.get_next_file()               # '/tmp/file2'
    A.get_next_file()               # None
    A.search_files('/tmp/*1')
    A.get_next_file()               # '/tmp/file1'
    A.get_next_file()               # None
    '''
    def __init__(self):
        self._filelist = []

    def search_files(self, mask):
        r'''Add filenames in list.
        '''
        self._filelist += list(plib.list_files(mask))

    def get_next_file(self):
        r'''Return one filename and delete him from list.
        '''
        try: return self._filelist.pop(0)
        except IndexError: return None


def interpret_config_file(filename):
    r'''Read and interpret data from file.
    '''
    def read_yaml(rawdata): return yaml.safe_load(rawdata)
    def read_json(rawdata): return json.loads(rawdata)

    with open(filename) as fd: rawdata = fd.read()

    filename_extention = filename.split('.')[-1]
    if filename_extention == 'json':
        funcs = (read_json, read_yaml)
    elif filename_extention == 'yaml' or filename_extention == 'yml':
        funcs = (read_yaml, read_json)
    else:
        funcs = (read_yaml, read_json)

    data = None
    for func in funcs:
        try:
            data = func(rawdata)
            break
        except Exception:
            continue
    if not isinstance(data, dict):
        raise TypeError('Wrong file format.')

    return data


def make_main_configuration():
    r'''Return new main configuration.
    '''
    conf = plib.Options()
    conf.add_option('plugin_dirs', default=['plugins'])
    conf.add_option('pid_file',    default='')
    conf.add_option('add_conf',    default=[])
    conf.add_option('plugins',     default={})
    # conf.add_option('storage_dir', default='/tmp/pylegraf/storage')
    conf.add_choices_option('log_level', default='warning',
                            choices=['debug', 'info', 'warning',
                                     'error', 'critical'])
    conf.add_choices_option('exec_mode', default='threads',
                            choices=('once', 'single', 'threads'))
    conf.add_increasing_option('add_plugin_dirs', default=[])
    return conf


def make_plugin_configuration(plugin_name=''):
    r'''Return new plugin configuration.
    '''
    conf = plib.Options()
    conf.add_option('module',      default='')
    conf.add_option('exec_period', default=10)
    conf.add_option('fail_limit',  default=10)
    conf.add_option('premetric',   default='')
    conf.add_option('metric',      default=plugin_name)
    conf.add_option('postmetric',  default='')
    conf.add_option('settings',    default={})
    return conf


def is_configuration_valid(conf):
    r'''Check configuration.
    '''
    if not conf['plugin_dirs'] and not conf['add_plugin_dirs']:
        LOG.error('No directories for importing plugins.')
        return False
    used_plugin_metrics = []
    if not conf['plugins']:
        LOG.error('No plugins specified.')
        return False
    for plugin_name, plugin_conf in conf['plugins'].items():
        if not plugin_conf['module']:
            LOG.error('Not specified "module" for plugin "{}".'
                      .format(plugin_name))
            return False
        if plugin_conf['exec_period'] < 1:
            LOG.error('Too small "exec_period" for plugin "{}".'
                      .format(plugin_name))
            return False
        if plugin_conf['plugin_metric'] in used_plugin_metrics:
            LOG.warning('Duplicate metric {} for plugin "{}".'
                        .format(plugin_conf['plugin_metric'], plugin_name))
        used_plugin_metrics.append(plugin_conf['plugin_metric'])
        if not isinstance(plugin_conf['settings'], dict):
            LOG.error('Settings of plugin "{}" is not dictionary.'
                      .format(plugin_name))
            return False
    return True


def get_execution_arguments():
    r'''Get an execution arguments.
    '''
    default_conf_file = '/etc/pylegraf/conf.yml'
    parser = ArgumentParser()
    parser.description = __doc__

    if '--unittest' in sys.argv:
        parser.add_argument('--unittest', dest='unittest', action='store_true')
    parser.add_argument('--version', action='version', version=str(__version__))
    parser.add_argument('-c', '--conf-file', dest='conf_file', type=str,
                        default=None, metavar='FILE',
                        help='Read configuration from this FILE.'
                        ' Default is "{}".'.format(default_conf_file))
    parser.add_argument('-v', '--verbose', action='store_true',
                        dest='verbose', default=False,
                        help='Verbose mode ("debug" log level).')
    parser.add_argument('--show-conf-yaml', action='store_true',
                        dest='show_conf_yaml', default=False,
                        help='Show compiled configuration and exit.')
    parser.add_argument('--test', dest='test', type=str,
                        default=None, metavar='PLUGIN',
                        help='Test specified PLUGIN.')

    parseargs = parser.parse_args()
    execargs = {a[0]: a[1] for a in parseargs._get_kwargs()}
    if execargs['conf_file'] is None:
        if 'PYLEGRAF_CONF_FILE' in os.environ:
            execargs['conf_file'] = os.environ['PYLEGRAF_CONF_FILE']
        else:
            execargs['conf_file'] = default_conf_file

    return execargs


class PidFile():
    r'''Context manager. Create and delete PID file.
    '''
    def __init__(self, filename=None):
        self.filename = filename

    def __enter__(self):
        if self.filename:
            LOG.info('Try to create PID file "{}"'.format(self.filename))
            error = None
            try:
                with open(self.filename, 'w') as fd:
                    fd.write(str(os.getpid()))
            except OSError as err: error = str(err)
            if error: LOG.error('Can not create PID file. ' + error)

    def __exit__(self, *_):
        if self.filename:
            LOG.info('Try to delete PID file "{}"'.format(self.filename))
            error = None
            try:
                os.remove(self.filename)
            except OSError as err: error = str(err)
            if error: LOG.error('Can not delete PID file. ' + error)


def _unittest():
    r'''Tests
    '''
    LOG.setLevel(logging.CRITICAL)
    from test_lib import tmpdir, make_tmpfile, clear_tmpdir

    ### def read_configuration_file()
    F = read_configuration_file
    main_conf = 'main_conf.yml'
    clear_tmpdir()
    make_tmpfile(
        main_conf,
        'plugin_dirs:\n'
        '  - dir1\n'
        '  - /dir2\n'
        'exec_mode: single\n'
        'add_conf:\n'
        '  - ' + tmpdir + '/add_conf*.yml\n'
    )
    make_tmpfile(
        'add_conf.yml',
        'add_plugin_dirs:\n'
        '  - dir3\n'
        '  - dir4\n'
        'exec_mode: once\n'
        'add_conf:\n'
        '  - ' + tmpdir + '/add_conf.yml\n'
    )
    make_tmpfile(
        'add_conf2.yml',
        'pid_file: nopid\n'
        'plugins:\n'
        '  plg1:\n'
        '    module: mod\n'
    )
    assert F(tmpdir + '/' + main_conf) == {
        'add_plugin_dirs': ['dir3', 'dir4'],
        'exec_mode': 'once',
        'log_level': 'warning',
        'pid_file': 'nopid',
        'plugin_dirs': ['dir1', '/dir2'],
        'plugins': {'plg1': {
            'exec_period': 10,
            'fail_limit': 10,
            'metric': 'plg1',
            'module': 'mod',
            'postmetric': '',
            'premetric': '',
            'settings': {}
        }}
    }

    ### class FileListener()
    A = FileListener()
    clear_tmpdir()
    for f in ('aa.a', 'aa.b', 'ab.a', 'ab.b', 'bb.a', 'bb.b'): make_tmpfile(f)
    A.search_files(os.path.join(tmpdir, 'aa.*'))
    assert A.get_next_file() == os.path.join(tmpdir, 'aa.a')
    assert A.get_next_file() == os.path.join(tmpdir, 'aa.b')
    assert A.get_next_file() is None
    A.search_files(tmpdir + '/*.b')
    assert A.get_next_file() == os.path.join(tmpdir, 'aa.b')
    A.search_files(tmpdir + '/bb.b')
    assert A.get_next_file() == os.path.join(tmpdir, 'ab.b')
    assert A.get_next_file() == os.path.join(tmpdir, 'bb.b')
    assert A.get_next_file() == os.path.join(tmpdir, 'bb.b')
    assert A.get_next_file() is None

    ### def interpret_config_file()
    F = interpret_config_file
    clear_tmpdir()
    make_tmpfile(
        'conf.yml',
        'a:\n'
        '  - a1\n'
        '  - "a2"\n'
        'b: b1\n'
        'c:\n'
        '  - "c1"\n'
        '  - c2\n'
        '  - 3\n'
        'd: [true, d2, 0.45]\n'
    )
    assert F(os.path.join(tmpdir, 'conf.yml')) == {
        'a': ['a1', 'a2'],
        'b': 'b1',
        'c': ['c1', 'c2', 3],
        'd': [True, 'd2', 0.45]
    }

    make_tmpfile(
        'conf.json',
        '{"a": [\n'
        '    "a1",\n'
        '    "a2"],\n'
        '"b": [\n'
        '    "b1",\n'
        '    "b2"]}\n'
    )
    assert F(os.path.join(tmpdir, 'conf.json')) == {
        'a': ['a1', 'a2'], 'b': ['b1', 'b2']}

    ### def make_main_configuration()
    A = make_main_configuration()
    assert A['log_level'] == 'warning'

    ### def make_plugin_configuration()
    A = make_plugin_configuration('plgname')
    assert A['exec_period'] > 0
    assert A['metric'] == 'plgname'

    ### def is_configuration_valid
    F = is_configuration_valid
    assert F({
        'add_plugin_dirs': ['dir3', 'dir4'],
        'plugin_dirs': ['dir1', '/dir2'],
        'plugins': {}                         # No plugins
    }) is False
    assert F({
        'add_plugin_dirs': ['dir3', 'dir4'],
        'plugin_dirs': ['dir1', '/dir2'],
        'plugins': {
            'plg1': {
                'exec_period': 0,             # Wrong exec_period
                'module': 'mod',
                'plugin_metric': 'plg1',
                'settings': {}
            }
        }
    }) is False
    assert F({
        'add_plugin_dirs': ['dir3', 'dir4'],
        'plugin_dirs': ['dir1', '/dir2'],
        'plugins': {
            'plg1': {
                'exec_period': 3,
                'module': '',                 # No module name
                'plugin_metric': 'plg1',
                'settings': {}
            }
        }
    }) is False
    assert F({
        'add_plugin_dirs': ['dir3', 'dir4'],
        'plugin_dirs': ['dir1', '/dir2'],
        'plugins': {
            'plg1': {
                'exec_period': 3,
                'module': 'mod',
                'plugin_metric': 'plg1',
                'settings': []                # Wrong settings format
            }
        }
    }) is False
    assert F({
        'add_plugin_dirs': [],                # No directories for importing
        'plugin_dirs': [],                    # plugins.
        'plugins': {
            'plg1': {
                'exec_period': 3,
                'module': 'mod',
                'plugin_metric': 'plg1',
                'settings': {}
            }
        }
    }) is False
    assert F({                                # Everything ok.
        'add_plugin_dirs': ['dir3', 'dir4'],
        'plugin_dirs': ['dir1', '/dir2'],
        'plugins': {
            'plg1': {
                'exec_period': 3,
                'module': 'mod',
                'plugin_metric': 'plg1',
                'settings': {'a': 1, 'b': 2}
            }
        }
    }) is True

    ### def get_execution_arguments()
    A = get_execution_arguments()
    assert A['unittest'] is True

    ### class PidFile()
    clear_tmpdir()
    pidfile = os.path.join(tmpdir, 'file.pid')

    with PidFile(pidfile): listdir = os.listdir(tmpdir)
    assert 'file.pid' in listdir
    listdir = os.listdir(tmpdir)
    assert 'file.pid' not in listdir

    with PidFile(''): listdir = os.listdir(tmpdir)
    assert 'file.pid' not in listdir

    return 0


if __name__ == '__main__' and '--unittest' in sys.argv: sys.exit(_unittest())
elif __name__ == '__main__': sys.exit(main())
