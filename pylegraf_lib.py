#!/usr/bin/env python3
__doc__ = 'Shared library.'

import os, sys
import socket
import time
from hashlib import md5 as md5hash
import pickle
import glob
from copy import copy
import yaml
import json


def python_version():
    r'''Return Python version as float number.
    '''
    return float(str(sys.version_info.major) + '.' + str(sys.version_info.minor))


def typecast(value, newtype):
    r'''Return casted value or None if failed.

    ----- examples -----
    typecast('yes', bool)      # True
    typecast({'no': 0}, bool)  # False
    typecast(23, str)          # '23'
    typecast((23, 12), str)    # '23'
    typecast({'a': 1}, list)   # ['a']
    typecast('foo', list)      # ['foo']
    typecast('a', int)         # None
    '''
    v = value
    t = newtype
    if type(v) is t or t is type(None) or t is None or v is None: return v
    if t is bool: return is_true(v)
    vseq = isinstance(v, list) or isinstance(v, tuple) or isinstance(v, set)
    vsimp = isinstance(v, str) or isinstance(v, int) or isinstance(v, float)
    error = None
    try:
        if vseq and t is str:      v = str(v[0]) if v else ''
        elif vseq and t is int:    v = int(v[0]) if v else 0
        elif vseq and t is float:  v = float(v[0]) if v else 0.0
        elif vsimp and t is list:  v = [v]
        elif vsimp and t is tuple: v = (v,)
        elif vsimp and t is set:   v = {v}
        else: v = t(v)
    except (ValueError, IndexError, TypeError, AttributeError) as err:
        error = 'Can`t typecast. {}'.format(err)
    if error: raise TypeError(error)
    return v


def is_true(value):
    r'''True if the value looks like true.

    ----- examples -----
    is_true(1)         # True
    is_true('Yes')     # True
    is_true(0)         # False
    is_true([])        # False
    is_true(['no', 1]) # False
    '''
    v = value
    if isinstance(v, list) or isinstance(v, tuple) or isinstance(v, set):
        if v: v = v[0]
        else: return False
    v = str(v).lower().strip('[]{}(),.! "\'')
    if v in ('true', 'yes', 'on', 'up', '1', 'allow', 'accept', 'notfalse'):
        return True
    return False


def read_yaml(rawdata):
    r'''Try to interpret a data as YAML.
    '''
    try: data = yaml.safe_load(rawdata)
    except (yaml.parser.ParserError,
            yaml.scanner.ScannerError,
            yaml.composer.ComposerError):
        return None
    return data


def read_json(rawdata):
    r'''Try to interpret a data as JSON.
    '''
    try: data = json.loads(rawdata)
    except Exception: return None
    return data


def which_not_none(heap):
    r'''Return the first element that is not None.
    '''
    for element in heap:
        if element is not None: return element


def absolute_path(path, pathfrom=None):
    r'''Convert relative path to absolute path.

    In: path     (str) - Path in file system.
        pathfrom (str) - Start point for relative path.
    '''
    if path.startswith('~/'): path = os.environ.get('HOME', '') + path[1:]
    if path.startswith('/'): return os.path.abspath(path)
    if pathfrom: pathfrom = absolute_path(pathfrom)
    else: pathfrom = os.path.abspath('.')
    if not os.path.isdir(pathfrom): pathfrom = os.path.dirname(pathfrom)
    path = pathfrom + '/' + path
    return(os.path.abspath(path))


def pick_up_an_unused_name(namebase, used_names=()):
    r'''Returns the name, that is not in the list.

    In: namebase  (str)
        name_list (list, tuple)

    ----- examples -----
    pick_up_an_unused_name('test')                     # 'test'
    pick_up_an_unused_name('test', ('test', 'test1'))  # 'test2'
    '''
    index = 0
    while (namebase + str(index or '')) in used_names: index += 1
    return namebase + str(index or '')


class TcpSocket():
    r'''Network tcp socket context manager.
    '''
    def __init__(self, host, port):
        self.host = host
        self.port = port

    def __enter__(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))
        return self.socket

    def __exit__(self, *ignore):
        self.socket.close()


class CacheForFunction():
    r'''Function wrapper for cache of returns.
    time_to_live      - Time to live note in seconds.
    cache_size_limit  - Maximum number of notes may cached.
    clearing_period   - Period in seconds for clearing the cache.

    ----- examples -----
    ctf = CacheForFunction(socket.getfqdn)
    ctf('192.168.1.1')   # router.office (from function)
    ctf('192.168.1.1')   # router.office (from cache)

    ctf = CacheForFunction(convert_to_metric)
    ctf('hello!!!')      # 'hello___'    from function
    ctf('hello there')   # 'hello_there' from function
    ctf('hello!!!')      # 'hello___'    from cache
    ctf.clear()
    ctf('hello!!!')      # 'hello___'    from function
    '''
    def __init__(self, func):
        self._func = func
        self.time_to_live     = 1000
        self.cache_size_limit = 1000
        self.clearing_period  = 1000
        self.clear_all()

    def __call__(self, *args, **kargs):
        r'''Return value and store it in cache.
        '''
        key = str(args) + str(kargs)
        now = int(time.time())
        cache = self._cache

        # Is it time to clear the cache?
        if now > self._next_clearing_time:
            for k in tuple(cache.keys()):
                if cache[k][0] < now: del(cache[k])
            self._next_clearing_time = now + self.clearing_period

        try:
            if cache[key][0] >= now: return cache[key][1]
        except KeyError: pass
        value = self._func(*args, **kargs)

        # The cache is not full?
        if len(cache) < self.cache_size_limit:
            cache[key] = [now + self.time_to_live, value]

        return value

    def clear_all(self):
        r'''Clear whole cache.
        '''
        self._cache = {}
        self._next_clearing_time = int(time.time()) + self.clearing_period
        return True

    def cache_length(self):
        return len(self._cache)


def SendTcp(host, port, message):
    r'''Send message trought tcp.
    '''
    try:
        with TcpSocket(host, port) as sock: sock.send(message)
    except ConnectionError:
        return False
    return True


def convert_to_metric(string, preserve_dot=False):
    r'''Convert string to valid metric.

    ----- examples -----
    convert_to_metric(' go.TO20\t*')                     # '_go_to20___'
    convert_to_metric('.a.b...c..')                      # '_a_b___c__'
    convert_to_metric('..a.b...c..', preserve_dot=True)  # 'a.b.c'
    '''
    string = '' if string is None else str(string)
    string = ''.join(
        [a if (a.isalnum() or a in '-_.') else '_' for a in string])
    if not preserve_dot:
        string = string.replace('.', '_')
        return string
    string = string.strip('.')
    prev = ''
    while string != prev:
        prev = string
        string = string.replace('..', '.')
    return string


def join_metric(parts):
    r'''Join metric correctly. Every part must be valid metric.

    ----- examples -----
    join_metric(('a', 'b', 'c'))             # 'a.b.c'
    join_metric(('', '', 'c'))               # 'c'
    join_metric(('', ''))                    # ''
    '''
    significant_parts = [a for a in parts if a]
    return '.'.join(significant_parts)


def mask_chars(string, search, mask='_'):
    r'''Replace few chars with one char.

    ----- examples -----
    mask_chars('/dev/foo', '/')       # '_dev_foo'
    mask_chars('/\dev /...foo', ' /\\.', '*')  # '**dev*****foo'
    '''
    string = string.translate(str.maketrans(search, mask * len(search)))
    return string


def in_percents(portion, whole_number, precision=0):
    r'''Returns a percentage.
    In: portion      (int, float)
        whole_number (int, float)
        precision    (int)

    ----- examples -----
    in_percents(3, 31, 3)       # 9.677
    in_percents(444.3, 20, -2)  # 2200.0
    in_percents(7, 10)          # 70.0
    '''
    try: return round(portion * 100 / whole_number, precision)
    except ZeroDivisionError: return 0.0


def time_ms():
    r'''Return unix time in milliseconds.
    '''
    return int(time.time() * 1000)


def list_files(mask):
    r'''Returns a list of files by mask.
    In: mask, filepath or dirpath.
    Out: list of files as tuple.

    ----- examples -----
    list_files('/etc/hosts')  # ('/etc/hosts')
    list_files('/etc/host*')  # ('/etc/hostname', '/etc/hosts')
    '''
    if os.path.isfile(mask): return (mask,)
    if os.path.isdir(mask): mask += '/*'
    things = (a for a in glob.glob(mask))
    files = tuple(sorted(filter(os.path.isfile, things)))
    return files


class Options():
    r'''Simple work with a list of options.

    ----- examples -----
    A = Options()
    A.add_option('option1', default='no data')
    A.add_increasing_option('option2')
    A.get('option1')                      # 'no data'
    A.set('option1', 'something')
    A.get('option1')                      # 'something'
    A.set('option2', 'fus')
    A.set('option2', '-ro')
    A.set('option2', '-dah')
    A.get('option2')                      # 'fus-ro-dah'
    '''

    def __init__(self):
        self._options = {}

    def add_option(self, key, default=None):
        r'''Add standard option to list.
        '''
        self._options[key] = {}
        option = self._options[key]
        option['value']         = None
        option['default']       = default
        option['type']          = type(default) if default is not None else None
        option['choices']       = None
        option['is_increasing'] = False

    def add_choices_option(self, key, default=None, choices=None):
        r'''Add option with list of choices.
        '''
        self.add_option(key, default)
        self._options[key]['choices'] = choices

    def add_increasing_option(self, key, default=None):
        r'''Add option with increasing value.
        '''
        self.add_option(key, default)
        self._options[key]['is_increasing'] = True

    def __setitem__(self, key, value):
        option = self._options[key]
        value = typecast(copy(value), option['type'])
        if option['choices'] is not None and value not in option['choices']:
            raise ValueError('Value not in choices.')
        if option['is_increasing'] and option['value'] is not None:
            option['value'] += value
        else:
            option['value'] = value

    def set(self, key, value):
        r'''Set value.
        '''
        self.__setitem__(key, value)

    def set_default(self, key, default):
        r'''Set default value.
        '''
        option = self._options[key]
        option['default'] = default
        option['type']    = type(default) if default is not None else None
        self.__setitem__(key, option['value'])

    def __getitem__(self, key):
        option = self._options[key]
        if option['value'] is not None: return option['value']
        else: return option['default']

    def clear(self, key):
        r'''Delete value of option.
        '''
        self.option[key]['value'] = None

    def get(self, key):
        r'''Return option value if set or default value instead.
        '''
        return self.__getitem__(key)

    def get_all(self):
        r'''Return values of whole list of options.
        '''
        conf = {}
        for key in self._options: conf[key] = self.__getitem__(key)
        return conf

    def get_default(self, key):
        r'''Return default option value.
        '''
        return self._options[key]['default']

    def is_changed(self, key):
        r'''True if value of option is setted.
        '''
        return self._options[key]['value'] is not None

    def __contains__(self, key): return key in self._options

    def __delitem__(self, key): del(self._options[key])

    def __len__(self): return len(self._options)

    def __iter__(self):
        keys = tuple(self._options)
        for key in keys: yield key

    def keys(self):
        r'''Return all keys iterator.
        '''
        keys = tuple(self._options)
        for key in keys: yield key

    def values(self):
        r'''Return all values iterator.
        '''
        keys = tuple(self._options)
        for key in keys: yield self.__getitem__(key)

    def items(self):
        r'''Return all keys, values iterator.
        '''
        keys = tuple(self._options)
        for key in keys: yield key, self.__getitem__(key)


def calculate_hash_md5(data):
    r'''Return md5 hash of data.

    ----- examples -----
    calculate_hash_md5('foo')  # 'a087fdd839ca21a7d958ea964c4dae84'
    '''
    h = md5hash()
    h.update(pickle.dumps(data, protocol=4))
    return h.hexdigest()


def _unittest():
    '''Tests
    '''
    from test_lib import tmpdir, make_tmpfile, clear_tmpdir

    ### def python_version

    ### def typecast()
    F = typecast
    assert F('a', None) == 'a'
    assert F(None, str) is None
    assert F('yes', bool) is True
    assert F({'no': 0}, bool) is False
    assert F(23, str)         == '23'
    assert F((23, 12), str)   == '23'
    assert F({'a': 1}, list)  == ['a']
    assert F('foo', list)     == ['foo']
    err = False
    try: F('a', int)
    except TypeError: err = True
    assert err

    ### def is_true()
    F = is_true
    assert F(1) is True
    assert F('Yes') is True
    assert F(0) is False
    assert F([]) is False
    assert F(['no', 1]) is False

    ### def read_yaml()
    F = read_yaml
    assert F('a:\n  a1: 1\n  a2:\n  - a11\n  - 12\n') == \
        {'a': {'a1': 1, 'a2': ['a11', 12]}}

    ### def read_json()
    F = read_json
    assert F('{"a": {"b": 1, "c": ["d", 2]}}') == \
        {'a': {'b': 1, 'c': ['d', 2]}}

    ### which_not_none
    F = which_not_none
    assert F((None, '', 0, 'foo')) == ''
    assert F((None, 0, 'foo')) == 0
    assert F((None, None)) is None

    ### def absolute_path()
    F = absolute_path
    wdir = os.getcwd()
    assert F('')                    == wdir
    assert F('foo/bar')             == wdir + '/foo/bar'
    assert F('', '/etc')            == '/etc'
    assert F('foo/bar', '/etc')     == '/etc/foo/bar'
    assert F('foo/', '/etc/hosts')  == '/etc/foo'
    assert F('/foo/', '/etc/hosts') == '/foo'

    ### pick_up_an_unused_name()
    F = pick_up_an_unused_name
    assert F('test')                    == 'test'
    assert F('test', ())                == 'test'
    assert F('test', ('test', 'test1')) == 'test2'

    ### def TcpSocket()

    ### class CacheForFunction()
    A = CacheForFunction(in_percents)
    assert A(3, 10)     == 30  # from origin function
    assert A(3, 10)     == 30  # from cache
    assert A.cache_length() == 1

    A = CacheForFunction(convert_to_metric)
    assert A('hello!!!')    == 'hello___'     # from origin function
    assert A('hello there') == 'hello_there'  # from origin function
    assert A('hello!!!')    == 'hello___'     # from cache
    A.clear_all()
    assert A.cache_length()     == 0
    assert A('hello!!!')    == 'hello___'     # from origin function

    ### def SendTcp()

    ### def convert_to_metric()
    F = convert_to_metric
    assert F(' go.TO20\t.***.') == '_go_TO20______'
    assert F('..')              == '__'
    assert F('..a.b...c..')     == '__a_b___c__'
    assert F(' go.TO20\t.***.', preserve_dot=True) == '_go.TO20_.___'
    assert F('..', preserve_dot=True)              == ''
    assert F('..a.b...c..', preserve_dot=True)     == 'a.b.c'

    ### def join_metric()
    F = join_metric
    assert F(('a', 'b', 'c')) == 'a.b.c'
    assert F(('', '', 'c'))   == 'c'
    assert F((''))            == ''

    ### def mask_chars()
    F = mask_chars
    assert F(string='/dev/foo', search=' /\\.') == '_dev_foo'
    assert F(string='/\\dev /...foo\n', search=' /\\.\n\t', mask='*') == \
        '**dev*****foo*'

    ### def in_percents()
    F = in_percents
    assert F(3, 31, 3)      == 9.677
    assert F(444.3, 20, -2) == 2200.0
    assert F(0, 25)         == 0.0
    assert F(10, 0)         == 0.0
    assert F(7, 10)         == 70.0
    assert F(-7, 10)        == -70.0
    assert F(-7, -10)       == 70.0
    assert F(7, -10)        == -70.0

    ### def time_ms()

    ### def list_files()
    F = list_files
    clear_tmpdir()
    for file in ('file1.yml', 'file2.yml', 'file1.yaml', 'file1.conf'):
        make_tmpfile(file)

    assert F(tmpdir + '/file1.yml') == (tmpdir + '/file1.yml',)
    assert F(tmpdir + '/*.yml') == tuple(tmpdir + a for a in ('/file1.yml',
                                                              '/file2.yml'))
    assert F(tmpdir + '/*.conf') == (tmpdir + '/file1.conf',)
    assert F(tmpdir) == tuple(tmpdir + a for a in ('/file1.conf', '/file1.yaml',
                                                   '/file1.yml', '/file2.yml'))

    ### class Options()
    A = Options()

    A.add_option('option1', default='no data')
    assert A.get('option1') == 'no data'
    A.set('option1', 'something')
    assert A.get('option1') == 'something'

    A.add_increasing_option('option2')
    A.set('option2', 'fus')
    A.set('option2', '-ro')
    A.set('option2', '-dah')
    assert A.get('option2') == 'fus-ro-dah'

    A.add_choices_option('option3', 1, (1, 2, 3))
    A['option3'] = 2
    assert A['option3'] == 2
    error = False
    try: A['option3'] = 0
    except ValueError: error = True
    assert error
    assert A['option3'] == 2

    ### def calculate_hash_md5()
    F = calculate_hash_md5
    assert F('foo') == 'a087fdd839ca21a7d958ea964c4dae84'

    return 0


if __name__ == '__main__' and '--unittest' in sys.argv: sys.exit(_unittest())
