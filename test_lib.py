#!/usr/bin/env python3
__doc__ = 'Unittest library.'

import os

tmpdir = '/tmp/pylegraf-unittest'
if not os.path.isdir(tmpdir): os.mkdir(tmpdir)


def make_tmpfile(filename, data=''):
    try:
        with open(tmpdir + '/' + filename, 'w') as fd: fd.write(data)
    except OSError:
        return None


def add_to_tmpfile(filename, data=''):
    try:
        with open(tmpdir + '/' + filename, 'a') as fd: return fd.write(data)
    except OSError:
        return None


def read_tmpfile(filename):
    try:
        with open(tmpdir + '/' + filename, 'r') as fd: return fd.read()
    except OSError:
        return None


def clear_tmpdir(directory=tmpdir):
    if not directory.startswith(tmpdir): return
    for f in os.listdir(directory):
        ff = os.path.join(directory, f)
        if os.path.isdir(ff):
            clear_tmpdir(ff)
            os.rmdir(ff)
        else:
            os.remove(ff)
